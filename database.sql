-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: poputy
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appliacations`
--

DROP TABLE IF EXISTS `appliacations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appliacations` (
  `id_appliacation` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `users_id_users` int(11) DEFAULT NULL,
  `type_of_application_id_type_of_applications` int(11) DEFAULT NULL,
  `all_paramentres` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_id_status` int(11) DEFAULT NULL,
  `groups_of_application_id_groups_of_applications` int(11) DEFAULT NULL,
  `target_id_target` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `place` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_country_from` int(11) DEFAULT NULL,
  `sublist_city_from` int(11) DEFAULT NULL,
  `details_place_from` text COLLATE utf8_unicode_ci,
  `add_map` int(1) DEFAULT NULL,
  `list_country_to` int(11) DEFAULT NULL,
  `sublist_city_to` int(11) DEFAULT NULL,
  `details_place_to` text COLLATE utf8_unicode_ci,
  `urgently` int(1) DEFAULT NULL,
  `date_from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surname_recipient` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_recipient` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name_recipient` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_recipient` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surname_recipient_contact1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_recipient_contact1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name_recipient_contact1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_recipient_contact1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surname_recipient_contact2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_recipient_contact2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name_recipient_contact2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_recipient_contact2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remuneration_transportation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deposit` int(1) DEFAULT NULL,
  `pledge_requirement_amount` int(1) DEFAULT NULL,
  `number_days_after_date_dispatch` int(11) DEFAULT NULL,
  `my_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `photo_recipient` text COLLATE utf8_unicode_ci,
  `weight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dimensions` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_category_time_together` int(11) DEFAULT NULL,
  `sublist_purpose_time_together` int(11) DEFAULT NULL,
  `other` text COLLATE utf8_unicode_ci,
  `budget_from` int(11) DEFAULT NULL,
  `budget_to` int(11) DEFAULT NULL,
  `list_currency` int(11) DEFAULT NULL,
  `date_where` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approximate_date` int(1) DEFAULT NULL,
  `list_part_of_day` int(1) DEFAULT NULL,
  `possible_children` int(1) DEFAULT NULL,
  `count_people` int(11) DEFAULT NULL,
  `plus_or_minus_person` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `drive_car` int(1) DEFAULT NULL,
  `list_transport` int(11) DEFAULT NULL,
  `my_car` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remuneration_from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remuneration_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adres_where` text COLLATE utf8_unicode_ci,
  `adres_whence` text COLLATE utf8_unicode_ci,
  `time_from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_category_travel` int(11) DEFAULT NULL,
  `sublist_purpose_travel` int(11) DEFAULT NULL,
  `count_days` int(11) DEFAULT NULL,
  `list_settlement_type` int(11) DEFAULT NULL,
  `requirement_pledge_transportation` int(1) NOT NULL,
  `fotos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto_recipient` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_appliacation`),
  KEY `idx-appliacations-users_id_users` (`users_id_users`),
  KEY `idx-appliacations-type_of_application_id_type_of_applications` (`type_of_application_id_type_of_applications`),
  KEY `idx-appliacations-status_id_status` (`status_id_status`),
  KEY `idx-appliacations-groups_of_application_id` (`groups_of_application_id_groups_of_applications`),
  KEY `idx-appliacations-target_id_target` (`target_id_target`),
  CONSTRAINT `fk-appliacations-groups_of_application_id` FOREIGN KEY (`groups_of_application_id_groups_of_applications`) REFERENCES `groups_of_applications` (`id_groups_of_applications`) ON DELETE CASCADE,
  CONSTRAINT `fk-appliacations-status_id_status` FOREIGN KEY (`status_id_status`) REFERENCES `status` (`id_status`) ON DELETE CASCADE,
  CONSTRAINT `fk-appliacations-target_id_target` FOREIGN KEY (`target_id_target`) REFERENCES `target` (`id_target`) ON DELETE CASCADE,
  CONSTRAINT `fk-appliacations-type_of_application_id_type_of_applications` FOREIGN KEY (`type_of_application_id_type_of_applications`) REFERENCES `type_of_applications` (`id_type_of_application`) ON DELETE CASCADE,
  CONSTRAINT `fk-appliacations-users_id_users` FOREIGN KEY (`users_id_users`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appliacations`
--

LOCK TABLES `appliacations` WRITE;
/*!40000 ALTER TABLE `appliacations` DISABLE KEYS */;
INSERT INTO `appliacations` VALUES (23,'',5,1,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,'',0,NULL,NULL,'',0,'','','','','','','','','','','','','','','','',0,0,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'//uploads/fotos/1490137357.jpg','//uploads/fotos/1490137357.jpg'),(24,'88',5,5,NULL,1,NULL,NULL,NULL,6,'7','2017-03-26',576,577,'test2888',1,576,577,'test2666',1,'1970-01-01','1970-01-01','','','','','5','5','5','5','5','5','5','5','5','426',1,0,5,'5','3434pppp',NULL,'s','s',452,599,'23123',31,31,435,'2017-03-09',1,NULL,1,268,288,NULL,1,449,'1','45','54','3333',NULL,'99','1',NULL,NULL,NULL,NULL,1,'','');
/*!40000 ALTER TABLE `appliacations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id_countries` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_countries_pr` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_countries`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Россия',NULL),(3,'Россия',NULL);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `educations`
--

DROP TABLE IF EXISTS `educations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `educations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_profile_id` int(11) DEFAULT NULL,
  `view` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interval` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `period_from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `period_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx-educations-user_profile_id` (`user_profile_id`),
  CONSTRAINT `fk-educations-user_profile_id` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profiles` (`id_user_profiles`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=57722 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `educations`
--

LOCK TABLES `educations` WRITE;
/*!40000 ALTER TABLE `educations` DISABLE KEYS */;
INSERT INTO `educations` VALUES (57716,NULL,1,'',NULL,'','',''),(57717,NULL,1,'222',NULL,'222','2017-03-08','2017-03-17'),(57718,NULL,1,'3333333',NULL,'3333333','2017-03-01','2017-03-23'),(57719,NULL,1,'1111111',NULL,'1111111','2017-03-01','2017-03-10'),(57720,NULL,1,'3',NULL,'3','2017-03-08','2017-03-11'),(57721,NULL,1,'222',NULL,'222','2017-03-02','2017-03-17');
/*!40000 ALTER TABLE `educations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `appl_id` int(11) DEFAULT NULL,
  `exten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (7,'39f5433d4eddcf59cd3dd826c4c7d0bd','/uploads/fotos/1490137357.jpg',NULL,'jpg',5),(8,'230e0d32d1de9e0d0e3e8bc6aa31821c_fitted_630x420','/uploads/fotos/1490137357.jpg',NULL,'jpg',5),(9,'39f5433d4eddcf59cd3dd826c4c7d0bd','uploads/brif/1490137357.jpg',23,'jpg',5),(10,'230e0d32d1de9e0d0e3e8bc6aa31821c_fitted_630x420','/uploads/fotos/1490137357.jpg',23,'jpg',5);
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `foreign_language`
--

DROP TABLE IF EXISTS `foreign_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreign_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_profile_id` int(11) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `lang_level_id` int(11) DEFAULT NULL,
  `list_language` int(11) DEFAULT NULL,
  `list_language_level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx-foreign_language-user_profile_id` (`user_profile_id`),
  CONSTRAINT `fk-foreign_language-user_profile_id` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profiles` (`id_user_profiles`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=379 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `foreign_language`
--

LOCK TABLES `foreign_language` WRITE;
/*!40000 ALTER TABLE `foreign_language` DISABLE KEYS */;
INSERT INTO `foreign_language` VALUES (378,'',1,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `foreign_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups_of_applications`
--

DROP TABLE IF EXISTS `groups_of_applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups_of_applications` (
  `id_groups_of_applications` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target_id_target` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_groups_of_applications`),
  KEY `idx-groups_of_applications-target_id_target` (`target_id_target`),
  CONSTRAINT `fk-appliacations-target_id_targets` FOREIGN KEY (`target_id_target`) REFERENCES `target` (`id_target`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups_of_applications`
--

LOCK TABLES `groups_of_applications` WRITE;
/*!40000 ALTER TABLE `groups_of_applications` DISABLE KEYS */;
/*!40000 ALTER TABLE `groups_of_applications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_profile_id` int(11) DEFAULT NULL,
  `name_campany` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interval` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `period_from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `period_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx-jobs-user_profile_id` (`user_profile_id`),
  CONSTRAINT `fk-jobs-user_profile_id` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profiles` (`id_user_profiles`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41023 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` VALUES (41021,NULL,1,'чучу',NULL,'чучик','2017-03-08','2017-03-25'),(41022,NULL,1,'ваы',NULL,'333','2017-03-01','2017-03-25');
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `translation` text COLLATE utf8_unicode_ci,
  `message` text COLLATE utf8_unicode_ci,
  `id_pr` int(11) NOT NULL AUTO_INCREMENT,
  `related` int(11) DEFAULT '0',
  PRIMARY KEY (`id_pr`),
  UNIQUE KEY `unique_id_pr` (`id_pr`),
  KEY `fk_message_source_message` (`id`),
  CONSTRAINT `fk_message_source_message` FOREIGN KEY (`id`) REFERENCES `source_message` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=680 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (1,'ru-RU','1','Да1',1,0),(1,'ru-RU','1','Да1',2,0),(1,'en-EN','Yes','Yes',3,0),(NULL,'ru-Ru','','',4,0),(7,'ru-RU','Имя','Имя',5,0),(8,'ru-RU','Фамилия','Фамилия',6,0),(9,'ru-RU','Отчество','Отчество',7,0),(10,'ru-RU','День рождения','День рождения',8,0),(11,'ru-RU','Пол','Пол',9,0),(12,'ru-RU','Знак зодиака','Знак зодиака',10,0),(13,'ru-RU','Семейное положение','Семейное положение',11,0),(14,'ru-RU','Образование ','Образование ',12,0),(15,'ru-RU','Работа','Работа',13,0),(16,'ru-RU','Интересы','Интересы',14,0),(17,'ru-RU','Опыт','Опыт',15,0),(18,'ru-RU','Характер','Характер',16,0),(19,'ru-RU','Отношение к курению','Отношение к курению',17,0),(20,'ru-RU','Отношение к алкоголю','Отношение к алкоголю',18,0),(21,'ru-RU','Иностранный язык','Иностранный язык',19,0),(22,'ru-RU','Социальное положение','Социальное положение',20,0),(23,'ru-RU','Аватар','Аватар',21,0),(24,'ru-RU','Фон','Фон',22,0),(25,'ru-RU','Религия','Религия',23,0),(26,'ru-RU','Дети','Дети',24,0),(27,'ru-RU','Страна','Страна',25,0),(28,'ru-RU','Город','Город',26,0),(29,'ru-RU','Обновить','Обновить',27,0),(30,'ru-RU','Создать','Создать',28,0),(31,'ru-RU','мужской','мужской',29,0),(31,'ru-RU','женский','женский',30,0),(31,'ru-RU','предпочитаю не указывать','предпочитаю не указывать',31,0),(32,'ru-RU','Лев','Лев',32,0),(32,'ru-RU','Стрелец','Стрелец',33,0),(32,'ru-RU','Телец','Телец',34,0),(32,'ru-RU','Дева','Дева',35,0),(32,'ru-RU','Дева','Дева',36,0),(32,'ru-RU','Козерог','Козерог',37,0),(32,'ru-RU','Близнецы','Близнецы',38,0),(32,'ru-RU','Весы','Весы',39,0),(32,'ru-RU','Водолей','Водолей',40,0),(32,'ru-RU','Рак','Рак',41,0),(32,'ru-RU','Скорпион','Скорпион',42,0),(32,'ru-RU','Рыбы','Рыбы',43,0),(33,'ru-RU','женат/замужем','женат/замужем',44,0),(33,'ru-RU','не женат/не замужем','не женат/не замужем',45,0),(33,'ru-RU','разведен/разведена','разведен/разведена',46,0),(33,'ru-RU','одинок/одинока','одинок/одинока',47,0),(33,'ru-RU','гражданский брак','гражданский брак',48,0),(33,'ru-RU','другое','другое',49,0),(34,'ru-RU','нет','нет',50,0),(34,'ru-RU','взрослые дети','взрослые дети',51,0),(34,'ru-RU','учатся в школе','учатся в школе',52,0),(34,'ru-RU','дошкольного возраста','дошкольного возраста',53,0),(35,'ru-RU','среднее','среднее',55,0),(35,'ru-RU','среднеспециальное','среднеспециальное',56,0),(35,'ru-RU','высшее','высшее',57,0),(35,'ru-RU','ученая степень','ученая степень',58,0),(36,'ru-RU','Христианство','Христианство',59,0),(36,'ru-RU','Ислам','Ислам',60,0),(36,'ru-RU','Иудаизм','Иудаизм',61,0),(36,'ru-RU','Буддизм','Буддизм',62,0),(36,'ru-RU','Язычество','Язычество',63,0),(36,'ru-RU','Атеизм','Атеизм',64,0),(36,'ru-RU','Верю в Бога','Верю в Бога',65,0),(36,'ru-RU','другое','другое',66,0),(37,'ru-RU','Английский','Английский',67,0),(37,'ru-RU','Себуанский','Себуанский',68,0),(37,'ru-RU','Шведский','Шведский',69,0),(37,'ru-RU','Немецкий','Немецкий',70,0),(37,'ru-RU','Нидерландский','Нидерландский',71,0),(37,'ru-RU','Французский','Французский',72,0),(37,'ru-RU','Русский★','Русский★',73,0),(37,'ru-RU','Итальянский','Итальянский',74,0),(37,'ru-RU','Испанский','Испанский',75,0),(37,'ru-RU','Варайский','Варайский',76,0),(37,'ru-RU','Польский','Польский',77,0),(37,'ru-RU','Вьетнамский','Вьетнамский',78,0),(37,'ru-RU','Японский','Японский',79,0),(37,'ru-RU','Португальский','Португальский',80,0),(37,'ru-RU','Китайский','Китайский',81,0),(37,'ru-RU','Украинский','Украинский',82,0),(37,'ru-RU','Каталанский','Каталанский',83,0),(37,'ru-RU','Персидский','Персидский',84,0),(37,'ru-RU','Арабский','Арабский',85,0),(37,'ru-RU','Норвежский (букмол)','Норвежский (букмол)',86,0),(37,'ru-RU','Сербохорватский','Сербохорватский',87,0),(37,'ru-RU','Финский','Финский',88,0),(37,'ru-RU','Венгерский','Венгерский',89,0),(37,'ru-RU','Индонезийский','Индонезийский',90,0),(37,'ru-RU','Румынский','Румынский',91,0),(37,'ru-RU','Чешский','Чешский',92,0),(37,'ru-RU','Корейский','Корейский',93,0),(37,'ru-RU','Сербский','Сербский',94,0),(37,'ru-RU','Турецкий','Турецкий',95,0),(37,'ru-RU','Малайский','Малайский',96,0),(37,'ru-RU','Баскский','Баскский',97,0),(37,'ru-RU','Эсперанто','Эсперанто',98,0),(37,'ru-RU','Болгарский','Болгарский',99,0),(37,'ru-RU','Датский','Датский',100,0),(37,'ru-RU','Минангкабау','Минангкабау',101,0),(37,'ru-RU','Казахский','Казахский',102,0),(37,'ru-RU','Словацкий','Словацкий',103,0),(37,'ru-RU','Армянский','Армянский',104,0),(37,'ru-RU','Южноминьский','Южноминьский',105,0),(37,'ru-RU','Иврит','Иврит',106,0),(37,'ru-RU','Литовский','Литовский',107,0),(37,'ru-RU','Хорватский','Хорватский',108,0),(37,'ru-RU','Чеченский','Чеченский',109,0),(37,'ru-RU','Словенский','Словенский',110,0),(37,'ru-RU','Эстонский','Эстонский',111,0),(37,'ru-RU','Галисийский','Галисийский',112,0),(37,'ru-RU','Норвежский (нюнорск)','Норвежский (нюнорск)',113,0),(37,'ru-RU','Узбекский','Узбекский',114,0),(37,'ru-RU','Греческий','Греческий',115,0),(37,'ru-RU','Белорусский','Белорусский',116,0),(37,'ru-RU','Латинский','Латинский',117,0),(37,'ru-RU','Английский (упрощённый)','Английский (упрощённый)',118,0),(37,'ru-RU','Волапюк','Волапюк',119,0),(37,'ru-RU','Хинди','Хинди',120,0),(37,'ru-RU','Тайский','Тайский',121,0),(37,'ru-RU','Азербайджанский','Азербайджанский',122,0),(37,'ru-RU','Урду','Урду',123,0),(37,'ru-RU','Грузинский','Грузинский',124,0),(37,'ru-RU','Тамильский','Тамильский',125,0),(37,'ru-RU','Валлийский','Валлийский',126,0),(37,'ru-RU','Македонский','Македонский',127,0),(37,'ru-RU','Малагасийский','Малагасийский',128,0),(37,'ru-RU','Окситанский','Окситанский',129,0),(37,'ru-RU','Латышский','Латышский',130,0),(37,'ru-RU','Боснийский','Боснийский',131,0),(37,'ru-RU','Неварский','Неварский',132,0),(37,'ru-RU','Татарский','Татарский',133,0),(37,'ru-RU','Таджикский','Таджикский',134,0),(37,'ru-RU','Телугу','Телугу',135,0),(37,'ru-RU','Тагальский','Тагальский',136,0),(37,'ru-RU','Албанский','Албанский',137,0),(37,'ru-RU','Пьемонтский','Пьемонтский',138,0),(37,'ru-RU','Киргизский','Киргизский',139,0),(37,'ru-RU','Бретонский','Бретонский',140,0),(37,'ru-RU','Белорусский (тарашкевица)','Белорусский (тарашкевица)',141,0),(37,'ru-RU','Юэ','Юэ',142,0),(37,'ru-RU','Гаитянский креольский','Гаитянский креольский',143,0),(37,'ru-RU','Яванский','Яванский',144,0),(37,'ru-RU','Астурийский','Астурийский',145,0),(37,'ru-RU','Бенгальский','Бенгальский',146,0),(37,'ru-RU','Люксембургский','Люксембургский',147,0),(37,'ru-RU','Малаялам','Малаялам',148,0),(37,'ru-RU','Маратхи','Маратхи',149,0),(37,'ru-RU','Африкаанс','Африкаанс',150,0),(37,'ru-RU','Западный пенджаби','Западный пенджаби',151,0),(37,'ru-RU','Шотландский (германский)','Шотландский (германский)',152,0),(37,'ru-RU','Исландский','Исландский',153,0),(37,'ru-RU','Ирландский','Ирландский',154,0),(37,'ru-RU','Чувашский','Чувашский',155,0),(37,'ru-RU','Башкирский','Башкирский',156,0),(37,'ru-RU','Западнофризский','Западнофризский',157,0),(37,'ru-RU','Суахили','Суахили',158,0),(37,'ru-RU','Бирманский','Бирманский',159,0),(37,'ru-RU','Ломбардский','Ломбардский',160,0),(37,'ru-RU','Арагонский','Арагонский',161,0),(37,'ru-RU','Йоруба','Йоруба',162,0),(37,'ru-RU','Непальский','Непальский',163,0),(37,'ru-RU','Идо','Идо',164,0),(37,'ru-RU','Гуджарати','Гуджарати',165,0),(37,'ru-RU','Нижненемецкий диалект немецкого','Нижненемецкий диалект немецкого',166,0),(37,'ru-RU','Сицилийский','Сицилийский',167,0),(37,'ru-RU','Бишнуприя-манипури','Бишнуприя-манипури',168,0),(37,'ru-RU','Восточный пенджаби','Восточный пенджаби',169,0),(37,'ru-RU','Курдский','Курдский',170,0),(37,'ru-RU','Алеманнские диалекты немецкого','Алеманнские диалекты немецкого',171,0),(37,'ru-RU','Баварский диалект немецкого','Баварский диалект немецкого',172,0),(37,'ru-RU','Каннада','Каннада',173,0),(37,'ru-RU','Кечуа','Кечуа',174,0),(37,'ru-RU','Интерлингва','Интерлингва',175,0),(37,'ru-RU','Сунданский','Сунданский',176,0),(37,'ru-RU','Сорани','Сорани',177,0),(37,'ru-RU','Монгольский','Монгольский',178,0),(37,'ru-RU','Египетский диалект арабского','Египетский диалект арабского',179,0),(37,'ru-RU','Жемайтский диалект литовского','Жемайтский диалект литовского',180,0),(37,'ru-RU','Иранский вариант азербайджанского','Иранский вариант азербайджанского',181,0),(37,'ru-RU','Неаполитанский','Неаполитанский',182,0),(37,'ru-RU','Валлонский','Валлонский',183,0),(37,'ru-RU','Шотландский (кельтский)','Шотландский (кельтский)',184,0),(37,'ru-RU','Бугийский','Бугийский',185,0),(37,'ru-RU','Идиш','Идиш',186,0),(37,'ru-RU','Амхарский','Амхарский',187,0),(37,'ru-RU','Баньюмасанский','Баньюмасанский',188,0),(37,'ru-RU','Сингальский','Сингальский',189,0),(37,'ru-RU','Фарерский','Фарерский',190,0),(37,'ru-RU','Мазандеранский','Мазандеранский',191,0),(37,'ru-RU','Ория','Ория',192,0),(37,'ru-RU','Лимбургский','Лимбургский',193,0),(37,'ru-RU','Якутский','Якутский',194,0),(37,'ru-RU','Верхнелужицкий','Верхнелужицкий',195,0),(37,'ru-RU','Венетский','Венетский',196,0),(37,'ru-RU','Санскрит','Санскрит',197,0),(37,'ru-RU','Осетинский','Осетинский',198,0),(37,'ru-RU','Илоканский','Илоканский',199,0),(37,'ru-RU','Майтхили','Майтхили',200,0),(37,'ru-RU','Горномарийский','Горномарийский',201,0),(37,'ru-RU','Фиджийский хинди','Фиджийский хинди',202,0),(37,'ru-RU','Лугово-восточный марийский','Лугово-восточный марийский',203,0),(37,'ru-RU','Мегрельский','Мегрельский',204,0),(37,'ru-RU','Тарантинский','Тарантинский',205,0),(37,'ru-RU','Астекский','Астекский',206,0),(37,'ru-RU','Эмилиано-романьольский','Эмилиано-романьольский',207,0),(37,'ru-RU','Капампанганский','Капампанганский',208,0),(37,'ru-RU','Бходжпури','Бходжпури',209,0),(37,'ru-RU','Пушту','Пушту',210,0),(37,'ru-RU','Северный сото','Северный сото',211,0),(37,'ru-RU','Южный зазаки','Южный зазаки',212,0),(37,'ru-RU','Хакка','Хакка',213,0),(37,'ru-RU','Синдхи','Синдхи',214,0),(37,'ru-RU','Северносаамский','Северносаамский',215,0),(37,'ru-RU','Маори','Маори',216,0),(37,'ru-RU','Центральный бикольский','Центральный бикольский',217,0),(37,'ru-RU','Нижнесаксонский диалект нидерландского','Нижнесаксонский диалект нидерландского',218,0),(37,'ru-RU','Гань','Гань',219,0),(37,'ru-RU','Гилянский','Гилянский',220,0),(37,'ru-RU','Западнофламандский','Западнофламандский',221,0),(37,'ru-RU','Русинский','Русинский',222,0),(37,'ru-RU','Тибетский','Тибетский',223,0),(37,'ru-RU','Силезский','Силезский',224,0),(37,'ru-RU','Выруский','Выруский',225,0),(37,'ru-RU','Сардинский','Сардинский',226,0),(37,'ru-RU','Корсиканский','Корсиканский',227,0),(37,'ru-RU','Вепсский','Вепсский',228,0),(37,'ru-RU','Севернолурский','Севернолурский',229,0),(37,'ru-RU','Туркменский','Туркменский',230,0),(37,'ru-RU','Китайский (вэньянь)','Китайский (вэньянь)',231,0),(37,'ru-RU','Кашубский','Кашубский',232,0),(37,'ru-RU','Крымско-татарский','Крымско-татарский',233,0),(37,'ru-RU','Кхмерский','Кхмерский',234,0),(37,'ru-RU','Мэнский','Мэнский',235,0),(37,'ru-RU','Коми-зырянский','Коми-зырянский',236,0),(37,'ru-RU','Севернофризский','Севернофризский',237,0),(37,'ru-RU','Ассамский','Ассамский',238,0),(37,'ru-RU','Сефардский','Сефардский',239,0),(37,'ru-RU','Зеландский','Зеландский',240,0),(37,'ru-RU','Сомалийский','Сомалийский',241,0),(37,'ru-RU','Восточноминьский диалект китайского','Восточноминьский диалект китайского',242,0),(37,'ru-RU','Ачехский','Ачехский',243,0),(37,'ru-RU','Аймара','Аймара',244,0),(37,'ru-RU','Удмуртский','Удмуртский',245,0),(37,'ru-RU','Корнский','Корнский',246,0),(37,'ru-RU','Восточнофризский','Восточнофризский',247,0),(37,'ru-RU','Нормандский','Нормандский',248,0),(37,'ru-RU','Интерлингве','Интерлингве',249,0),(37,'ru-RU','Лезгинский','Лезгинский',250,0),(37,'ru-RU','Эрзянский','Эрзянский',251,0),(37,'ru-RU','Коми-пермяцкий','Коми-пермяцкий',252,0),(37,'ru-RU','Романшский','Романшский',253,0),(37,'ru-RU','Пикардский','Пикардский',254,0),(37,'ru-RU','Уйгурский','Уйгурский',255,0),(37,'ru-RU','Лигурийский','Лигурийский',256,0),(37,'ru-RU','Мальтийский','Мальтийский',257,0),(37,'ru-RU','Фриульский','Фриульский',258,0),(37,'ru-RU','Гуарани','Гуарани',259,0),(37,'ru-RU','Нижнелужицкий','Нижнелужицкий',260,0),(37,'ru-RU','Конкани','Конкани',261,0),(37,'ru-RU','Мальдивский','Мальдивский',262,0),(37,'ru-RU','Чабакано','Чабакано',263,0),(37,'ru-RU','Эстремадурский','Эстремадурский',264,0),(37,'ru-RU','Древнеанглийский','Древнеанглийский',265,0),(37,'ru-RU','Кабильский','Кабильский',266,0),(37,'ru-RU','Мирандский','Мирандский',267,0),(37,'ru-RU','Рипуарские диалекты немецкого','Рипуарские диалекты немецкого',268,0),(37,'ru-RU','Лингала','Лингала',269,0),(37,'ru-RU','Гагаузский','Гагаузский',270,0),(37,'ru-RU','Шона','Шона',271,0),(37,'ru-RU','Навахо','Навахо',272,0),(37,'ru-RU','Франкопровансальский','Франкопровансальский',273,0),(37,'ru-RU','Пангасинанский','Пангасинанский',274,0),(37,'ru-RU','Пали','Пали',275,0),(37,'ru-RU','Аварский','Аварский',276,0),(37,'ru-RU','Лаосский','Лаосский',277,0),(37,'ru-RU','Калмыцкий','Калмыцкий',278,0),(37,'ru-RU','Пфальцский диалект немецкого','Пфальцский диалект немецкого',279,0),(37,'ru-RU','Карачаево-балкарский','Карачаево-балкарский',280,0),(37,'ru-RU','Гавайский','Гавайский',281,0),(37,'ru-RU','Каракалпакский','Каракалпакский',282,0),(37,'ru-RU','Ливвиковский диалект карельского','Ливвиковский диалект карельского',283,0),(37,'ru-RU','Бурятский','Бурятский',284,0),(37,'ru-RU','Руанда','Руанда',285,0),(37,'ru-RU','Пенсильванский диалект немецкого','Пенсильванский диалект немецкого',286,0),(37,'ru-RU','Папьяменто','Папьяменто',287,0),(37,'ru-RU','Банджарский','Банджарский',288,0),(37,'ru-RU','Тонганский','Тонганский',289,0),(37,'ru-RU','Новиаль','Новиаль',290,0),(37,'ru-RU','Гренландский','Гренландский',291,0),(37,'ru-RU','Арамейский','Арамейский',292,0),(37,'ru-RU','Ямайский креольский','Ямайский креольский',293,0),(37,'ru-RU','Кабардино-черкесский','Кабардино-черкесский',294,0),(37,'ru-RU','Хауса','Хауса',295,0),(37,'ru-RU','Тетум','Тетум',296,0),(37,'ru-RU','Тувинский','Тувинский',297,0),(37,'ru-RU','Ток-писин','Ток-писин',298,0),(37,'ru-RU','Кикуйю','Кикуйю',299,0),(37,'ru-RU','Игбо','Игбо',300,0),(37,'ru-RU','Науруанский','Науруанский',301,0),(37,'ru-RU','Абхазский','Абхазский',302,0),(37,'ru-RU','Лакский','Лакский',303,0),(37,'ru-RU','Арумынский','Арумынский',304,0),(37,'ru-RU','Ложбан','Ложбан',305,0),(37,'ru-RU','Таитянский','Таитянский',306,0),(37,'ru-RU','Конго','Конго',307,0),(37,'ru-RU','Чжуанский','Чжуанский',308,0),(37,'ru-RU','Луганда','Луганда',309,0),(37,'ru-RU','Волоф','Волоф',310,0),(37,'ru-RU','Мокшанский','Мокшанский',311,0),(37,'ru-RU','Сранан-тонго','Сранан-тонго',312,0),(38,'ru-RU','базовый','базовый',313,0),(38,'ru-RU','читаю со словарем','читаю со словарем',314,0),(38,'ru-RU','свободно разговариваю','свободно разговариваю',315,0),(38,'ru-RU','родной язык','родной язык',316,0),(39,'ru-RU','деловая','деловая',317,0),(39,'ru-RU','путешествия','путешествия',318,0),(39,'ru-RU','экстрим','экстрим',319,0),(39,'ru-RU','спорт','спорт',320,0),(39,'ru-RU','weekend','weekend',321,0),(39,'ru-RU',' \"по интересам\"',' \"по интересам\"',322,0),(39,'ru-RU','фан-группы','фан-группы',323,0),(39,'ru-RU','волонтерство','волонтерство',324,0),(39,'ru-RU','автомобили','автомобили',325,0),(39,'ru-RU','паломничество','паломничество',326,0),(40,'ru-RU','конференция','конференция',327,0),(40,'ru-RU','бизнес/промышленная выставка','бизнес/промышленная выставка',328,0),(40,'ru-RU','ярмарка','ярмарка',329,0),(40,'ru-RU','другое','другое',330,0),(41,'ru-RU','туризм в далекие страны','туризм в далекие страны',331,0),(41,'ru-RU','путешествие (отель)','путешествие (отель)',332,0),(41,'ru-RU','поход (палаточный лагерь)','поход (палаточный лагерь)',333,0),(41,'ru-RU','археологические раскопки','археологические раскопки',334,0),(41,'ru-RU','другое','другое',335,0),(42,'ru-RU','конная','конная',336,0),(42,'ru-RU','байдарки','байдарки',337,0),(42,'ru-RU','велопробег','велопробег',338,0),(42,'ru-RU','дайвинг','дайвинг',339,0),(42,'ru-RU','скай-дайвинг','скай-дайвинг',340,0),(42,'ru-RU','серфинг','серфинг',341,0),(42,'ru-RU','скалолазание','скалолазание',342,0),(42,'ru-RU','другое','другое',343,0),(43,'ru-RU','совместные занятия йогой','совместные занятия йогой',344,0),(43,'ru-RU','Покататься на роликах ','Покататься на роликах ',345,0),(43,'ru-RU','Покататься на велосипеде','Покататься на велосипеде',346,0),(43,'ru-RU','Покататься на коньках','Покататься на коньках',347,0),(43,'ru-RU','Покататься на лыжах','Покататься на лыжах',348,0),(43,'ru-RU','поиграть в футбол','поиграть в футбол',349,0),(43,'ru-RU','поиграть в волейбол','поиграть в волейбол',350,0),(43,'ru-RU','поиграть в баскетбол','поиграть в баскетбол',351,0),(43,'ru-RU','борьба - тренировки','борьба - тренировки',352,0),(43,'ru-RU','бокс - тренировки','бокс - тренировки',353,0),(43,'ru-RU','самбо - тренировки','самбо - тренировки',354,0),(43,'ru-RU','каратэ - тренировки','каратэ - тренировки',355,0),(43,'ru-RU','бег','бег',356,0),(43,'ru-RU','фитнес','фитнес',357,0),(43,'ru-RU','плавание','плавание',358,0),(43,'ru-RU','теннис','теннис',359,0),(43,'ru-RU','бадминтон','бадминтон',360,0),(43,'ru-RU','танцы','танцы',361,0),(43,'ru-RU','устроить соревнования','устроить соревнования',362,0),(43,'ru-RU','другое','другое',363,0),(44,'ru-RU','прогулка','прогулка',364,0),(44,'ru-RU','экскурсия','экскурсия',365,0),(44,'ru-RU','концерт','концерт',366,0),(44,'ru-RU','кино','кино',367,0),(44,'ru-RU','театр','театр',368,0),(44,'ru-RU','аквапарк','аквапарк',369,0),(44,'ru-RU','пляжный отдых','пляжный отдых',370,0),(44,'ru-RU','галерея','галерея',371,0),(44,'ru-RU','выставка','выставка',372,0),(44,'ru-RU','другое','другое',373,0),(45,'ru-RU','светское мероприятие','светское мероприятие',374,0),(45,'ru-RU','баня','баня',375,0),(45,'ru-RU','рыбалка','рыбалка',376,0),(45,'ru-RU','шашлыки','шашлыки',377,0),(45,'ru-RU','Поедем отдохнуть с детьми!','Поедем отдохнуть с детьми!',378,0),(45,'ru-RU','аквапарк','аквапарк',379,0),(45,'ru-RU','другое','другое',380,0),(46,'ru-RU','квесты','квесты',381,0),(46,'ru-RU','народные праздники','народные праздники',382,0),(46,'ru-RU','шахматный турнир','шахматный турнир',383,0),(46,'ru-RU','бёрд-вотчинг','бёрд-вотчинг',384,0),(46,'ru-RU','другое','другое',385,0),(47,'ru-RU','футбол','футбол',386,0),(47,'ru-RU','баскетбол','баскетбол',387,0),(47,'ru-RU','волйбол','волйбол',388,0),(47,'ru-RU','хоккей','хоккей',389,0),(47,'ru-RU','другое','другое',390,0),(48,'ru-RU','совместная высадка деревьев','совместная высадка деревьев',391,0),(48,'ru-RU','сделаем наш район чистым (уборка)','сделаем наш район чистым (уборка)',392,0),(48,'ru-RU','посещение детских домов','посещение детских домов',393,0),(48,'ru-RU','посещение домов престарелых','посещение домов престарелых',394,0),(48,'ru-RU','сбор вещей нуждающимся','сбор вещей нуждающимся',395,0),(48,'ru-RU','сбор продуктов нуждающимся','сбор продуктов нуждающимся',396,0),(48,'ru-RU','приюты для животных','приюты для животных',397,0),(48,'ru-RU','поиск безвести пропавших','поиск безвести пропавших',398,0),(48,'ru-RU','поисковые отряды ВОВ','поисковые отряды ВОВ',399,0),(48,'ru-RU','Раскопки на местах боев ','Раскопки на местах боев ',400,0),(48,'ru-RU','Другое','Другое',401,0),(49,'ru-RU','ищу попутчика окупить бензин','ищу попутчика окупить бензин',402,0),(49,'ru-RU','ищу \"на ком поехать\"','ищу \"на ком поехать\"',403,0),(49,'ru-RU','автотуризм','автотуризм',404,0),(49,'ru-RU','Внедорожные 4х4 покатушки','Внедорожные 4х4 покатушки',405,0),(49,'ru-RU','Выходные в стиле Трофи','Выходные в стиле Трофи',406,0),(49,'ru-RU','Другое','Другое',407,0),(50,'ru-RU','Христианство','Христианство',408,0),(50,'ru-RU','Мусульманство','Мусульманство',409,0),(50,'ru-RU','Иудаизм','Иудаизм',410,0),(50,'ru-RU','Буддизм','Буддизм',411,0),(50,'ru-RU','Другое','Другое',412,0),(51,'ru-RU','палаточный лагерь','палаточный лагерь',413,0),(51,'ru-RU','хостел','хостел',414,0),(51,'ru-RU','апартаменты','апартаменты',415,0),(51,'ru-RU','квартира','квартира',416,0),(51,'ru-RU','шале','шале',417,0),(51,'ru-RU','коттедж','коттедж',418,0),(51,'ru-RU','отель 3 звезды','отель 3 звезды',419,0),(51,'ru-RU','отель 4 звезды','отель 4 звезды',420,0),(51,'ru-RU','отель 5 звезд','отель 5 звезд',421,0),(51,'ru-RU','другое','другое',422,0),(52,'ru-RU','RUR','RUR',423,0),(52,'ru-RU','EUR','EUR',424,0),(52,'ru-RU','USD','USD',425,0),(52,'ru-RU','CHF','CHF',426,0),(52,'ru-RU','GBP','GBP',427,0),(52,'ru-RU','JPY','JPY',428,0),(52,'ru-RU','UAH','UAH',429,0),(52,'ru-RU','KZT','KZT',430,0),(52,'ru-RU','TRY','TRY',431,0),(52,'ru-RU','CAD','CAD',432,0),(52,'ru-RU','AUD','AUD',433,0),(52,'ru-RU','INR','INR',434,0),(52,'ru-RU','другое','другое',435,0),(53,'ru-RU','автомобиль','автомобиль',436,0),(53,'ru-RU','самолет','самолет',437,0),(53,'ru-RU','поезд','поезд',438,0),(53,'ru-RU','пароход','пароход',439,0),(53,'ru-RU','пешком','пешком',440,0),(53,'ru-RU','метро','метро',441,0),(53,'ru-RU','автобус','автобус',442,0),(53,'ru-RU','трамвай','трамвай',443,0),(53,'ru-RU','троллейбус','троллейбус',444,0),(53,'ru-RU','верхом','верхом',445,0),(53,'ru-RU','велосипед','велосипед',446,0),(53,'ru-RU','собачья упряжка','собачья упряжка',447,0),(53,'ru-RU','лыжи','лыжи',448,0),(53,'ru-RU','вплавь','вплавь',449,0),(53,'ru-RU','другое','другое',450,0),(54,'ru-RU','деловая','деловая',451,0),(54,'ru-RU','экстрим','экстрим',452,0),(54,'ru-RU','спорт','спорт',453,0),(54,'ru-RU','совместный досуг','совместный досуг',454,0),(54,'ru-RU','weekend','weekend',455,0),(54,'ru-RU','\"по интересам\"','\"по интересам\"',456,0),(54,'ru-RU','фан-группы','фан-группы',457,0),(54,'ru-RU','волонтерство','волонтерство',458,0),(54,'ru-RU','автомобили','автомобили',459,0),(55,'ru-RU','на рассвете','на рассвете',460,0),(55,'ru-RU','рано утром','рано утром',461,0),(55,'ru-RU','днем','днем',462,0),(55,'ru-RU','вечером','вечером',463,0),(55,'ru-RU','при свете луны','при свете луны',464,0),(55,'ru-RU','другое','другое',465,0),(56,'ru-RU','курю много','курю много',466,0),(56,'ru-RU','курю изредка','курю изредка',467,0),(56,'ru-RU','не курю и не выношу, когда курят другие','не курю и не выношу, когда курят другие',468,0),(56,'ru-RU','не курю  ','не курю  ',469,0),(57,'ru-RU','люблю выпить','люблю выпить',470,0),(57,'ru-RU','пью в компаниях изредка','пью в компаниях изредка',471,0),(57,'ru-RU','пью только по праздникам','пью только по праздникам',472,0),(57,'ru-RU','не пью','не пью',473,0),(57,'ru-RU','не пью вообще и не выношу, когда пьют другие','не пью вообще и не выношу, когда пьют другие',474,0),(58,'ru-RU','хирург','хирург',475,0),(58,'ru-RU','следователь','следователь',476,0),(58,'ru-RU','дизайнер интерьера','дизайнер интерьера',477,0),(58,'ru-RU','пилот','пилот',478,0),(58,'ru-RU','нанотехнолог','нанотехнолог',479,0),(58,'ru-RU','космонавт','космонавт',480,0),(58,'ru-RU','корреспондент','корреспондент',481,0),(58,'ru-RU','генетик','генетик',482,0),(58,'ru-RU','астрофизик','астрофизик',483,0),(58,'ru-RU','частный детектив','частный детектив',484,0),(58,'ru-RU','палеонтолог','палеонтолог',485,0),(58,'ru-RU','судебный эксперт','судебный эксперт',486,0),(58,'ru-RU','тестер','тестер',487,0),(58,'ru-RU','режиссер','режиссер',488,0),(58,'ru-RU','продюсер','продюсер',489,0),(58,'ru-RU','фотограф','фотограф',490,0),(58,'ru-RU','архитектор','архитектор',491,0),(58,'ru-RU','спасатель','спасатель',492,0),(58,'ru-RU','реаниматолог','реаниматолог',493,0),(58,'ru-RU','инженер','инженер',494,0),(58,'ru-RU','международник','международник',495,0),(58,'ru-RU','патологоанатом','патологоанатом',496,0),(58,'ru-RU','инженер-конструктор','инженер-конструктор',497,0),(58,'ru-RU','астроном','астроном',498,0),(58,'ru-RU','инженер-технолог','инженер-технолог',499,0),(58,'ru-RU','архитектор-проектировщик','архитектор-проектировщик',500,0),(58,'ru-RU','микробиолог','микробиолог',501,0),(58,'ru-RU','пожарный','пожарный',502,0),(58,'ru-RU','кондитер','кондитер',503,0),(58,'ru-RU','судья','судья',504,0),(58,'ru-RU','реставратор','реставратор',505,0),(58,'ru-RU','медицинский технолог','медицинский технолог',506,0),(58,'ru-RU','ведущий радио и телевидения','ведущий радио и телевидения',507,0),(58,'ru-RU','химик-технолог','химик-технолог',508,0),(58,'ru-RU','pr-менеджер','pr-менеджер',509,0),(58,'ru-RU','эксперт','эксперт',510,0),(58,'ru-RU','лаборант химического анализа','лаборант химического анализа',511,0),(58,'ru-RU','дипломат','дипломат',512,0),(58,'ru-RU','актёр','актёр',513,0),(58,'ru-RU','инженер бюро технической инвентаризации','инженер бюро технической инвентаризации',514,0),(58,'ru-RU','руководитель','руководитель',515,0),(58,'ru-RU','повар','повар',516,0),(58,'ru-RU','зоолог','зоолог',517,0),(58,'ru-RU','переводчик','переводчик',518,0),(58,'ru-RU','художник','художник',519,0),(58,'ru-RU','дизайнер','дизайнер',520,0),(58,'ru-RU','шоколатье','шоколатье',521,0),(58,'ru-RU','биотехнолог','биотехнолог',522,0),(58,'ru-RU','звукорежиссер','звукорежиссер',523,0),(58,'ru-RU','биохимик','биохимик',524,0),(58,'ru-RU','web-программист','web-программист',525,0),(58,'ru-RU','геофизик','геофизик',526,0),(58,'ru-RU','инженер-строитель','инженер-строитель',527,0),(58,'ru-RU','медицинский лабораторный техник','медицинский лабораторный техник',528,0),(58,'ru-RU','актёр театра и кино','актёр театра и кино',529,0),(58,'ru-RU','скульптор','скульптор',530,0),(58,'ru-RU','mobile-разработчик','mobile-разработчик',531,0),(58,'ru-RU','маркетолог','маркетолог',532,0),(58,'ru-RU','дизайнер ландшафта','дизайнер ландшафта',533,0),(58,'ru-RU','водолаз','водолаз',534,0),(58,'ru-RU','пульмонолог','пульмонолог',535,0),(58,'ru-RU','специалист по системам безопасности','специалист по системам безопасности',536,0),(58,'ru-RU','кинолог','кинолог',537,0),(58,'ru-RU','военный лётчик','военный лётчик',538,0),(58,'ru-RU','исследователь','исследователь',539,0),(58,'ru-RU','резчик по камню','резчик по камню',540,0),(58,'ru-RU','биофизик','биофизик',541,0),(58,'ru-RU','дизайнер рекламных агентств','дизайнер рекламных агентств',542,0),(58,'ru-RU','сурдопереводчик','сурдопереводчик',543,0),(58,'ru-RU','врач','врач',544,0),(58,'ru-RU','инженер-проектировщик','инженер-проектировщик',545,0),(58,'ru-RU','психолог','психолог',546,0),(58,'ru-RU','арт-директор','арт-директор',547,0),(58,'ru-RU','web-дизайнер','web-дизайнер',548,0),(58,'ru-RU','химик','химик',549,0),(58,'ru-RU','певец','певец',550,0),(58,'ru-RU','озеленитель','озеленитель',551,0),(58,'ru-RU','ювелир','ювелир',552,0),(58,'ru-RU','врач-косметолог','врач-косметолог',553,0),(58,'ru-RU','вирусолог','вирусолог',554,0),(58,'ru-RU','эколог','эколог',555,0),(58,'ru-RU','фельдшер','фельдшер',556,0),(58,'ru-RU','прокурор','прокурор',557,0),(58,'ru-RU','системный программист','системный программист',558,0),(58,'ru-RU','авиационный инженер','авиационный инженер',559,0),(58,'ru-RU','биолог','биолог',560,0),(58,'ru-RU','модельер-конструктор','модельер-конструктор',561,0),(58,'ru-RU','художественный редактор','художественный редактор',562,0),(58,'ru-RU','композитор','композитор',563,0),(58,'ru-RU','специалист по управлению персоналом','специалист по управлению персоналом',564,0),(58,'ru-RU','педиатр','педиатр',565,0),(58,'ru-RU','диктор','диктор',566,0),(58,'ru-RU','инженер-испытатель','инженер-испытатель',567,0),(58,'ru-RU','монтажер телерадиовещательных компаний','монтажер телерадиовещательных компаний',568,0),(58,'ru-RU','видеооператор','видеооператор',569,0),(58,'ru-RU','художник-живописец','художник-живописец',570,0),(58,'ru-RU','учитель','учитель',571,0),(58,'ru-RU','инженер связи','инженер связи',572,0),(58,'ru-RU','программист-разработчик','программист-разработчик',573,0),(58,'ru-RU','техник по строительству и эксплуатации мостов','техник по строительству и эксплуатации мостов',574,0),(58,'ru-RU','другое','другое',575,0),(59,'ru-RU','<>','<>',576,0),(60,'ru-RU','<>','<>',577,0),(62,'ru-RU','уровень владения','уровень владения',578,0),(61,'ru-RU',' названия языка',' названия языка',579,0),(63,'ru-RU','Уровень владения','Уровень владения',580,0),(64,'ru-RU','Описание владения языком','Описание владения языком',581,0),(68,'ru-RU','Название кампании','Название кампании',582,0),(67,'ru-RU','Занимаемая должность','Занимаемая должность',583,0),(65,'ru-RU','Период от','Период от',584,0),(66,'ru-RU','Период до','Период до',585,0),(69,'ru-RU','Наименование учебного заведения','Наименование учебного заведения',586,0),(70,'ru-RU','Тип учебного заведения','Тип учебного заведения',587,0),(71,'ru-RU','Период до','Период до',588,0),(72,'ru-RU','Период от ','Период от ',589,0),(73,'ru-RU','возраст','возраст',590,NULL),(73,'ru-RU','пол','пол',591,31),(73,'ru-RU','знак зодиака','знак зодиака',592,32),(73,'ru-RU','семейное положение','семейное положение',593,33),(73,'ru-RU','дети','дети',594,34),(73,'ru-RU','количество детей','количество детей',595,NULL),(73,'ru-RU','образование','образование',596,35),(73,'ru-RU','религия','религия',597,36),(73,'ru-RU','знание языков','знание языков',598,37),(74,'ru-RU','Цель тест','Цель тест',599,NULL),(75,'ru-RU','Цель путешествия','Цель путешествия',600,NULL),(76,'ru-RU','Страна отправления','Страна отправления',601,NULL),(77,'ru-RU','Город отправления','Город отправления',602,NULL),(78,'ru-RU','Детали места отправления (адрес или объект)','Детали места отправления (адрес или объект)',603,NULL),(79,'ru-RU','Добавить мое положение на карте','Добавить мое положение на карте',604,NULL),(80,'ru-RU','Страна доставки','Страна доставки',605,NULL),(81,'ru-RU','Город доставки','Город доставки',606,NULL),(82,'ru-RU','Детали места доставки (адрес или объект)','Детали места доставки (адрес или объект)',607,NULL),(83,'ru-RU','Срочно','Срочно',608,NULL),(84,'ru-RU','Дата от','Дата от',609,NULL),(85,'ru-RU','Дата до','Дата до',610,NULL),(86,'ru-RU','Фамилия получатеоя','Фамилия получатеоя',611,NULL),(87,'ru-RU','Имя получателя','Имя получателя',612,NULL),(88,'ru-RU','Отчество получателя','Отчество получателя',613,NULL),(89,'ru-RU','Телефон получателя','Телефон получателя',614,NULL),(90,'ru-RU','Фамилия','Фамилия',615,NULL),(91,'ru-RU','Имя','Имя',616,NULL),(92,'ru-RU','Отчество','Отчество',617,NULL),(93,'ru-RU','телефон','телефон',618,NULL),(94,'ru-RU','Фамилия','Фамилия',619,NULL),(95,'ru-RU','Имя','Имя',620,NULL),(96,'ru-RU','Отчество','Отчество',621,NULL),(97,'ru-RU','телефон','телефон',622,NULL),(98,'ru-RU','Вознаграждение за услуги перевозки','Вознаграждение за услуги перевозки',623,NULL),(99,'ru-RU','Валюта ','Валюта ',624,NULL),(100,'ru-RU','Наличие залога в размере оплаты услуг ','Наличие залога в размере оплаты услуг ',625,NULL),(101,'ru-RU','Требование залога в размере оценочной стоимости посылки ','Требование залога в размере оценочной стоимости посылки ',626,NULL),(102,'ru-RU','Количество дней после даты отправки, после которых вещи можно считать утерянными','Количество дней после даты отправки, после которых вещи можно считать утерянными',627,NULL),(103,'ru-RU','мой контактный телефон','мой контактный телефон',628,NULL),(104,'ru-RU','комментарий','комментарий',629,NULL),(105,'ru-RU','прикрепить фото получателя','прикрепить фото получателя',630,NULL),(106,'ru-RU','Вес кг','Вес кг',631,NULL),(107,'ru-RU','Габариты см*см*см','Габариты см*см*см',632,NULL),(108,'ru-RU','категория','категория',633,NULL),(109,'ru-RU','цель','цель',634,NULL),(110,'ru-RU','другое','другое',635,NULL),(111,'ru-RU','бюджет от ','бюджет от ',636,NULL),(112,'ru-RU','бюджет до','бюджет до',637,NULL),(113,'ru-RU','Валюта','Валюта',638,NULL),(114,'ru-RU','когда (дата)','когда (дата)',639,NULL),(115,'ru-RU','примерная дата','примерная дата',640,NULL),(116,'ru-RU','когда (время - утро вечер день ночь)','когда (время - утро вечер день ночь)',641,NULL),(117,'ru-RU','можно с детьми','можно с детьми',642,NULL),(118,'ru-RU','Ожидаемый объем группы, человек','Ожидаемый объем группы, человек',643,NULL),(119,'ru-RU','плюс-минус человек','плюс-минус человек',644,NULL),(120,'ru-RU','Описание','Описание',645,NULL),(121,'ru-RU','я веду автомобиль ищу попутчика','я веду автомобиль ищу попутчика',646,NULL),(122,'ru-RU','иконки транспорта','иконки транспорта',647,NULL),(123,'ru-RU','в случае галочки моя машина - тип марка автомобиля','в случае галочки моя машина - тип марка автомобиля',648,NULL),(124,'ru-RU','вознаграждение от','вознаграждение от',649,NULL),(125,'ru-RU','вознаграждение до','вознаграждение до',650,NULL),(126,'ru-RU','Адрес куда','Адрес куда',651,NULL),(127,'ru-RU','Адрес откуда','Адрес откуда',652,NULL),(128,'ru-RU','время отправки','время отправки',653,NULL),(129,'ru-RU','время прибытия','время прибытия',654,NULL),(130,'ru-RU','категория','категория',655,NULL),(131,'ru-RU','цель','цель',656,NULL),(132,'ru-RU','на сколько дней','на сколько дней',657,NULL),(133,'ru-RU','тип проживания','тип проживания',658,NULL),(139,'ru-RU','Валюта','Валюта',659,NULL),(138,'ru-RU','стоимость','стоимость',660,NULL),(137,'ru-RU','Состав','Состав',661,NULL),(136,'ru-RU','Габариты','Хрупкое',662,NULL),(135,'ru-RU','Вес','Вес',663,NULL),(134,'ru-RU','Имя','Имя',664,NULL),(140,'ru-RU','прикрепить фотки','прикрепить фотки',665,NULL),(142,'ru-RU','прикрепить фото получателя','прикрепить фото получателя',666,NULL),(142,'ru-RU','прикрепить фото получателя','прикрепить фото получателя',667,NULL),(143,'ru-RU','Требования к кандидатам','Требования к кандидатам',668,NULL),(144,'ru-RU','характеристики','характеристики',669,NULL),(145,'ru-RU','Тип заявки','Тип заявки',670,NULL),(146,'ru-RU','Название ','Название ',671,NULL),(147,'ru-RU','Статус','Статус',672,NULL),(148,'ru-RU','Группа ','Группа ',673,NULL),(149,'ru-RU','Дата заявки','Дата заявки',674,NULL),(151,'ru-RU','Цена','Цена',675,NULL),(150,'ru-RU','Место','Место',676,NULL),(153,'ru-RU','Дата приезда','Дата приезда',677,NULL),(152,'ru-RU','Дата отъезда','Дата отъезда',678,NULL),(154,'ru-RU','требование залога на улуги перевозки','требование залога на улуги перевозки',679,NULL);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1487172619),('m130524_201442_init',1487172624),('m170222_050019_add_profile_users',1487838718),('m170223_131426_add_related_profile',1487856091),('m170224_113010_update_user',1487950190),('m170224_151415_applications',1487950474),('m170224_160412_applications_update',1488106791),('m170226_105709_update_user',1488106791),('m170304_113744_language',1488627940),('m170304_122013_language_inset',1488656591),('m170304_193747_update_user',1488656591),('m170319_154312_update_lang_table',1489938250),('m170319_163634_update_edu_table',1489941560),('m170319_164403_update_jobs_table',1489941873),('m170319_191823_update_apl',1489955461);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `package_parcels`
--

DROP TABLE IF EXISTS `package_parcels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_parcels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dimensions` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appl_id` int(11) DEFAULT NULL,
  `brittle` int(1) DEFAULT NULL,
  `composition` text COLLATE utf8_unicode_ci,
  `price` int(10) DEFAULT NULL,
  `currency` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package_parcels`
--

LOCK TABLES `package_parcels` WRITE;
/*!40000 ALTER TABLE `package_parcels` DISABLE KEYS */;
INSERT INTO `package_parcels` VALUES (1,'1','2','3',14,NULL,'4',1,427),(12,'1','1','1',15,NULL,'1',1,426),(13,'1','1','1',16,NULL,'1',1,426),(14,'1','1','1',17,NULL,'1',1,426),(15,'777','7','6',NULL,NULL,'5',4,427),(16,'34','2','2',NULL,NULL,'2',2,426);
/*!40000 ALTER TABLE `package_parcels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `referal_links`
--

DROP TABLE IF EXISTS `referal_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referal_links` (
  `id_referal_links` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(1) DEFAULT NULL,
  `hash_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `users_id_users` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_referal_links`),
  KEY `idx-referal_links-users_id_users` (`users_id_users`),
  CONSTRAINT `fk-referal_links-users_id_users` FOREIGN KEY (`users_id_users`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `referal_links`
--

LOCK TABLES `referal_links` WRITE;
/*!40000 ALTER TABLE `referal_links` DISABLE KEYS */;
INSERT INTO `referal_links` VALUES (10,1,'ZR66uystqsoe06Mw',10,'igors456727@yandex.ru','http://prop.local/site/signup?hash_link=ZR66uystqsoe06Mw&email=igors456727@yandex.ru'),(12,1,'3NXSXXgA7RfLFCg3',NULL,'1',NULL),(13,1,'L53CEKeTXAeRRxe2',NULL,'1',NULL),(14,1,'JVabO4aQ4p6jWGq3',5,'1',NULL),(15,1,'27iONK8w8WhkpTRf',5,'1',NULL),(16,1,'FsVvHiwcTQ4GOkaQ',5,'1','http://prop.local/site/signup?hash_link=FsVvHiwcTQ4GOkaQ');
/*!40000 ALTER TABLE `referal_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requirements_candidates`
--

DROP TABLE IF EXISTS `requirements_candidates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requirements_candidates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_list_person_characteristics` int(11) DEFAULT NULL,
  `id_sublist_person_characteristcs` int(11) DEFAULT NULL,
  `appl_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requirements_candidates`
--

LOCK TABLES `requirements_candidates` WRITE;
/*!40000 ALTER TABLE `requirements_candidates` DISABLE KEYS */;
/*!40000 ALTER TABLE `requirements_candidates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `source_message`
--

DROP TABLE IF EXISTS `source_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `source_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `source_message`
--

LOCK TABLES `source_message` WRITE;
/*!40000 ALTER TABLE `source_message` DISABLE KEYS */;
INSERT INTO `source_message` VALUES (1,'app','Yes'),(7,'app','Name'),(8,'app','Second Name'),(9,'app','Surname'),(10,'app','Birthday'),(11,'app','Sex'),(12,'app','Zodiac'),(13,'app','Marital Status'),(14,'app','Education'),(15,'app','Job'),(16,'app','Interest'),(17,'app','Experience'),(18,'app','Character'),(19,'app','Smoke'),(20,'app','Alchocol'),(21,'app','Foreign Language'),(22,'app','Social Net'),(23,'app','client_avatar'),(24,'app','client_background'),(25,'app','Religion'),(26,'app','Children'),(27,'app','Contries Id Contries'),(28,'app','Towns Id Towns'),(29,'app','Update'),(30,'app','Create'),(31,'app','list_gender'),(32,'app','list_zodiac'),(33,'app','list_marital_status'),(34,'app','list_children'),(35,'app','list_education'),(36,'app','list_religion'),(37,'app','list_language'),(38,'app','list_language_level'),(39,'app','list_category_travel'),(40,'app','business'),(41,'app','travel'),(42,'app','extreme'),(43,'app','sport'),(44,'app','time_together'),(45,'app','Weekend'),(46,'app','mutual_interests'),(47,'app','fans'),(48,'app','volunteers'),(49,'app','cars'),(50,'app','pilgrimage'),(51,'app','list_settlement_type'),(52,'app','list_currency'),(53,'app','list_transport'),(54,'app','list_category_time_together'),(55,'app','list_part_of_day'),(56,'app','list_smoking_attitude'),(57,'app','list_alcohol_attitude'),(58,'app','list_profession'),(59,'app','list_country'),(60,'app','sublist_city'),(61,'app','lang_id'),(62,'app','lang_level_id'),(63,'app','foreignlanguage_level'),(64,'app','foreignlanguage_description'),(65,'app','job_period_from'),(66,'app','job_period_to'),(67,'app','job_position'),(68,'app','job_name_campany'),(69,'app','education_name'),(70,'app','education_view'),(71,'app','education_period_to'),(72,'app','education_period_from'),(73,'app','list_person_characteristics'),(74,'app','sublist_purpose_time_together'),(75,'app','sublist_purpose_travel'),(76,'app','apl_list_country_from'),(77,'app','apl_sublist_city_from'),(78,'app','apl_details_place_from'),(79,'app','apl_add_map'),(80,'app','apl_list_country_to'),(81,'app','apl_sublist_city_to'),(82,'app','apl_details_place_to'),(83,'app','apl_urgently'),(84,'app','apl_date_from'),(85,'app','apl_date_to'),(86,'app','apl_surname_recipient'),(87,'app','apl_name_recipient'),(88,'app','apl_middle_name_recipient'),(89,'app','apl_phone_recipient'),(90,'app','apl_surname_recipient_contact1'),(91,'app','apl_name_recipient_contact1'),(92,'app','apl_middle_name_recipient_contact1'),(93,'app','apl_phone_recipient_contact1'),(94,'app','apl_surname_recipient_contact2'),(95,'app','apl_name_recipient_contact2'),(96,'app','apl_middle_name_recipient_contact2'),(97,'app','apl_phone_recipient_contact2'),(98,'app','apl_remuneration_transportation'),(99,'app','apl_currency'),(100,'app','apl_deposit'),(101,'app','apl_pledge_requirement_amount'),(102,'app','apl_number_days_after_date_dispatch'),(103,'app','apl_my_phone'),(104,'app','apl_comment'),(105,'app','apl_photo_recipient'),(106,'app','apl_weight'),(107,'app','apl_dimensions'),(108,'app','apl_list_category_time_together'),(109,'app','apl_sublist_purpose_time_together'),(110,'app','apl_other'),(111,'app','apl_budget_from'),(112,'app','apl_budget_to'),(113,'app','apl_list_currency'),(114,'app','apl_date_where'),(115,'app','apl_approximate_date'),(116,'app','apl_list_part_of_day'),(117,'app','apl_possible_children'),(118,'app','apl_count_people'),(119,'app','apl_plus_or_minus_person'),(120,'app','apl_description'),(121,'app','apl_drive_car'),(122,'app','apl_list_transport'),(123,'app','apl_my_car'),(124,'app','apl_remuneration_from'),(125,'app','apl_remuneration_to'),(126,'app','apl_adres_where'),(127,'app','apl_adres_whence'),(128,'app','apl_time_from'),(129,'app','apl_time_to'),(130,'app','apl_list_category_travel'),(131,'app','apl_sublist_purpose_travel'),(132,'app','apl_count_days'),(133,'app','apl_list_settlement_type'),(134,'app','packege_name'),(135,'app','packege_weight'),(136,'app','packege_dimensions'),(137,'app','packege_composition'),(138,'app','packege_price'),(139,'app','packege_currency'),(140,'app','apl_fotos'),(141,'app','foto_recipient'),(142,'app','apl_foto_recipient'),(143,'app','requirementsCandidates_id_list_person_characteristics'),(144,'app','requirementsCandidates_id_sublist_person_characteristcs'),(145,'app','Type Of Application Id Type Of Applications'),(146,'app','Title'),(147,'app','Status Id Status'),(148,'app','Groups Of Application Id Groups Of Applications'),(149,'app','Date action'),(150,'app','place'),(151,'app','price'),(152,'app','Date go to'),(153,'app','Date go cam back'),(154,'app','apl_requirement_pledge_transportation');
/*!40000 ALTER TABLE `source_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (0,'Создана'),(1,'Опубликована'),(2,'Удалена');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `target`
--

DROP TABLE IF EXISTS `target`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `target` (
  `id_target` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_target`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `target`
--

LOCK TABLES `target` WRITE;
/*!40000 ALTER TABLE `target` DISABLE KEYS */;
INSERT INTO `target` VALUES (3,'Тест'),(4,'Тест 2');
/*!40000 ALTER TABLE `target` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `towns`
--

DROP TABLE IF EXISTS `towns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `towns` (
  `id_towns` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contries_id_contries` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_towns`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `towns`
--

LOCK TABLES `towns` WRITE;
/*!40000 ALTER TABLE `towns` DISABLE KEYS */;
INSERT INTO `towns` VALUES (5,'Москва',1),(6,'Рязань',1),(7,'Питер',1);
/*!40000 ALTER TABLE `towns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_of_applications`
--

DROP TABLE IF EXISTS `type_of_applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_of_applications` (
  `id_type_of_application` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_type_of_application`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_of_applications`
--

LOCK TABLES `type_of_applications` WRITE;
/*!40000 ALTER TABLE `type_of_applications` DISABLE KEYS */;
INSERT INTO `type_of_applications` VALUES (1,'Передать посылку'),(2,'Купите и привезите мне'),(3,'Я еду, готов передать'),(4,'Проведем время вместе (событие)'),(5,'Едем вместе(попутчик в дорогу)'),(6,'Едем вместе(путешествие)');
/*!40000 ALTER TABLE `type_of_applications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `invite` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (5,'igor456727@yandex.ru','Sbcjckn3NtXp7sXpECtIjrNyF48tF6QS','$2y$13$9Y0.dRe5Ku9Qonq4s0Q5kOThek3dhZjgYLuXMS/F1xuXpxlfQ59Wa',NULL,'igor456727@yandex.ru',10,1487838773,1487838773,NULL),(8,'igor.morgachev.b@gmail.com','y3LdsU-2IPBOKAg8QatNv92S4fMdWVVA','$2y$13$p3zMr/lGDaQWuhoiln5UoejZwyKnWw9If/0BknNaNBuI.AC0IZ5.i',NULL,'igor.morgachev.b@gmail.com',0,1487847241,1487847241,NULL),(10,'igor4567237@yandex.ru','xjndeIENcnzd3bZdz9KdS9Yr4FW7odvY','$2y$13$cq3eGQAdWCnqXDzvwvfIreGMe9CU/gSrfitWYnj/9xMpspU93kTNm',NULL,'igor4567237@yandex.ru',10,1488109609,1488109609,5);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_profiles`
--

DROP TABLE IF EXISTS `user_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_profiles` (
  `id_user_profiles` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `second_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `zodiac` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marital_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `education` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `job` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interest` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `experience` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `character` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `smoke` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alchocol` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foreign_language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `social_net` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `religion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `children` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `users_id_users` int(11) DEFAULT NULL,
  `contries_id_contries` int(11) DEFAULT NULL,
  `towns_id_towns` int(11) DEFAULT NULL,
  `towns_contries_id_contries` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `birthday` date DEFAULT NULL,
  PRIMARY KEY (`id_user_profiles`),
  KEY `idx-user_profiles-users_id_users` (`users_id_users`),
  CONSTRAINT `fk-user_profiles-users_id_users` FOREIGN KEY (`users_id_users`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_profiles`
--

LOCK TABLES `user_profiles` WRITE;
/*!40000 ALTER TABLE `user_profiles` DISABLE KEYS */;
INSERT INTO `user_profiles` VALUES (1,'1','2','3',30,'36','46','1','d','8','9','10','466','472','','16','uploads/avatars/1488191133.png','uploads/background/1487863002.jpg','59','51',5,3,6,NULL,1487847241,1487847241,'2017-03-19'),(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL),(4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,NULL,NULL,NULL,1488109610,1488109610,NULL);
/*!40000 ALTER TABLE `user_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_has_users`
--

DROP TABLE IF EXISTS `users_has_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_has_users` (
  `users_id_users` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_has_users`
--

LOCK TABLES `users_has_users` WRITE;
/*!40000 ALTER TABLE `users_has_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_has_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-22  0:36:59
