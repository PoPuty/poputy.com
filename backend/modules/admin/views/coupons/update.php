<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Coupons\Coupons */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Coupons',
]) . $model->id_coupon;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coupons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_coupon, 'url' => ['view', 'id' => $model->id_coupon]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="coupons-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
