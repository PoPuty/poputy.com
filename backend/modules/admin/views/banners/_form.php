<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Banners\Banners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banners-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'file')->fileInput([]) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'articles_id_article')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(\app\modules\admin\models\ArticleItems\ArticleItems::find()->asArray()->all(), 'id', 'title'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    <?php
    echo $form->field($model, 'categories_id_category')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(\app\modules\admin\models\ArticleCategories\ArticleCategories::find()->asArray()->all(), 'id', 'name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
