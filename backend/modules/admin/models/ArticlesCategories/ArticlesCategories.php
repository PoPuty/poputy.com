<?php

namespace app\modules\admin\models\ArticlesCategories;

use Yii;

/**
 * This is the model class for table "articles_categories".
 *
 * @property integer $id
 * @property integer $catid
 * @property integer $artid
 */
class ArticlesCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articles_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catid', 'artid'], 'required'],
            [['catid', 'artid'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'catid' => Yii::t('app', 'Catid'),
            'artid' => Yii::t('app', 'Artid'),
        ];
    }
}
