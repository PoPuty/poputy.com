<?php

namespace app\modules\admin\models\Tags;

use Yii;

/**
 * This is the model class for table "tags".
 *
 * @property integer $id_tag
 * @property string $name
 * @property string $title
 * @property string $description
 * @property string $keywords
 *
 * @property ArticleItems[] $articleItems
 */
class Tags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'name_ru', 'name_en', 'name_fi', 'title_ru', 'title_fi', 'title_en', 'title'], 'string', 'max' => 255],
            [['description', 'keywords', 'description_ru', 'keywords_ru', 'description_en', 'keywords_en', 'description_fi', 'keywords_fi'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tag' => Yii::t('app', 'Id Tag'),
            'name' => Yii::t('app', 'Name'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'keywords' => Yii::t('app', 'Keywords'),
            'name_en' => Yii::t('app', 'Name_en'),
            'title_en' => Yii::t('app', 'Title_en'),
            'description_en' => Yii::t('app', 'Description_en'),
            'keywords_en' => Yii::t('app', 'Keywords_en'),
            'name_ru' => Yii::t('app', 'Name_ru'),
            'title_ru' => Yii::t('app', 'Title_ru'),
            'description_ru' => Yii::t('app', 'Description_ru'),
            'keywords_ru' => Yii::t('app', 'Keywords_ru'),
            'name_fi' => Yii::t('app', 'Name_fi'),
            'title_fi' => Yii::t('app', 'Title_fi'),
            'description_fi' => Yii::t('app', 'Description_fi'),
            'keywords_fi' => Yii::t('app', 'Keywords_fi'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleItems()
    {
        return $this->hasMany(ArticleItems::className(), ['tags_id_tag' => 'id_tag']);
    }
}
