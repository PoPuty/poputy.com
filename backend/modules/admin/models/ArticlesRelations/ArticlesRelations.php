<?php

namespace app\modules\admin\models\ArticlesRelations;

use Yii;
use app\modules\admin\models\ArticleItems\ArticleItems;
/**
 * This is the model class for table "articles_relations".
 *
 * @property integer $id
 * @property integer $id_article
 * @property integer $id_article_relations
 *
 * @property ArticleItems $idArticleRelations
 * @property ArticleItems $idArticle
 */
class ArticlesRelations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articles_relations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_article', 'id_article_relations'], 'integer'],
            [['id_article_relations'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleItems::className(), 'targetAttribute' => ['id_article_relations' => 'id']],
            [['id_article'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleItems::className(), 'targetAttribute' => ['id_article' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_article' => Yii::t('app', 'Id Article'),
            'id_article_relations' => Yii::t('app', 'Id Article Relations'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdArticleRelations()
    {
        return $this->hasOne(ArticleItems::className(), ['id' => 'id_article_relations']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdArticle()
    {
        return $this->hasOne(ArticleItems::className(), ['id' => 'id_article']);
    }
}
