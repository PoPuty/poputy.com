<?php

namespace app\modules\admin\models\Banners;

use  app\modules\admin\models\ArticleItems\ArticleItems;
use   app\modules\admin\models\ArticleCategories\ArticleCategories;
use Yii;

/**
 * This is the model class for table "banners".
 *
 * @property integer $id_banner
 * @property string $photo
 * @property string $link
 * @property integer $articles_id_article
 * @property integer $categories_id_category
 *
 * @property ArticleCategories $categoriesIdCategory
 * @property ArticleItems $articlesIdArticle
 */
class Banners extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file',], 'file' , 'skipOnEmpty' => true,  'extensions' => 'pdf, jpeg, jpg'],
            [['articles_id_article', 'categories_id_category'], 'required'],
            [['articles_id_article', 'categories_id_category'], 'integer'],
            [['photo', 'link'], 'string', 'max' => 255],
            [['categories_id_category'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleCategories::className(), 'targetAttribute' => ['categories_id_category' => 'id']],
            [['articles_id_article'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleItems::className(), 'targetAttribute' => ['articles_id_article' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_banner' => Yii::t('app', 'Id Banner'),
            'photo' => Yii::t('app', 'Photo'),
            'link' => Yii::t('app', 'Link'),
            'articles_id_article' => Yii::t('app', 'Articles Id Article'),
            'categories_id_category' => Yii::t('app', 'Categories Id Category'),
            'file' => Yii::t('app', 'File banners'),
        ];
    }

    public function beforeSave($insert)
    {
        $this->file = \yii\web\UploadedFile::getInstance($this, 'file');

        if($this->file)
        {
            
            $time = time();
            $this->file->saveAs('uploads/banners/' . $time . '.' . $this->file->extension);
            $this->photo = '/uploads/banners/' . $time . '.' . $this->file->extension;

        }
        if (parent::beforeSave($insert)) {


            return true;
        }

    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriesIdCategory()
    {
        return $this->hasOne(ArticleCategories::className(), ['id' => 'categories_id_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticlesIdArticle()
    {
        return $this->hasOne(ArticleItems::className(), ['id' => 'articles_id_article']);
    }
}
