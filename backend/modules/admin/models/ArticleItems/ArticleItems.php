<?php

namespace app\modules\admin\models\ArticleItems;

use Yii;

/**
 * This is the model class for table "article_items".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property integer $catid
 * @property integer $userid
 * @property string $introtext
 * @property string $fulltext
 * @property integer $state
 * @property string $access
 * @property string $language
 * @property string $theme
 * @property integer $ordering
 * @property integer $hits
 * @property string $image
 * @property string $image_caption
 * @property string $image_credits
 * @property string $video
 * @property string $video_type
 * @property string $video_caption
 * @property string $video_credits
 * @property string $created
 * @property integer $created_by
 * @property string $modified
 * @property integer $modified_by
 * @property string $params
 * @property string $metadesc
 * @property string $metakey
 * @property string $robots
 * @property string $author
 * @property string $copyright
 *
 * @property Banners[] $banners
 * @property BayArticles[] $bayArticles
 */
class ArticleItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'alias', 'access', 'language'], 'required'],
            [['catid', 'userid', 'state', 'ordering', 'hits', 'created_by', 'modified_by'], 'integer'],
            [['introtext', 'fulltext', 'image', 'video', 'params', 'metadesc', 'metakey'], 'string'],
            [['created', 'modified'], 'safe'],
            [['title', 'alias', 'image_caption', 'image_credits', 'video_caption', 'video_credits'], 'string', 'max' => 255],
            [['access'], 'string', 'max' => 64],
            [['language'], 'string', 'max' => 7],
            [['theme'], 'string', 'max' => 12],
            [['video_type', 'robots'], 'string', 'max' => 20],
            [['author', 'copyright'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'alias' => Yii::t('app', 'Alias'),
            'catid' => Yii::t('app', 'Catid'),
            'userid' => Yii::t('app', 'Userid'),
            'introtext' => Yii::t('app', 'Introtext'),
            'fulltext' => Yii::t('app', 'Fulltext'),
            'state' => Yii::t('app', 'State'),
            'access' => Yii::t('app', 'Access'),
            'language' => Yii::t('app', 'Language'),
            'theme' => Yii::t('app', 'Theme'),
            'ordering' => Yii::t('app', 'Ordering'),
            'hits' => Yii::t('app', 'Hits'),
            'image' => Yii::t('app', 'Image'),
            'image_caption' => Yii::t('app', 'Image Caption'),
            'image_credits' => Yii::t('app', 'Image Credits'),
            'video' => Yii::t('app', 'Video'),
            'video_type' => Yii::t('app', 'Video Type'),
            'video_caption' => Yii::t('app', 'Video Caption'),
            'video_credits' => Yii::t('app', 'Video Credits'),
            'created' => Yii::t('app', 'Created'),
            'created_by' => Yii::t('app', 'Created By'),
            'modified' => Yii::t('app', 'Modified'),
            'modified_by' => Yii::t('app', 'Modified By'),
            'params' => Yii::t('app', 'Params'),
            'metadesc' => Yii::t('app', 'Metadesc'),
            'metakey' => Yii::t('app', 'Metakey'),
            'robots' => Yii::t('app', 'Robots'),
            'author' => Yii::t('app', 'Author'),
            'copyright' => Yii::t('app', 'Copyright'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanners()
    {
        return $this->hasMany(Banners::className(), ['articles_id_article' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBayArticles()
    {
        return $this->hasMany(BayArticles::className(), ['articles_id_article' => 'id']);
    }
}
