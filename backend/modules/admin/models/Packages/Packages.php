<?php

namespace app\modules\admin\models\Packages;

use Yii;

/**
 * This is the model class for table "packages".
 *
 * @property integer $id_package
 * @property string $name
 * @property integer $limetime
 * @property string $price
 *
 * @property BayArticles[] $bayArticles
 * @property PackagesHasUsers[] $packagesHasUsers
 */
class Packages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'packages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['livetime'], 'string', 'max' => 255],
            [['name', 'price'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_package' => Yii::t('app', 'Id Package'),
            'name' => Yii::t('app', 'Name'),
            'livetime' => Yii::t('app', 'Livetime'),
            'price' => Yii::t('app', 'Price'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBayArticles()
    {
        return $this->hasMany(BayArticles::className(), ['packages_id_package' => 'id_package']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackagesHasUsers()
    {
        return $this->hasMany(PackagesHasUsers::className(), ['packages_id_package' => 'id_package']);
    }
}
