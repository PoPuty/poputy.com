<?php

namespace app\modules\admin\models\Coupons;

use Yii;

/**
 * This is the model class for table "coupons".
 *
 * @property integer $id_coupon
 * @property string $hash
 * @property integer $created
 * @property integer $livetime
 *
 * @property User[] $users
 */
class Coupons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coupons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created', 'livetime'], 'required'],
            [['created', 'livetime'], 'string', 'max' => 255],
            [['hash'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_coupon' => Yii::t('app', 'Id Coupon'),
            'hash' => Yii::t('app', 'Hash'),
            'created' => Yii::t('app', 'Created'),
            'livetime' => Yii::t('app', 'Livetime'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['coupons_id_coupon' => 'id_coupon']);
    }
}
