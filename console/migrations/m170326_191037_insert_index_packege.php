<?php

use yii\db\Migration;

class m170326_191037_insert_index_packege extends Migration
{
    public function up()
    {

        $this->createIndex(
            'idx-bay_articles-articles_id_article',
            'bay_articles',
            'articles_id_article'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-bay_articles-articles_id_article',
            'bay_articles',
            'articles_id_article',
            'article_items',
            'id',
            'CASCADE'
        );


        $this->createIndex(
            'idx-bay_articles-packages_id_package',
            'bay_articles',
            'packages_id_package'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-bay_articles-packages_id_package',
            'bay_articles',
            'packages_id_package',
            'packages',
            'id_package',
            'CASCADE'
        );

        $this->createIndex(
            'idx-bay_articles-users_id_user',
            'bay_articles',
            'users_id_user'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-bay_articles-users_id_user',
            'bay_articles',
            'users_id_user',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-packages_has_users-users_id_user',
            'packages_has_users',
            'users_id_user'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-packages_has_users-users_id_user',
            'packages_has_users',
            'users_id_user',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-packages_has_users-packages_id_package',
            'packages_has_users',
            'packages_id_package'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-packages_has_users-packages_id_package',
            'packages_has_users',
            'packages_id_package',
            'packages',
            'id_package',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m170326_191037_insert_index_packege cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
