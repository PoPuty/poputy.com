<?php

use yii\db\Migration;

class m170304_113744_language extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('source_message', [
            'id' => $this->primaryKey(),
            'category' => $this->string(255),
            'message' => $this->text(),
        ], $tableOptions);

        $this->createTable('message', [
            'id' => $this->integer(11),
            'id_pr' => $this->primaryKey(),
            'language' => $this->string(255),
            'translation' => $this->text(),
            'message' => $this->text(),
        ], $tableOptions);



        // add foreign key for table `user`
        $this->addForeignKey(
            'fk_message_source_message',
            'message',
            'id',
            'source_message',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m170304_113744_language cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
