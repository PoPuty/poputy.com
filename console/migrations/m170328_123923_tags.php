<?php

use yii\db\Migration;

class m170328_123923_tags extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('tags', [
            'id_tag' => $this->primaryKey(),
            'name' => $this->string(255),
            'title' => $this->string(255),
            'description' => $this->string(255),
            'keywords' => $this->string(255),
        ], $tableOptions);

        $this->addColumn('article_items', 'tags_id_tag', 'int(11)');
        $this->createIndex(
            'idx-article_items-tags_id_tag',
            'article_items',
            'tags_id_tag'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-article_items-tags_id_tag',
            'article_items',
            'tags_id_tag',
            'tags',
            'id_tag',
            'CASCADE'
        );

    }

    public function down()
    {
        echo "m170328_123923_tags cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
