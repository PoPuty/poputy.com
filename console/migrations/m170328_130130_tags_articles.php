<?php

use yii\db\Migration;

class m170328_130130_tags_articles extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('articles_tags', [
            'id' => $this->primaryKey(),
            'id_tag' => $this->integer(11),
            'id_article' => $this->integer(11),
        ], $tableOptions);
    }

    public function down()
    {
        echo "m170328_130130_tags_articles cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
