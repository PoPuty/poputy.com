<?php

use yii\db\Migration;

class m170222_050019_add_profile_users extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('user_profiles', [
            'id_user_profiles' => $this->primaryKey(),
            'name' => $this->string(255),
            'second_name' => $this->string(255),
            'surname' => $this->string(255),
            'sex' => $this->integer(1),
            'zodiac' => $this->string(255),
            'marital_status' => $this->string(255),
            'education' => $this->string(255),
            'job' => $this->string(255),
            'interest' => $this->string(1024),
            'experience' => $this->string(1024),
            'character' => $this->string(1024),
            'smoke' => $this->string(10),
            'alchocol' => $this->string(10),
            'birthday' => $this->string(255),
            'foreign_language' => $this->string(255),
            'social_net' => $this->string(255),
            'photo' => $this->string(255),
            'background' => $this->string(255),
            'religion' => $this->string(255),
            'children' => $this->string(255),
            'users_id_users' => $this->integer(11),
            'contries_id_contries' => $this->integer(11),
            'towns_id_towns' => $this->integer(11),
            'towns_contries_id_contries' => $this->integer(11),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('towns', [
            'id_towns' => $this->primaryKey(),
            'name' => $this->string(255),
            'contries_id_contries' => $this->integer(11),
        ], $tableOptions);

        $this->createTable('countries', [
            'id_countries' => $this->primaryKey(),
            'name' => $this->string(255),
            'id_countries_pr' => $this->integer(11),
        ], $tableOptions);

        $this->createTable('users_has_users', [
            'users_id_users' => $this->primaryKey(),
            'users_id_users' => $this->integer(11),
        ], $tableOptions);

        $this->createTable('referal_links', [
            'id_referal_links' => $this->primaryKey(),
            'active' => $this->integer(1),
            'hash_link' => $this->string(255),
            'users_id_users' => $this->integer(11),
        ], $tableOptions);







        $this->createIndex(
            'idx-user_profiles-users_id_users',
            'user_profiles',
            'users_id_users'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_profiles-users_id_users',
            'user_profiles',
            'users_id_users',
            'user',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-referal_links-users_id_users',
            'referal_links',
            'users_id_users'
        );
        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-referal_links-users_id_users',
            'referal_links',
            'users_id_users',
            'user',
            'id',
            'CASCADE'
        );


        /* $this->createIndex(
             'idx-countries-id_countries_pr',
             'countries',
             'id_countries_pr'
         );
         // add foreign key for table `user`
         $this->addForeignKey(
             'fk-countries-id_countries_pr',
             'countries',
             'id_countries_pr',
             'user_profiles',
             'contries_id_contries',
             'CASCADE'
         );
         $this->createIndex(
             'idx-towns-id_towns',
             'towns',
             'id_towns'
         );

         // add foreign key for table `user`
         $this->addForeignKey(
             'fk-towns-id_towns',
             'towns',
             'id_towns',
             'user_profiles',
             'towns_id_towns',
             'CASCADE'
         );*/
    }

    public function down()
    {
        echo "m170222_050019_add_profile_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
