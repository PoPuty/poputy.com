<?php

use yii\db\Migration;

class m170326_190149_insert_table_packege extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('bay_articles', [
            'id_bay_articles' => $this->primaryKey(),
            'articles_id_article' => $this->integer()->notNull(),
            'packages_id_package' => $this->integer()->notNull(),
            'users_id_user' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('packages', [
            'id_package' => $this->primaryKey(),
            'name' => $this->string(255),
            'limetime' => $this->string(255),
            'price' => $this->string(255),
        ], $tableOptions);

        $this->createTable('packages_has_users', [
            'packages_id_package' => $this->integer()->notNull(),
            'users_id_user' => $this->integer()->notNull(),
            'bay_date' => $this->string(255),
            'finish_date' => $this->string(255),
        ], $tableOptions);

    }

    public function down()
    {
        echo "m170326_190149_insert_table_packege cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
