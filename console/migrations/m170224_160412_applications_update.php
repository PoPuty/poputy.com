<?php

use yii\db\Migration;

class m170224_160412_applications_update extends Migration
{
    public function up()
    {
        $this->addColumn('appliacations', 'created_at', 'int(11)');
        $this->addColumn('appliacations', 'price', 'int(11)');
        $this->addColumn('appliacations', 'place', 'varchar(255)');
        $this->addColumn('appliacations', 'date_action', 'varchar(255)');

        $this->insert('type_of_applications', ['name' => 'Нужно передать посылку',]);
        $this->insert('type_of_applications', ['name' => 'Готов прихватить вещи',]);
        $this->insert('type_of_applications', ['name' => 'Ищу попутчиков/набираю группу (идея)',]);
        $this->insert('type_of_applications', ['name' => 'Ищу попутчиков/набираю группу (реальный набор)',]);

        $this->insert('status', ['name' => 'Создана',]);
        $this->insert('status', ['name' => 'Опубликована',]);
        $this->insert('status', ['name' => 'Удалена',]);

        $this->insert('countries', ['name' => 'Россия']);
        $this->insert('towns', ['name' => 'Москва', 'contries_id_contries' => 1]);
        $this->insert('towns', ['name' => 'Рязань', 'contries_id_contries' => 1]);
        $this->insert('towns', ['name' => 'Питер', 'contries_id_contries' => 1]);

        $this->insert('target', ['name' => 'Тест']);
        $this->insert('target', ['name' => 'Тест 2']);

        $this->insert('groups_of_applications', ['name' => 'Группа 1', 'target_id_target' => 1]);
        $this->insert('groups_of_applications', ['name' => 'Группа 2', 'target_id_target' => 1]);
        $this->insert('groups_of_applications', ['name' => 'Группа 3', 'target_id_target' => 2]);
    }

    public function down()
    {
        echo "m170224_160412_applications_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
