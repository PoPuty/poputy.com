<?php

use yii\db\Migration;

class m170319_154312_update_lang_table extends Migration
{
    public function up()
    {
        $this->addColumn('foreign_language', 'list_language', 'int(11)');
        $this->addColumn('foreign_language', 'list_language_level', 'int(11)');
    }

    public function down()
    {
        echo "m170319_154312_update_lang_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
