<?php

use yii\db\Migration;

class m170328_125849_tags_articles extends Migration
{
    public function up()
    {
        $this->addColumn('article_items', 'tags', 'varchar(1000)');
    }

    public function down()
    {
        echo "m170328_125849_tags_articles cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
