<?php

use yii\db\Migration;

class m170319_164403_update_jobs_table extends Migration
{
    public function up()
    {
        $this->addColumn('jobs', 'period_from', 'varchar(255)');
        $this->addColumn('jobs', 'period_to', 'varchar(255)');
    }

    public function down()
    {
        echo "m170319_164403_update_jobs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
