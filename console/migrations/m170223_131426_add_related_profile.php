<?php

use yii\db\Migration;

class m170223_131426_add_related_profile extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('educations', [
            'id' => $this->primaryKey(),
            'description' => $this->string(255),
            'user_profile_id' => $this->integer(11),
        ], $tableOptions);

        $this->createTable('jobs', [
            'id' => $this->primaryKey(),
            'description' => $this->string(255),
            'user_profile_id' => $this->integer(11),
        ], $tableOptions);

        $this->createTable('foreign_language', [
            'id' => $this->primaryKey(),
            'description' => $this->string(255),
            'user_profile_id' => $this->integer(11),
        ], $tableOptions);

        $this->createIndex(
            'idx-educations-user_profile_id',
            'educations',
            'user_profile_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-educations-user_profile_id',
            'educations',
            'user_profile_id',
            'user_profiles',
            'id_user_profiles',
            'CASCADE'
        );
        $this->createIndex(
            'idx-jobs-user_profile_id',
            'jobs',
            'user_profile_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-jobs-user_profile_id',
            'jobs',
            'user_profile_id',
            'user_profiles',
            'id_user_profiles',
            'CASCADE'
        );
        $this->createIndex(
            'idx-foreign_language-user_profile_id',
            'foreign_language',
            'user_profile_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-foreign_language-user_profile_id',
            'foreign_language',
            'user_profile_id',
            'user_profiles',
            'id_user_profiles',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m170223_131426_add_related_profile cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
