<?php

use yii\db\Migration;

class m170224_151415_applications extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('appliacations', [
            'id_appliacation' => $this->primaryKey(),
            'title' => $this->string(255),
            'users_id_users' => $this->integer(11),
            'type_of_application_id_type_of_applications' => $this->integer(11),
            'all_paramentres' => $this->string(255),
            'status_id_status' => $this->integer(11),
            'groups_of_application_id_groups_of_applications' => $this->integer(11),
            'target_id_target' => $this->integer(11),
        ], $tableOptions);

        $this->createTable('status', [
            'id_status' => $this->primaryKey(),
            'name' => $this->string(255),

        ], $tableOptions);

        $this->createTable('type_of_applications', [
            'id_type_of_application' => $this->primaryKey(),
            'name' => $this->string(45),

        ], $tableOptions);

        $this->createTable('groups_of_applications', [
            'id_groups_of_applications' => $this->primaryKey(),
            'name' => $this->string(45),
            'target_id_target' => $this->integer(11)
        ], $tableOptions);

        $this->createTable('target', [
            'id_target' => $this->primaryKey(),
            'name' => $this->string(255),

        ], $tableOptions);


        $this->createIndex(
            'idx-appliacations-users_id_users',
            'appliacations',
            'users_id_users'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-appliacations-users_id_users',
            'appliacations',
            'users_id_users',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-appliacations-type_of_application_id_type_of_applications',
            'appliacations',
            'type_of_application_id_type_of_applications'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-appliacations-type_of_application_id_type_of_applications',
            'appliacations',
            'type_of_application_id_type_of_applications',
            'type_of_applications',
            'id_type_of_application',
            'CASCADE'
        );

        $this->createIndex(
            'idx-appliacations-status_id_status',
            'appliacations',
            'status_id_status'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-appliacations-status_id_status',
            'appliacations',
            'status_id_status',
            'status',
            'id_status',
            'CASCADE'
        );

        $this->createIndex(
            'idx-appliacations-groups_of_application_id',
            'appliacations',
            'groups_of_application_id_groups_of_applications'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-appliacations-groups_of_application_id',
            'appliacations',
            'groups_of_application_id_groups_of_applications',
            'groups_of_applications',
            'id_groups_of_applications',
            'CASCADE'
        );

        $this->createIndex(
            'idx-appliacations-target_id_target',
            'appliacations',
            'target_id_target'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-appliacations-target_id_target',
            'appliacations',
            'target_id_target',
            'target',
            'id_target',
            'CASCADE'
        );

        $this->createIndex(
            'idx-groups_of_applications-target_id_target',
            'groups_of_applications',
            'target_id_target'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-appliacations-target_id_targets',
            'groups_of_applications',
            'target_id_target',
            'target',
            'id_target',
            'CASCADE'
        );
    }


    public function down()
    {
        echo "m170224_151415_applications cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
