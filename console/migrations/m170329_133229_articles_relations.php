<?php

use yii\db\Migration;

class m170329_133229_articles_relations extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('articles_relations', [
            'id' => $this->primaryKey(),
            'id_article' => $this->integer(11),
            'id_article_relations' => $this->integer(11),
        ], $tableOptions);

        $this->createIndex(
            'idx-articles_relations-id_article',
            'articles_relations',
            'id_article'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-articles_relations-id_article',
            'articles_relations',
            'id_article',
            'article_items',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-articles_relations-id_article_relations',
            'articles_relations',
            'id_article_relations'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-articles_relations-id_article_relations',
            'articles_relations',
            'id_article_relations',
            'article_items',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m170329_133229_articles_relations cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
