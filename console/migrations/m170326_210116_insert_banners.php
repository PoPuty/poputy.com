<?php

use yii\db\Migration;

class m170326_210116_insert_banners extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('banners', [
            'id_banner' => $this->primaryKey(),
            'photo' => $this->string(255),
            'link' => $this->string(255),
            'articles_id_article' => $this->integer()->notNull(),
            'categories_id_category' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createIndex(
            'idx-banners-articles_id_article',
            'banners',
            'articles_id_article'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-banners-articles_id_article',
            'banners',
            'articles_id_article',
            'article_items',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-banners-categories_id_category',
            'banners',
            'categories_id_category'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-banners-categories_id_category',
            'banners',
            'categories_id_category',
            'article_categories',
            'id',
            'CASCADE'
        );


    }

    public function down()
    {
        echo "m170326_210116_insert_banners cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
