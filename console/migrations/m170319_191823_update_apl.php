<?php

use yii\db\Migration;

class m170319_191823_update_apl extends Migration
{
    public function up()
    {
        $this->addColumn('appliacations', 'list_country_from', 'int(11)');
        $this->addColumn('appliacations', 'sublist_city_from', 'int(11)');
        $this->addColumn('appliacations', 'details_place_from', 'text');
        $this->addColumn('appliacations', 'add_map', 'int(1)');
        $this->addColumn('appliacations', 'requirement_pledge_transportation', 'int(1)');
        $this->addColumn('appliacations', 'list_country_to', 'int(11)');
        $this->addColumn('appliacations', 'sublist_city_to', 'int(11)');
        $this->addColumn('appliacations', 'details_place_to', 'text');
        $this->addColumn('appliacations', 'urgently', 'int(1)');
        $this->addColumn('appliacations', 'date_from', 'varchar(255)');
        $this->addColumn('appliacations', 'date_to', 'varchar(255)');
        $this->addColumn('appliacations', 'surname_recipient', 'varchar(255)');
        $this->addColumn('appliacations', 'name_recipient', 'varchar(255)');
        $this->addColumn('appliacations', 'middle_name_recipient', 'varchar(255)');
        $this->addColumn('appliacations', 'phone_recipient', 'varchar(255)');
        $this->addColumn('appliacations', 'surname_recipient_contact1', 'varchar(255)');
        $this->addColumn('appliacations', 'name_recipient_contact1', 'varchar(255)');
        $this->addColumn('appliacations', 'middle_name_recipient_contact1', 'varchar(255)');
        $this->addColumn('appliacations', 'phone_recipient_contact1', 'varchar(255)');
        $this->addColumn('appliacations', 'surname_recipient_contact2', 'varchar(255)');
        $this->addColumn('appliacations', 'name_recipient_contact2', 'varchar(255)');
        $this->addColumn('appliacations', 'middle_name_recipient_contact2', 'varchar(255)');
        $this->addColumn('appliacations', 'phone_recipient_contact2', 'varchar(255)');
        $this->addColumn('appliacations', 'remuneration_transportation', 'varchar(255)');
        $this->addColumn('appliacations', 'currency', 'varchar(255)');
        $this->addColumn('appliacations', 'deposit', 'int(1)');
        $this->addColumn('appliacations', 'pledge_requirement_amount', 'int(1)');
        $this->addColumn('appliacations', 'number_days_after_date_dispatch', 'int(11)');
        $this->addColumn('appliacations', 'my_phone', 'varchar(255)');
        $this->addColumn('appliacations', 'comment', 'text');
        $this->addColumn('appliacations', 'photo_recipient', 'text');
        $this->addColumn('appliacations', 'weight', 'varchar(255)');
        $this->addColumn('appliacations', 'dimensions', 'varchar(255)');

        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable(
            'files',array(
            'id'=>'pk',
            'name'=>'varchar(255) NOT NULL',
            'path'=>'varchar(255) NOT NULL',
            'appl_id'=>'int(11)',
            'exten'=>'varchar(255) NOT NULL',
            'author_id'=> "int(10)",
        ), $tableOptions);

        $this->createTable(
            'package_parcels',array(
            'id'=>'pk',
            'name'=> "varchar(255)",
            'weight'=>'varchar(255)',
            'dimensions'=>'varchar(255)',
            'appl_id'=>'int(11)',
            'brittle'=>'int(1)',
            'composition'=>'text',
            'price'=> "int(10)",
            'currency'=> "int(1)",
        ), $tableOptions);

        //end type 1

        //type 4
        $this->addColumn('appliacations', 'list_category_time_together', 'int(11)');
        $this->addColumn('appliacations', 'sublist_purpose_time_together', 'int(11)');
        $this->addColumn('appliacations', 'other', 'text');
        $this->addColumn('appliacations', 'budget_from', 'int(11)');
        $this->addColumn('appliacations', 'budget_to', 'int(11)');
        $this->addColumn('appliacations', 'list_currency', 'int(11)');
        $this->addColumn('appliacations', 'date_where', 'varchar(255)');
        $this->addColumn('appliacations', 'approximate_date', 'int(1)');
        $this->addColumn('appliacations', 'list_part_of_day', 'int(1)');
        $this->addColumn('appliacations', 'possible_children', 'int(1)');
        $this->addColumn('appliacations', 'count_people', 'int(11)');
        $this->addColumn('appliacations', 'plus_or_minus_person', 'int(11)');
        $this->addColumn('appliacations', 'description', 'text');


        //type 5
        $this->addColumn('appliacations', 'drive_car', 'int(1)');
        $this->addColumn('appliacations', 'list_transport', 'int(11)');
        $this->addColumn('appliacations', 'my_car', 'varchar(255)');
        $this->addColumn('appliacations', 'remuneration_from', 'varchar(255)');
        $this->addColumn('appliacations', 'remuneration_to', 'varchar(255)');
        $this->addColumn('appliacations', 'fotos', 'varchar(255)');
        $this->addColumn('appliacations', 'foto_recipient', 'varchar(255)');
        $this->addColumn('appliacations', 'adres_where', 'text');
        $this->addColumn('appliacations', 'adres_whence', 'text');

        $this->addColumn('appliacations', 'time_from', 'varchar(255)');
        $this->addColumn('appliacations', 'time_to', 'varchar(255)');

        //type 6
        $this->addColumn('appliacations', 'list_category_travel', 'int(11)');
        $this->addColumn('appliacations', 'sublist_purpose_travel', 'int(11)');
        $this->addColumn('appliacations', 'count_days', 'int(11)');
        $this->addColumn('appliacations', 'list_settlement_type', 'int(11)');


        $this->createTable(
            'requirements_candidates',array(
            'id'=>'pk',
            'id_list_person_characteristics'=>'int(11)',
            'id_sublist_person_characteristcs'=>'int(11)',
            'appl_id'=>'int(11)',
            'user_id'=>'int(11)',
        ), $tableOptions);

    }

    public function down()
    {
        echo "m170319_191823_update_apl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
