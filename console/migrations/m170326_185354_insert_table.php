<?php

use yii\db\Migration;

class m170326_185354_insert_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('coupons', [
            'id_coupon' => $this->primaryKey(),
            'hash' => $this->string(255),
            'created' => $this->integer()->notNull(),
            'livetime' => $this->string(255),
        ], $tableOptions);

        $this->addColumn('user', 'coupons_id_coupon', 'int(11)');

        $this->createIndex(
            'idx-user-coupons_id_coupon',
            'user',
            'coupons_id_coupon'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user-coupons_id_coupon',
            'user',
            'coupons_id_coupon',
            'coupons',
            'id_coupon',
            'CASCADE'
        );

    }

    public function down()
    {
        echo "m170326_185354_insert_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
