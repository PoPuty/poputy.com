<?php

use yii\db\Migration;

class m170226_105709_update_user extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'invite', 'int(11)');
        $this->addColumn('referal_links', 'email', 'varchar(255)');
        $this->addColumn('referal_links', 'link', 'varchar(255)');
    }

    public function down()
    {
        echo "m170226_105709_update_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
