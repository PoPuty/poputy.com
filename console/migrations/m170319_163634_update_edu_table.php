<?php

use yii\db\Migration;

class m170319_163634_update_edu_table extends Migration
{
    public function up()
    {
        $this->addColumn('educations', 'period_from', 'varchar(255)');
        $this->addColumn('educations', 'period_to', 'varchar(255)');
    }

    public function down()
    {
        echo "m170319_163634_update_edu_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
