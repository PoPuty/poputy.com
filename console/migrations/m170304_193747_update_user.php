<?php

use yii\db\Migration;

class m170304_193747_update_user extends Migration
{
    public function up()
    {
        $this->addColumn('foreign_language', 'lang_id', 'int(11)');
        $this->addColumn('foreign_language', 'lang_level_id', 'int(11)');
        $this->addColumn('educations', 'view', 'varchar(255)');
        $this->addColumn('educations', 'interval', 'varchar(255)');
        $this->addColumn('educations', 'name', 'varchar(255)');
        $this->addColumn('jobs', 'name_campany', 'varchar(255)');
        $this->addColumn('jobs', 'interval', 'varchar(255)');
        $this->addColumn('jobs', 'position', 'varchar(255)');

    }

    public function down()
    {
        echo "m170304_193747_update_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
