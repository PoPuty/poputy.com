<?php
$admin = 'http://dash.trenniajakiri.ee';
return [
    'adminEmail' => 'admin@example.com', 
    'adminpath' => $admin,
    'categoryImagePath' => $admin.'/img/articles/categories/',
            // Select URL To Upload Category Image
            'categoryImageURL'  => $admin.'/img/articles/categories/',
            // Select Path To Upload Category Thumb
            'categoryThumbPath' => $admin.'/img/articles/categories/thumb/',
            // Select URL To Upload Category Image
            'categoryThumbURL'  => $admin.'/img/articles/categories/thumb/',

            // Select Path To Upload Item Image
            'itemImagePath' => $admin.'/img/articles/items/',
            // Select URL To Upload Item Image
            'itemImageURL'  => $admin.'/img/articles/items/',
            // Select Path To Upload Item Thumb
            'itemThumbPaths' => $admin.'/img/articles/items/thumb/small/',
            'itemThumbPathm' => $admin.'/img/articles/items/thumb/medium/',
            'itemThumbPathl' => $admin.'/img/articles/items/thumb/large/',
            'itemThumbPathe' => $admin.'/img/articles/items/thumb/extra/',
            'itemThumbPath' => $admin.'/img/articles/items/thumb/extra/',
            // Select URL To Upload Item Thumb
            'itemThumbURL'  => $admin.'/img/articles/items/thumb/',
];
