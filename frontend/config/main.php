<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',

    'modules' => [
        'user' => [
            'class' => 'app\modules\user\Module',
        ],
        'article' => [
            'class' => 'app\modules\articles\Modules',
        ],
        'articles' => [
            'class' => 'cinghie\articles\Articles',
            'userClass' => 'common\models\User',

            // Select Languages allowed
            'languages' => [
                "it-IT" => "it-IT",
                "en-GB" => "en-GB"
            ],

            // Select Date Format
            'dateFormat' => 'd F Y',

            // Select Editor: no-editor, ckeditor, imperavi, tinymce, markdown
            'editor' => 'ckeditor',

            // Select Path To Upload Category Image
            'categoryImagePath' => '@webroot/img/articles/categories/',
            // Select URL To Upload Category Image
            'categoryImageURL'  => '@web/img/articles/categories/',
            // Select Path To Upload Category Thumb
            'categoryThumbPath' => '@webroot/img/articles/categories/thumb/',
            // Select URL To Upload Category Image
            'categoryThumbURL'  => '@web/img/articles/categories/thumb/',

            // Select Path To Upload Item Image
            'itemImagePath' => '@webroot/img/articles/items/',
            // Select URL To Upload Item Image
            'itemImageURL'  => '@web/img/articles/items/',
            // Select Path To Upload Item Thumb
            'itemThumbPath' => '@webroot/img/articles/items/thumb/',
            // Select URL To Upload Item Thumb
            'itemThumbURL'  => '@web/img/articles/items/thumb/',

            // Select Path To Upload Attachments
            'attachPath' => '@webroot/attachments/',
            // Select URL To Upload Attachment
            'attachURL' => '@web/img/articles/items/',
            // Select Image Types allowed
            'attachType' => 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, .csv, .pdf, text/plain, .jpg, .jpeg, .gif, .png',

            // Select Image Name: categoryname, original, casual
            'imageNameType' => 'categoryname',
            // Select Image Types allowed
            'imageType'     => 'jpg,jpeg,gif,png',
            // Thumbnails Options
            'thumbOptions'  => [
                'small'  => ['quality' => 100, 'width' => 150, 'height' => 100],
                'medium' => ['quality' => 100, 'width' => 200, 'height' => 150],
                'large'  => ['quality' => 100, 'width' => 300, 'height' => 250],
                'extra'  => ['quality' => 100, 'width' => 400, 'height' => 350],
            ],

            // Show Titles in the views
            'showTitles' => true,
        ],
    ],
    'components' => [


        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
        ],

        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            // Disable index.php
            'showScriptName' => false,
            // Disable r= routes
            'enablePrettyUrl' => true,
            // Disable site/ from the URL
            'rules' => [
                '/<id:\d+>/<alias:[A-Za-z0-9 -_.]+>' => 'articles/categories/view',
                '/<cat>/<id:\d+>/' => 'articles/items/view',
                'search/' => 'article/search',
                'tags/' => 'article/search/tags',
            ],
        ]
    ],
    'params' => $params,
];
