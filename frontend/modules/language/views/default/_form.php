<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\modules\language\models\Language\Message */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="message-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php
	echo $form->field($model, 'id')->dropDownList(ArrayHelper::map( app\modules\language\models\SourceMessage\SourceMessage::find()->asArray()->all(), 'id', 'message'), []);
   
    ?>

    <?= $form->field($model, 'language')->textInput(['maxlength' => true, 'value'=>'ru-RU']) ?>

    <?= $form->field($model, 'translation')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>
    


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
