<?php

namespace app\modules\language\controllers;

use Yii;
use app\modules\language\models\Language\Message;
use app\modules\language\models\SourceMessage\SourceMessage;
use app\modules\language\models\LanguageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Message model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Message models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LanguageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Message model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Message model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionParse()
    {
        exit;
        $objPHPExcel = \PHPExcel_IOFactory::load($_SERVER['DOCUMENT_ROOT'].'/list.xlsx');

        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, false);

            foreach ($sheetData as $key => $val) {
                if($key >= 61)
                {
                    $SourceMessage = SourceMessage::findOne(['message'=> $val[0]]);
                    if($SourceMessage)
                    {
                        $Message = new Message();
                        $Message->language = 'ru-RU';
                        $Message->translation = $val[1];
                        $Message->message = $val[1];
                        $Message->id = $SourceMessage->id;
                        $Message->save();
                    }else{
                        $SourceMessage = new SourceMessage();
                        $SourceMessage->category = 'app';
                        $SourceMessage->message = $val[0];
                        $SourceMessage->save();

                        $Message = new Message();
                        $Message->language = 'ru-RU';
                        $Message->translation = $val[1];
                        $Message->message = $val[1];
                        $Message->id = $SourceMessage->id;
                        $Message->save();
                    }
                }


            }
    }
    public function actionCreate()
    {
        $model = new Message();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['create']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Message model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_pr]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Message model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Message model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Message the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Message::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
