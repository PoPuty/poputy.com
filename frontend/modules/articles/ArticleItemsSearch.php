<?php

namespace app\modules\articles;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\articles\models\ArticleItems\ArticleItems;

/**
 * ArticleItemsSearch represents the model behind the search form about `app\modules\articles\models\ArticleItems\ArticleItems`.
 */
class ArticleItemsSearch extends ArticleItems
{
    public $search;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'catid', 'userid', 'state', 'ordering', 'hits', 'created_by', 'modified_by', 'tags_id_tag'], 'integer'],
            [['title', 'alias', 'introtext', 'fulltext', 'access', 'language', 'theme', 'image', 'image_caption',  'image_credits', 'video', 'video_type', 'video_caption', 'video_credits', 'created', 'modified', 'params', 'metadesc', 'metakey', 'robots', 'author', 'copyright', 'tags'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ArticleItems::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'catid' => $this->catid,
            'userid' => $this->userid,
            'state' => $this->state,
            'ordering' => $this->ordering,
            'hits' => $this->hits,
            'created' => $this->created,
            'created_by' => $this->created_by,
            'modified' => $this->modified,
            'modified_by' => $this->modified_by,
            'tags_id_tag' => $this->tags_id_tag,
        ]);
        if(isset($_REQUEST['search']) && $_REQUEST['search'] != '')
        {
             $query->andFilterWhere([
               'or',
                   ['like', 'title', $_REQUEST['search']],
                   ['like', 'introtext', $_REQUEST['search']],
                   ['like', 'fulltext', $_REQUEST['search']],
                   ['like', 'alias', $_REQUEST['search']],
               ]);
            
        }else{
             $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'introtext', $this->introtext])
            ->andFilterWhere(['like', 'fulltext', $this->fulltext])
            ->andFilterWhere(['like', 'access', $this->access])
            ->andFilterWhere(['like', 'language', $this->language])
            ->andFilterWhere(['like', 'theme', $this->theme])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'image_caption', $this->image_caption])
            ->andFilterWhere(['like', 'image_credits', $this->image_credits])
            ->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'video_type', $this->video_type])
            ->andFilterWhere(['like', 'video_caption', $this->video_caption])
            ->andFilterWhere(['like', 'video_credits', $this->video_credits])
            ->andFilterWhere(['like', 'params', $this->params])
            ->andFilterWhere(['like', 'metadesc', $this->metadesc])
            ->andFilterWhere(['like', 'metakey', $this->metakey])
            ->andFilterWhere(['like', 'robots', $this->robots])
            ->andFilterWhere(['like', 'author', $this->author])
            ->andFilterWhere(['like', 'copyright', $this->copyright])
            ->andFilterWhere(['like', 'tags', $this->tags]);
        }

       

        return $dataProvider;
    }
}
