<?php

namespace app\modules\articles\controllers;

use Yii;
use app\modules\articles\ArticleItemsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\modules\articles\models\ArticleItems\ArticleItems;
use yii\filters\VerbFilter;
use app\modules\articles\models\ArticlesTags\ArticlesTags;

use  yii\web\Session;
/**
 * SearchController implements the CRUD actions for ArticleItems model.
 */
class SearchController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ArticleItems models.
     * @return mixed
     */
    public function actionIndex($search)
    {
        $session = new Session;
        $session->open();
        $lang = $session->get('languages');

        $searchModel = new ArticleItemsSearch();
        $searchModel->search = $search;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'lang' => $lang
        ]);
    }

    public function actionTags($tag)
    {

        $session = new Session;
        $session->open();
        $lang = $session->get('languages');

        $ArticlesTags = ArticlesTags::find()->where(['id_tag' => $tag])->groupBy(['id_article'])->all();

        return $this->render('index_', [
            'dataProvider' => $ArticlesTags,
            'lang' => $lang
        ]);
    }

    /**
     * Displays a single ArticleItems model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ArticleItems model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ArticleItems();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ArticleItems model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ArticleItems model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ArticleItems model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ArticleItems the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ArticleItems::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
