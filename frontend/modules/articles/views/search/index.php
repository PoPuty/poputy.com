<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\articles\ArticleItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Search items');
$this->params['breadcrumbs'][] = $this->title;
?>


   <?php 
   foreach ($dataProvider->getModels() as $key => $value) {
       switch($lang)
       {
           case 'en': $value->title = $value->title_en; $value->introtext = $value->introtext_en;$value->category->name = $value->category->name_en;  break;
           case 'ru': $value->title = $value->title_ru; $value->introtext = $value->introtext_ru;$value->category->name = $value->category->name_ru;  break;
           case 'fi': $value->title = $value->title_fi; $value->introtext = $value->introtext_fi;$value->category->name = $value->category->name_fi;  break;

       }

        ?>
         <div class="col col_12_of_12">
                                <!-- Layout post 1 -->
                                <div class="layout_post_2 clearfix">
                                    <div class="item_thumb">
                                        <div class="thumb_icon">
                                            <a href="<?=$value->url;?>" jQuery><i class="fa fa-copy"></i></a>
                                        </div>
                                        <div class="thumb_hover">
                                            <a href="<?=$value->url;?>"><img src="<?=$value->thumbpath;?>" alt="Post"></a>
                                        </div>
                                        <div class="thumb_meta">
                                            <span class="category" jQuery><a href="<?=$value->caturl;?>"><?=$value->category->name;?></a></span>
                                            
                                        </div>
                                    </div>
                                    <div class="item_content">
                                        <h4><a href="post_single.html"><?=$value->title;?></a></h4>
                                        <p><?=$value->introtext;?> [...]</p>
                                        <div class="item_meta clearfix">
                                            <span class="meta_date"><?=$value->dateformat;?></span>
                                           
                                        </div>
                                    </div>
                                </div><!-- End Layout post 1 -->
                            </div>
        <?php
   }

   ?>
