<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\articles\models\ArticleItems\ArticleItems */

$this->title = Yii::t('app', 'Create Article Items');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Article Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-items-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
