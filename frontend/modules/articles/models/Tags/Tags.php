<?php

namespace app\modules\articles\models\Tags;

use Yii;

/**
 * This is the model class for table "tags".
 *
 * @property integer $id_tag
 * @property string $name
 * @property string $title
 * @property string $description
 * @property string $keywords
 *
 * @property ArticleItems[] $articleItems
 */
class Tags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'title', 'description', 'keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tag' => Yii::t('app', 'Id Tag'),
            'name' => Yii::t('app', 'Name'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'keywords' => Yii::t('app', 'Keywords'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleItems()
    {
        return $this->hasMany(ArticleItems::className(), ['tags_id_tag' => 'id_tag']);
    }
}
