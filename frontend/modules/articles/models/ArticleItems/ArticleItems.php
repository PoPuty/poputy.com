<?php

namespace app\modules\articles\models\ArticleItems;
use app\modules\articles\models\ArticleCategories\ArticleCategories;
use app\modules\articles\models\Tags\Tags;
use app\modules\articles\models\ArticlesRelations\ArticlesRelations;
use Yii;

use  yii\web\Session;
/**
 * This is the model class for table "article_items".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property integer $catid
 * @property integer $userid
 * @property string $introtext
 * @property string $fulltext
 * @property integer $state
 * @property string $access
 * @property string $language
 * @property string $theme
 * @property integer $ordering
 * @property integer $hits
 * @property string $image
 * @property string $image_caption
 * @property string $image_credits
 * @property string $video
 * @property string $video_type
 * @property string $video_caption
 * @property string $video_credits
 * @property string $created
 * @property integer $created_by
 * @property string $modified
 * @property integer $modified_by
 * @property string $params
 * @property string $metadesc
 * @property string $metakey
 * @property string $robots
 * @property string $author
 * @property string $copyright
 * @property integer $tags_id_tag
 * @property string $tags
 *
 * @property Tags $tagsIdTag
 * @property Banners[] $banners
 * @property BayArticles[] $bayArticles
 */
class ArticleItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'title_en', 'title_ru', 'alias', 'access', 'language'], 'required'],
            [['catid', 'userid', 'state', 'ordering', 'hits', 'created_by', 'modified_by', 'tags_id_tag'], 'integer'],
            [['introtext', 'introtext_en', 'introtext_ru', 'fulltext', 'fulltext_en', 'fulltext_ru', 'image', 'video', 'params', 'metadesc', 'metakey'], 'string'],
            [['created', 'modified'], 'safe'],
            [['title', 'alias', 'image_caption', 'image_credits', 'video_caption', 'video_credits'], 'string', 'max' => 255],
            [['access'], 'string', 'max' => 64],
            [['language'], 'string', 'max' => 7],
            [['theme'], 'string', 'max' => 12],
            [['video_type', 'robots'], 'string', 'max' => 20],
            [['author', 'copyright'], 'string', 'max' => 50],
            [['tags'], 'string', 'max' => 1000],
            [['tags_id_tag'], 'exist', 'skipOnError' => true, 'targetClass' => Tags::className(), 'targetAttribute' => ['tags_id_tag' => 'id_tag']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'title_ru' => Yii::t('app', 'Title RU'),
            'title_en' => Yii::t('app', 'Title EN'),
            'alias' => Yii::t('app', 'Alias'),
            'catid' => Yii::t('app', 'Catid'),
            'userid' => Yii::t('app', 'Userid'),
            'introtext' => Yii::t('app', 'Introtext'),
            'fulltext' => Yii::t('app', 'Fulltext'),
            'introtext_ru' => Yii::t('app', 'Introtext RU'),
            'fulltext_ru' => Yii::t('app', 'Fulltext RU'),
            'introtext_en' => Yii::t('app', 'Introtext EN'),
            'fulltext_en' => Yii::t('app', 'Fulltext EN'),
            'state' => Yii::t('app', 'State'),
            'access' => Yii::t('app', 'Access'),
            'language' => Yii::t('app', 'Language'),
            'theme' => Yii::t('app', 'Theme'),
            'ordering' => Yii::t('app', 'Ordering'),
            'hits' => Yii::t('app', 'Hits'),
            'image' => Yii::t('app', 'Image'),
            'image_caption' => Yii::t('app', 'Image Caption'),
            'image_credits' => Yii::t('app', 'Image Credits'),
            'video' => Yii::t('app', 'Video'),
            'video_type' => Yii::t('app', 'Video Type'),
            'video_caption' => Yii::t('app', 'Video Caption'),
            'video_credits' => Yii::t('app', 'Video Credits'),
            'created' => Yii::t('app', 'Created'),
            'created_by' => Yii::t('app', 'Created By'),
            'modified' => Yii::t('app', 'Modified'),
            'modified_by' => Yii::t('app', 'Modified By'),
            'params' => Yii::t('app', 'Params'),
            'metadesc' => Yii::t('app', 'Metadesc'),
            'metakey' => Yii::t('app', 'Metakey'),
            'robots' => Yii::t('app', 'Robots'),
            'author' => Yii::t('app', 'Author'),
            'copyright' => Yii::t('app', 'Copyright'),
            'tags_id_tag' => Yii::t('app', 'Tags Id Tag'),
            'tags' => Yii::t('app', 'Tags'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagsIdTag()
    {
        return $this->hasOne(Tags::className(), ['id_tag' => 'tags_id_tag']);
    }

    public function getUrl()
    {
        return '/'.$this->category->alias.'/'.$this->id;
    }
    public function getCaturl()
    {
        return '/'.$this->category->id.'/'.$this->category->alias;
    }
    public function getThumbpath()
    {
        return Yii::$app->params['itemThumbPath'].$this->image;
    }
    public function getIntrotextlitle()
    {

        $session = new Session;
        $session->open();
        $lang = $session->get('languages');
        switch($lang)
        {
            case 'en': $this->introtext = $this->introtext_en;  break;
            case 'ru': $this->introtext = $this->introtext_ru;   break;
            case 'fi': $this->introtext = $this->introtext_fi;   break;

        }
        return mb_strimwidth($this->introtext, 0, 100, "");;
    }

    public function getDateformat()
    {
        return date(' jS F Y', strtotime($this->created));
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanners()
    {
        return $this->hasMany(Banners::className(), ['articles_id_article' => 'id']);
    }

    public function getCategory()
    {
        return $this->hasOne(ArticleCategories::className(), ['id' => 'catid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBayArticles()
    {
        return $this->hasMany(BayArticles::className(), ['articles_id_article' => 'id']);
    }
}
