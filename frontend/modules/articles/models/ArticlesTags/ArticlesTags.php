<?php

namespace app\modules\articles\models\ArticlesTags;

use Yii;
use app\modules\articles\models\ArticleItems\ArticleItems;

/**
 * This is the model class for table "articles_tags".
 *
 * @property integer $id
 * @property integer $id_tag
 * @property integer $id_article
 */
class ArticlesTags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articles_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_tag', 'id_article'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_tag' => Yii::t('app', 'Id Tag'),
            'id_article' => Yii::t('app', 'Id Article'),
        ];
    }

    public function getArticle()
    {
        return $this->hasOne(ArticleItems::className(), ['id' => 'id_article']);
    }
}
