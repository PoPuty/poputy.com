<?php

namespace app\modules\user\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\models\UserProfiles\UserProfiles;

/**
 * UserProfilesSearch represents the model behind the search form about `app\modules\user\models\UserProfiles\UserProfiles`.
 */
class UserProfilesSearch extends UserProfiles
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user_profiles', 'sex', 'users_id_users', 'contries_id_contries', 'towns_id_towns', 'towns_contries_id_contries', 'created_at', 'updated_at'], 'integer'],
            [['name', 'second_name', 'surname', 'birthday', 'zodiac', 'marital_status', 'education', 'job', 'interest', 'experience', 'character', 'smoke', 'alchocol', 'foreign_language', 'social_net', 'photo', 'background', 'religion', 'children'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserProfiles::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_user_profiles' => $this->id_user_profiles,
            'sex' => $this->sex,
            'users_id_users' => $this->users_id_users,
            'contries_id_contries' => $this->contries_id_contries,
            'towns_id_towns' => $this->towns_id_towns,
            'towns_contries_id_contries' => $this->towns_contries_id_contries,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'second_name', $this->second_name])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'birthday', $this->birthday])
            ->andFilterWhere(['like', 'zodiac', $this->zodiac])
            ->andFilterWhere(['like', 'marital_status', $this->marital_status])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'job', $this->job])
            ->andFilterWhere(['like', 'interest', $this->interest])
            ->andFilterWhere(['like', 'experience', $this->experience])
            ->andFilterWhere(['like', 'character', $this->character])
            ->andFilterWhere(['like', 'smoke', $this->smoke])
            ->andFilterWhere(['like', 'alchocol', $this->alchocol])
            ->andFilterWhere(['like', 'foreign_language', $this->foreign_language])
            ->andFilterWhere(['like', 'social_net', $this->social_net])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'background', $this->background])
            ->andFilterWhere(['like', 'religion', $this->religion])
            ->andFilterWhere(['like', 'children', $this->children]);

        return $dataProvider;
    }
}
