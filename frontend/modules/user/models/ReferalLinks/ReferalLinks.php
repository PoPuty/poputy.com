<?php

namespace app\modules\user\models\ReferalLinks;

use Yii;
use common\models\User;
/**
 * This is the model class for table "referal_links".
 *
 * @property integer $id_referal_links
 * @property integer $active
 * @property string $hash_link
 * @property integer $users_id_users
 *
 * @property User $usersIdUsers
 */
class ReferalLinks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'referal_links';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active', 'users_id_users'], 'integer'],
            [['hash_link', 'email', 'link'], 'string', 'max' => 255],
            [['email'], 'required'],
            [['users_id_users'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['users_id_users' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_referal_links' => Yii::t('app', 'Id Referal Links'),
            'active' => Yii::t('app', 'Active'),
            'hash_link' => Yii::t('app', 'Hash Link'),
            'users_id_users' => Yii::t('app', 'Users Id Users'),
            'email' => Yii::t('app', 'email'),
            'link' => Yii::t('app', 'link'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersIdUsers()
    {
        return $this->hasOne(User::className(), ['id' => 'users_id_users']);
    }
}
