<?php

namespace app\modules\user\models\User;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $invite
 * @property string $blocked_at
 * @property string $unconfirmed_email
 * @property string $code
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at', 'blocked_at', 'unconfirmed_email', 'code'], 'required'],
            [['status', 'created_at', 'updated_at', 'invite'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'blocked_at', 'unconfirmed_email', 'code'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'invite' => Yii::t('app', 'Invite'),
            'blocked_at' => Yii::t('app', 'Blocked At'),
            'unconfirmed_email' => Yii::t('app', 'Unconfirmed Email'),
            'code' => Yii::t('app', 'Code'),
        ];
    }
}
