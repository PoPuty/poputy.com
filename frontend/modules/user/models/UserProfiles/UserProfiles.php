<?php

namespace app\modules\user\models\UserProfiles;

use Yii;
use common\models\User;
use app\modules\reference\models\Educations\Educations;
use app\modules\reference\models\Foreignlanguage\Foreignlanguage;
use app\modules\reference\models\Jobs\Jobs;
/**
 * This is the model class for table "user_profiles".
 *
 * @property integer $id_user_profiles
 * @property string $name
 * @property string $second_name
 * @property string $surname
 * @property integer $sex
 * @property string $zodiac
 * @property string $marital_status
 * @property string $education
 * @property string $job
 * @property string $interest
 * @property string $experience
 * @property string $character
 * @property string $smoke
 * @property string $alchocol
 * @property string $foreign_language
 * @property string $social_net
 * @property string $photo
 * @property string $background
 * @property string $religion
 * @property string $children
 * @property integer $users_id_users
 * @property integer $contries_id_contries
 * @property integer $towns_id_towns
 * @property integer $towns_contries_id_contries
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $usersIdUsers
 */
class UserProfiles extends \yii\db\ActiveRecord
{
    public $client_avatar;
    public $client_background;
    public $education;
    public $job;
    public $foreignlanguage;
    public $requirementsCandidates;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_avatar',], 'file' , 'skipOnEmpty' => true,  'extensions' => 'png, jpeg, jpg'],
            [['client_background',], 'file' , 'skipOnEmpty' => true,  'extensions' => 'png, jpeg, jpg'],
            [['sex', 'users_id_users', 'smoke', 'alchocol', 'contries_id_contries', 'zodiac', 'towns_id_towns', 'towns_contries_id_contries', 'created_at', 'marital_status', 'religion', 'children', 'zodiac', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [[ 'education', 'job',  'foreignlanguage' ], 'safe'],
            [['name', 'second_name', 'surname',  'social_net', 'photo', 'background',   'birthday'], 'string', 'max' => 255],
            [['interest', 'experience', 'character'], 'string', 'max' => 1024],

            [['users_id_users'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['users_id_users' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user_profiles' => Yii::t('app', 'Id User Profiles'),
            'name' => Yii::t('app', 'Name'),
            'second_name' => Yii::t('app', 'Second Name'),
            'surname' => Yii::t('app', 'Surname'),
            'birthday' => Yii::t('app', 'Birthday'),
            'sex' => Yii::t('app', 'Sex'),
            'zodiac' => Yii::t('app', 'Zodiac'),
            'marital_status' => Yii::t('app', 'Marital Status'),
            'education' => Yii::t('app', 'Education'),
            'job' => Yii::t('app', 'Job'),
            'interest' => Yii::t('app', 'Interest'),
            'experience' => Yii::t('app', 'Experience'),
            'character' => Yii::t('app', 'Character'),
            'smoke' => Yii::t('app', 'Smoke'),
            'alchocol' => Yii::t('app', 'Alchocol'),
            'foreign_language' => Yii::t('app', 'Foreign Language'),
            'foreignlanguage' => Yii::t('app', 'Foreign Language'),
            'social_net' => Yii::t('app', 'Social Net'),
            'photo' => Yii::t('app', 'Photo'),
            'background' => Yii::t('app', 'Background'),
            'religion' => Yii::t('app', 'Religion'),
            'children' => Yii::t('app', 'Children'),
            'users_id_users' => Yii::t('app', 'Users Id Users'),
            'contries_id_contries' => Yii::t('app', 'Contries Id Contries'),
            'towns_id_towns' => Yii::t('app', 'Towns Id Towns'),
            'towns_contries_id_contries' => Yii::t('app', 'Towns Contries Id Contries'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'client_avatar' => Yii::t('app', 'client_avatar'),
            'client_background' => Yii::t('app', 'client_background'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersIdUsers()
    {
        return $this->hasOne(User::className(), ['id' => 'users_id_users']);
    }

    public function getEducations()
    {
        return $this->hasMany(Educations::className(), ['user_profile_id' => 'id_user_profiles']);
    }
    public function getJobs()
    {
        return $this->hasMany(Jobs::className(), ['user_profile_id' => 'id_user_profiles']);
    }
    public function getForeignlanguages()
    {
        return $this->hasMany(Foreignlanguage::className(), ['user_profile_id' => 'id_user_profiles']);
    }

    /**
     * @inheritdoc
     * @return CountriesQuery the active query used by this AR class.
     */

}
