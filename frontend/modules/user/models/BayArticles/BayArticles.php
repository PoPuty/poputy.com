<?php

namespace app\modules\user\models\BayArticles;

use Yii;

/**
 * This is the model class for table "bay_articles".
 *
 * @property integer $id_bay_articles
 * @property integer $articles_id_article
 * @property integer $packages_id_package
 * @property integer $users_id_user
 *
 * @property User $usersIdUser
 * @property ArticleItems $articlesIdArticle
 * @property Packages $packagesIdPackage
 */
class BayArticles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bay_articles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['articles_id_article', 'packages_id_package', 'users_id_user'], 'required'],
            [['articles_id_article', 'packages_id_package', 'users_id_user'], 'integer'],
            [['users_id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['users_id_user' => 'id']],
            [['articles_id_article'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleItems::className(), 'targetAttribute' => ['articles_id_article' => 'id']],
            [['packages_id_package'], 'exist', 'skipOnError' => true, 'targetClass' => Packages::className(), 'targetAttribute' => ['packages_id_package' => 'id_package']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_bay_articles' => Yii::t('app', 'Id Bay Articles'),
            'articles_id_article' => Yii::t('app', 'Articles Id Article'),
            'packages_id_package' => Yii::t('app', 'Packages Id Package'),
            'users_id_user' => Yii::t('app', 'Users Id User'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'users_id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticlesIdArticle()
    {
        return $this->hasOne(ArticleItems::className(), ['id' => 'articles_id_article']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackagesIdPackage()
    {
        return $this->hasOne(Packages::className(), ['id_package' => 'packages_id_package']);
    }
}
