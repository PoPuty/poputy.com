<?php

namespace app\modules\user\models\PackagesHasUsers;

use Yii;

/**
 * This is the model class for table "packages_has_users".
 *
 * @property integer $packages_id_package
 * @property integer $users_id_user
 * @property integer $bay_date
 * @property integer $finish_date
 *
 * @property Packages $packagesIdPackage
 * @property User $usersIdUser
 */
class PackagesHasUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'packages_has_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['packages_id_package', 'users_id_user'], 'required'],
            [['packages_id_package', 'users_id_user', 'bay_date', 'finish_date'], 'integer'],
            [['packages_id_package'], 'exist', 'skipOnError' => true, 'targetClass' => Packages::className(), 'targetAttribute' => ['packages_id_package' => 'id_package']],
            [['users_id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['users_id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'packages_id_package' => Yii::t('app', 'Packages Id Package'),
            'users_id_user' => Yii::t('app', 'Users Id User'),
            'bay_date' => Yii::t('app', 'Bay Date'),
            'finish_date' => Yii::t('app', 'Finish Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackagesIdPackage()
    {
        return $this->hasOne(Packages::className(), ['id_package' => 'packages_id_package']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'users_id_user']);
    }
}
