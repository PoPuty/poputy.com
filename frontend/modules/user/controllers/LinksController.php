<?php

namespace app\modules\user\controllers;

use Yii;
use app\modules\user\models\ReferalLinks\ReferalLinks;
use app\modules\user\models\UserProfilesSearch;
use app\modules\reference\models\Educations\Educations;
use app\modules\reference\models\Foreignlanguage\Foreignlanguage;
use app\modules\reference\models\Jobs\Jobs;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

use yii\imagine\Image;
use yii\imagine;
use yii\imagine\Image\Box;
use yii\imagine\Image\ImageInterface;
use yii\imagine\Image\ImagineInterface;
use yii\imagine\Image\ManipulatorInterface;
use yii\imagine\Image\Point;


/**
 * DefaultController implements the CRUD actions for UserProfiles model.
 */
class LinksController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionUpload($id)
    {

            $data['file'] = array();
            $model = $this->findModel($id);
            /*if($model->load(Yii::$app->request->post()) && $model->save())
            {
                echo '123';
            }*/
            $model->client_avatar = \yii\web\UploadedFile::getInstance($model, 'client_avatar');
            
            $time = strtotime(date('d-m-Y H:i:s'));

            if($model->client_avatar)
            {

                $path = 'uploads/avatars/' . $time . '.' . $model->client_avatar->extension;
                $model->client_avatar->saveAs($_SERVER['DOCUMENT_ROOT'].'/'.$path);

                Image::thumbnail($path, 800, 600)
                    ->save($path, ['quality' => 80]);
                
                $model->photo = $path;
                $model->client_avatar = '';
                $model->save();
                $data['file'] = ['id' => '1', 'name' => 'file', 'file' =>  '/'.$path];
            }
        echo json_encode($data);
        

    }


    public function actionUploadcropper($id)
        {
            $data['file'] = array();
            $model = $this->findModel($id);

            foreach($_FILES['image'] as $key => $val){
                $_FILES['UserProfiles'][$key] = array('client_avatar' => $val);
            }


            $model->client_avatar = \yii\web\UploadedFile::getInstance($model, 'client_avatar');
            
            $time = strtotime(date('d-m-Y H:i:s'));
            
            if($model->client_avatar)
            {
                $type = explode('/', $model->client_avatar->type);

                $path = 'uploads/avatars/' . $time . '.' . $type[1];
                $model->client_avatar->saveAs($_SERVER['DOCUMENT_ROOT'].'/'.$path);

                
                
                $model->photo = $path;
                $model->client_avatar = '';
                $model->save();
                $data['file'] = ['id' => '1', 'name' => 'file', 'file' =>  '/'.$path];
            }
        echo json_encode($data);

        }
    /**
     * Lists all UserProfiles models.
     * @return mixed
     */
    public function actionFieldget($field)
    {
        $model = new UserProfiles();
        $form = ActiveForm::begin();
        return $form->field($model, $field)->textarea(['rows' => 6]);
    }

    public function actionIndex()
    {
        $searchModel = new UserProfilesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserProfiles model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserProfiles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserProfiles();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {




            return $this->redirect(['view', 'id' => $model->id_user_profiles]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserProfiles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdatetime($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Educations::deleteAll(['user_profile_id' => $model->id_user_profiles]);


            foreach ($model->education as $key => $val) {
                $Educations = new Educations();
                $Educations->description = $val;
                $Educations->user_profile_id = $model->id_user_profiles;
                $Educations->save();
            }
            Jobs::deleteAll(['user_profile_id' => $model->id_user_profiles]);
            foreach ($model->job as $key => $val) {
                $Jobs = new Jobs();
                $Jobs->description = $val;
                $Jobs->user_profile_id = $model->id_user_profiles;
                $Jobs->save();
            }
            Foreignlanguage::deleteAll(['user_profile_id' => $model->id_user_profiles]);
            foreach ($model->foreignlanguage as $key => $val) {
                $Foreignlanguage = new Foreignlanguage();
                $Foreignlanguage->description = $val;
                $Foreignlanguage->user_profile_id = $model->id_user_profiles;
                $Foreignlanguage->save();
            }

        }
    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Educations::deleteAll(['user_profile_id' => $model->id_user_profiles]);


            foreach ($model->education as $key => $val ) {
                $Educations = new Educations();
                $Educations->description = $val;
                $Educations->user_profile_id = $model->id_user_profiles;
                $Educations->save();
            }
            Jobs::deleteAll(['user_profile_id' => $model->id_user_profiles]);
            foreach ($model->job as $key => $val ) {
                $Jobs = new Jobs();
                $Jobs->description = $val;
                $Jobs->user_profile_id = $model->id_user_profiles;
                $Jobs->save();
            }
            Foreignlanguage::deleteAll(['user_profile_id' => $model->id_user_profiles]);
            foreach ($model->foreignlanguage as $key => $val ) {
                $Foreignlanguage = new Foreignlanguage();
                $Foreignlanguage->description = $val;
                $Foreignlanguage->user_profile_id = $model->id_user_profiles;
                $Foreignlanguage->save();
            }

            $model->client_avatar = \yii\web\UploadedFile::getInstance($model, 'client_avatar');
            $model->client_background = \yii\web\UploadedFile::getInstance($model, 'client_background');
            $time = strtotime(date('d-m-Y H:i:s'));

            if($model->client_avatar)
            {

                $path = 'uploads/avatars/' . $time . '.' . $model->client_avatar->extension;
                $model->client_avatar->saveAs($_SERVER['DOCUMENT_ROOT'].'/'.$path);

                Image::thumbnail($path, 800, 600)
                    ->save($path, ['quality' => 80]);
                /*$path = 'uploads/avatars/' . $time . '.' . $model->client_avatar->extension;
                $model->client_f->saveAs($_SERVER['DOCUMENT_ROOT'].'/'.$path);
*/
                $model->photo = $path;
                $model->client_avatar = '';
                $model->save();

            }
            if($model->client_background)
            {
                $path = 'uploads/background/' . $time . '.' . $model->client_background->extension;
                $model->client_background->saveAs($_SERVER['DOCUMENT_ROOT'].'/'.$path);

                $model->background = $path;
                $model->client_background = '';
                $model->save();

            }




            return $this->redirect(['view', 'id' => $model->id_user_profiles]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserProfiles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserProfiles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserProfiles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ReferalLinks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
