<?php

namespace app\modules\user\controllers;

use Yii;
use app\modules\user\models\User\User;
use app\modules\user\models\UserSearch;

use app\modules\user\models\PackagesHasUsers\PackagesHasUsers;
use app\modules\user\models\Packages\Packages;
use app\modules\user\models\BayArticles\BayArticles;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
/**
 * DefaultController implements the CRUD actions for UserProfiles model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserProfiles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserProfiles model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserProfiles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */


    /**
     * Updates an existing UserProfiles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateemail($code, $email)
    {
        $user = User::findOne(['code'=>$code, 'id' => Yii::$app->user->id]);
        if($user)
        {
            $user->email = $email;
            $user->save();
            echo Json::encode(['status' => 'true']);
        }else{
            echo Json::encode(['status' => 'false']);
        }
    }
    public function actionCodevalidate($code)
    {
        $user = User::findOne(['code'=>$code, 'id' => Yii::$app->user->id]);

        if($user)
        {
            echo Json::encode(['status' => 'true']);
        }else{
            echo Json::encode(['status' => 'false']);
        }
    }
    public function actionGetcode()
    {
        $code =  uniqid('', true);
        $user = User::findOne(Yii::$app->user->id);
        $user->code = $code;
        $user->save();

        mail(Yii::$app->user->identity->email, "Получить код", "$code ");


    }
    public function actionUpdatepackage()
    {

        $packages = Packages::find()->limit(6)->all();

            return $this->render('updatePackage', [
                'packages' => $packages,
            ]);

    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserProfiles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserProfiles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserProfiles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
