<?php

namespace app\modules\user\controllers;

use Yii;
use app\modules\user\models\UserProfiles\UserProfiles;
use app\modules\user\models\UserProfilesSearch;
use app\modules\reference\models\Educations\Educations;
use app\modules\reference\models\Foreignlanguage\Foreignlanguage;
use app\modules\reference\models\Jobs\Jobs;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

use app\modules\applications\models\Applications\Applications;
use  app\modules\reference\models\PackageParcels\PackageParcels;
use app\modules\language\models\Language\Message;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\imagine\Image;
use yii\imagine;
use yii\imagine\Image\Box;
use yii\imagine\Image\ImageInterface;
use yii\imagine\Image\ImagineInterface;
use yii\imagine\Image\ManipulatorInterface;
use yii\imagine\Image\Point;


/**
 * DefaultController implements the CRUD actions for UserProfiles model.
 */
class ProfileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionUpload($id)
    {

            $data['file'] = array();
            $model = $this->findModel($id);
            /*if($model->load(Yii::$app->request->post()) && $model->save())
            {
                echo '123';
            }*/
            $model->client_avatar = \yii\web\UploadedFile::getInstance($model, 'client_avatar');
            
            $time = strtotime(date('d-m-Y H:i:s'));

            if($model->client_avatar)
            {

                $path = 'uploads/avatars/' . $time . '.' . $model->client_avatar->extension;
                $model->client_avatar->saveAs($_SERVER['DOCUMENT_ROOT'].'/'.$path);

                Image::thumbnail($path, 800, 600)
                    ->save($path, ['quality' => 80]);
                
                $model->photo = $path;
                $model->client_avatar = '';
                $model->save();
                $data['file'] = ['id' => '1', 'name' => 'file', 'file' =>  '/'.$path];
            }
        echo json_encode($data);
        

    }


    public function actionUploadcropper($id)
        {
            $data['file'] = array();
            $model = $this->findModel($id);

            foreach($_FILES['image'] as $key => $val){
                $_FILES['UserProfiles'][$key] = array('client_avatar' => $val);
            }


            $model->client_avatar = \yii\web\UploadedFile::getInstance($model, 'client_avatar');
            
            $time = strtotime(date('d-m-Y H:i:s'));
            
            if($model->client_avatar)
            {
                $type = explode('/', $model->client_avatar->type);

                $path = 'uploads/avatars/' . $time . '.' . $type[1];
                $model->client_avatar->saveAs($_SERVER['DOCUMENT_ROOT'].'/'.$path);

                
                
                $model->photo = $path;
                $model->client_avatar = '';
                $model->save();
                $data['file'] = ['id' => '1', 'name' => 'file', 'file' =>  '/'.$path];
            }
        echo json_encode($data);

        }
    /**
     * Lists all UserProfiles models.
     * @return mixed
     */
    public function actionFieldget($field, $count)
    {
        $model = new UserProfiles();
        $form = ActiveForm::begin();
        if($field == 'foreignlanguage[]')
        {
            return $form->field($model, 'foreignlanguage['.$_REQUEST['count'].'][list_language]')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Message::find()->where(['id' => 37])->asArray()->all(), 'id_pr', 'message'),
                'language' => 'ru',

                'options' => ['placeholder' => 'Select a state ...','data-id' =>$_REQUEST['id']],
                'pluginOptions' => [
                    'name'=> 'UserProfiles[foreignlanguage]['.$_REQUEST['count'].'][list_language]',

                    'allowClear' => true
                ],
            ]).$form->field($model, 'foreignlanguage['.$_REQUEST['count'].'][list_language_level]')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Message::find()->where(['id' => 38])->asArray()->all(), 'id_pr', 'message'),
                'language' => 'ru',

                'options' => ['placeholder' => 'Select a state ...', 'data-id' =>$_REQUEST['id']],
                'pluginOptions' => [
                    'name'=> 'UserProfiles[foreignlanguage]['.$_REQUEST['count'].'][list_language_level]',

                    'allowClear' => true
                ],
            ])->label(Yii::t('app', 'foreignlanguage_level')).$form->field($model, 'foreignlanguage['.$_REQUEST['count'].'][description]')->textarea(['rows' => 6])->label(Yii::t('app', 'foreignlanguage_description'));

        }elseif($field == 'education[]'){
            return $form->field($model, 'education['.$_REQUEST['count'].'][view]')->textInput([])->label(Yii::t('app', 'education_view')).$form->field($model, 'education['.$_REQUEST['count'].'][period_from]')->widget(\yii\jui\DatePicker::classname(), [
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control datepicker']
            ])->label(Yii::t('app', 'education_period_from')).$form->field($model, 'education['.$_REQUEST['count'].'][period_to]')->widget(\yii\jui\DatePicker::classname(), [
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control datepicker']
            ])->label(Yii::t('app', 'education_period_to')). $form->field($model, 'education['.$_REQUEST['count'].'][name]')->textInput([])->label(Yii::t('app', 'education_name'));

        }elseif($field == 'requirementsCandidates[]') {
            return $form->field($model, 'requirementsCandidates['.$_REQUEST['count'].'][id_list_person_characteristics]')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Message::find()->where(['id' => 73])->asArray()->all(), 'id_pr', 'message'),
                'language' => 'ru',

                'options' => ['placeholder' => 'Select a state ...'],
                'pluginOptions' => [
                    'name'=> 'UserProfiles[requirementsCandidates]['.$_REQUEST['count'].'][id_list_person_characteristics]',
                    'allowClear' => true
                ],
            ])->label(Yii::t('app', 'requirementsCandidates_currency')).$form->field($model, 'requirementsCandidates['.$_REQUEST['count'].'][id_sublist_person_characteristcs]')->widget(Select2::classname(), [
                'data' => [],
                'language' => 'ru',

                'options' => ['placeholder' => 'Select a state ...'],
                'pluginOptions' => [
                    'name'=> 'UserProfiles[requirementsCandidates]['.$_REQUEST['count'].'][id_sublist_person_characteristcs]',
                    'allowClear' => true
                ],
            ])->label(Yii::t('app', 'requirementsCandidates_id_sublist_person_characteristcs'));

        }elseif($field == 'packege[]') {
            $model = new Applications();
            return $form->field($model, 'packege['.$_REQUEST['count'].'][name]')->textInput([ ])->label(Yii::t('app', 'packege_name')).$form->field($model, 'packege['.$_REQUEST['count'].'[weight]')->textInput([])->label(Yii::t('app', 'packege_weight')).$form->field($model, 'packege['.$_REQUEST['count'].'][dimensions]')->textInput([])->label(Yii::t('app', 'packege_dimensions')).$form->field($model, 'packege['.$_REQUEST['count'].'][composition]')->textInput([])->label(Yii::t('app', 'packege_composition')).$form->field($model, 'packege['.$_REQUEST['count'].'][price]')->textInput([])->label(Yii::t('app', 'packege_price')).$form->field($model, 'packege['.$_REQUEST['count'].'][currency]')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Message::find()->where(['id' => 52])->asArray()->all(), 'id_pr', 'message'),
                'language' => 'ru',

                'options' => ['placeholder' => 'Select a state ...', 'data-id' =>$_REQUEST['id']],
                'pluginOptions' => [
                    'name'=> 'UserProfiles[packege]['.$_REQUEST['count'].'][currency]',
                    'allowClear' => true
                ],
            ])->label(Yii::t('app', 'packege_currency'));
        } elseif($field == 'job[]'){

            return $form->field($model, 'job['.$_REQUEST['count'].'][name_campany]')->textInput([ ])->label(Yii::t('app', 'job_name_campany'))
             .$form->field($model, 'job['.$_REQUEST['count'].'][position]')->textInput([])->label(Yii::t('app', 'job_position')).$form->field($model, 'job['.$_REQUEST['count'].'][period_from]')->widget(\yii\jui\DatePicker::classname(), [
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control datepicker', ]
            ])->label(Yii::t('app', 'job_period_from')).$form->field($model, 'job['.$_REQUEST['count'].'][period_to]')->widget(\yii\jui\DatePicker::classname(), [
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control datepicker']
            ])->label(Yii::t('app', 'job_period_to'));
        }else{
            return $form->field($model, $field)->textarea(['rows' => 6]);
        }

    }

    public function actionIndex()
    {
        $searchModel = new UserProfilesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserProfiles model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserProfiles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserProfiles();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {




            return $this->redirect(['view', 'id' => $model->id_user_profiles]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserProfiles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdatetime($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Educations::deleteAll(['user_profile_id' => $model->id_user_profiles]);


            foreach ($model->education as $key => $val ) {
                $Educations = new Educations();
                //$Educations->description = $val;
                $Educations->period_from = $val['period_from'];
                $Educations->period_to = $val['period_to'];
                $Educations->name = $val['name'];
                $Educations->view = $val['view'];
                $Educations->user_profile_id = $model->id_user_profiles;
                $Educations->save();
            }
            Jobs::deleteAll(['user_profile_id' => $model->id_user_profiles]);
            foreach ($model->job as $key => $val ) {
                $Jobs = new Jobs();
                //$Jobs->description = $val['description'];
                $Jobs->period_from = $val['period_from'];
                $Jobs->period_to = $val['period_to'];
                $Jobs->position = $val['position'];
                $Jobs->name_campany = $val['name_campany'];
                $Jobs->user_profile_id = $model->id_user_profiles;
                $Jobs->save();
            }
            Foreignlanguage::deleteAll(['user_profile_id' => $model->id_user_profiles]);

            foreach ($model->foreignlanguage as $key => $val ) {
                $Foreignlanguage = new Foreignlanguage();
                $Foreignlanguage->description = $val['description'];
                $Foreignlanguage->list_language_level = $val['list_language_level'];
                $Foreignlanguage->list_language = $val['list_language'];
                $Foreignlanguage->user_profile_id = $model->id_user_profiles;
                $Foreignlanguage->save();
            }

        }
    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Educations::deleteAll(['user_profile_id' => $model->id_user_profiles]);


            foreach ($model->education as $key => $val ) {
                $Educations = new Educations();
                //$Educations->description = $val;
                $Educations->period_from = $val['period_from'];
                $Educations->period_to = $val['period_to'];
                $Educations->name = $val['name'];
                $Educations->view = $val['view'];
                $Educations->user_profile_id = $model->id_user_profiles;
                $Educations->save();
            }
            Jobs::deleteAll(['user_profile_id' => $model->id_user_profiles]);
            foreach ($model->job as $key => $val ) {
                $Jobs = new Jobs();
                //$Jobs->description = $val['description'];
                $Jobs->period_from = $val['period_from'];
                $Jobs->period_to = $val['period_to'];
                $Jobs->position = $val['position'];
                $Jobs->name_campany = $val['name_campany'];
                $Jobs->user_profile_id = $model->id_user_profiles;
                $Jobs->save();
            }
            Foreignlanguage::deleteAll(['user_profile_id' => $model->id_user_profiles]);

            foreach ($model->foreignlanguage as $key => $val ) {
                $Foreignlanguage = new Foreignlanguage();
                $Foreignlanguage->description = $val['description'];
                $Foreignlanguage->list_language_level = $val['list_language_level'];
                $Foreignlanguage->list_language = $val['list_language'];
                $Foreignlanguage->user_profile_id = $model->id_user_profiles;
                $Foreignlanguage->save();
            }

            $model->client_avatar = \yii\web\UploadedFile::getInstance($model, 'client_avatar');
            $model->client_background = \yii\web\UploadedFile::getInstance($model, 'client_background');
            $time = strtotime(date('d-m-Y H:i:s'));

            if($model->client_avatar)
            {

                $path = 'uploads/avatars/' . $time . '.' . $model->client_avatar->extension;
                $model->client_avatar->saveAs($_SERVER['DOCUMENT_ROOT'].'/'.$path);

                Image::thumbnail($path, 800, 600)
                    ->save($path, ['quality' => 80]);
                /*$path = 'uploads/avatars/' . $time . '.' . $model->client_avatar->extension;
                $model->client_f->saveAs($_SERVER['DOCUMENT_ROOT'].'/'.$path);
*/
                $model->photo = $path;
                $model->client_avatar = '';
                $model->save();

            }
            if($model->client_background)
            {
                $path = 'uploads/background/' . $time . '.' . $model->client_background->extension;
                $model->client_background->saveAs($_SERVER['DOCUMENT_ROOT'].'/'.$path);

                $model->background = $path;
                $model->client_background = '';
                $model->save();

            }




            return $this->redirect(['view', 'id' => $model->id_user_profiles]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserProfiles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserProfiles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserProfiles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserProfiles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
