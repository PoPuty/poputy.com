<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\UserProfilesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Profiles');
$this->params['breadcrumbs'][] = $this->title;
echo \Yii::$app->language;

?>
<div class="user-profiles-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('frontend', 'Create User Profiles'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_user_profiles',
            'name',
            'second_name',
            'surname',
            'sex',
            // 'zodiac',
            // 'marital_status',
            // 'education',
            // 'job',
            // 'interest',
            // 'experience',
            // 'character',
            // 'smoke',
            // 'alchocol',
            // 'foreign_language',
            // 'social_net',
            // 'photo',
            // 'background',
            // 'religion',
            // 'children',
            // 'users_id_users',
            // 'contries_id_contries',
            // 'towns_id_towns',
            // 'towns_contries_id_contries',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
