<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model app\modules\user\models\UserProfiles\UserProfiles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-profiles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php


    $model->birthday = date('d-m-Y');
    ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'second_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'birthday')->widget(\yii\jui\DatePicker::classname(), [
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
        'options' => ['class' => 'form-control']
    ]) ?>
    <?php
    echo $form->field($model, 'sex')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(app\modules\language\models\Language\Message::find()->where(['id' => 31])->asArray()->all(), 'id_pr', 'message'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    <?php
    echo $form->field($model, 'zodiac')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(app\modules\language\models\Language\Message::find()->where(['id' => 32])->asArray()->all(), 'id_pr', 'message'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    <?php
    echo $form->field($model, 'marital_status')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(app\modules\language\models\Language\Message::find()->where(['id' => 33])->asArray()->all(), 'id_pr', 'message'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?php

    if(count($model->educations) > 0 ) {
        foreach ($model->educations as $key => $val) {
            $model->education[$key]['period_from'] = $val['period_from'];
            $model->education[$key]['period_to'] = $val['period_to'];
            echo $form->field($model, 'education['.$key.'][view]')->textInput(['value' => $val['view']])->label(Yii::t('app', 'education_view'));
            echo $form->field($model, 'education['.$key.'][period_from]')->widget(\yii\jui\DatePicker::classname(), [
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'value' => $val['period_from'],
                'options' => ['class' => 'form-control', 'value' => $val['period_from']]
            ])->label(Yii::t('app', 'education_period_from'));
            echo $form->field($model, 'education['.$key.'][period_to]')->widget(\yii\jui\DatePicker::classname(), [
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'value' => $val['period_to'],
                'options' => ['class' => 'form-control', 'value' => $val['period_to']]
            ])->label(Yii::t('app', 'education_period_to'));
            echo $form->field($model, 'education['.$key.'][name]')->textInput(['value' => $val['view']])->label(Yii::t('app', 'education_name'));
        }

    }else{
        echo $form->field($model, 'education[0][view]')->textInput([])->label(Yii::t('app', 'education_view'));
        echo $form->field($model, 'education[0][period_from]')->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
            'options' => ['class' => 'form-control']
        ])->label(Yii::t('app', 'education_period_from'));
        echo $form->field($model, 'education[0][period_to]')->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
            'options' => ['class' => 'form-control']
        ])->label(Yii::t('app', 'education_period_to'));
        echo $form->field($model, 'education[0][name]')->textInput([])->label(Yii::t('app', 'education_name'));

    }
    ?>

    <?= Html::a('Добавить еще одно учебное заведение', null, [
        'class' => 'btn btn-success',
        'data' => [
            'toggle' => 'reroute',
            'action' => Url::toRoute(['profile/fieldget?field=education[]', 'userId' => $model->id_user_profiles, 'count' => count($model->education)])
        ]
    ]) ?>
    <?php

    if(count($model->jobs) > 0 ) {
        foreach ($model->jobs as $key => $val) {

            echo $form->field($model, 'job['.$key.'][name_campany]')->textInput([ 'value' => $val['name_campany']])->label(Yii::t('app', 'job_name_campany'));
            echo $form->field($model, 'job['.$key.'][position]')->textInput(['value' => $val['position']])->label(Yii::t('app', 'job_position'));
            $model->job[$key]['period_from'] = $val['period_from'];
            $model->job[$key]['period_to'] = $val['period_to'];
            echo $form->field($model, 'job['.$key.'][period_from]')->widget(\yii\jui\DatePicker::classname(), [
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'value' => $val['period_from'],
                'options' => ['class' => 'form-control', 'value' => $val['period_from']]
            ])->label(Yii::t('app', 'job_period_from'));
            echo $form->field($model, 'job['.$key.'][period_to]')->widget(\yii\jui\DatePicker::classname(), [
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'value' => $val['period_to'],
                'options' => ['class' => 'form-control', 'value' => $val['period_to']]
            ])->label(Yii::t('app', 'job_period_to'));
        }

    }else{
        echo $form->field($model, 'job[0][name_campany]')->textInput([ ])->label(Yii::t('app', 'job_name_campany'));
        echo $form->field($model, 'job[0][position]')->textInput([])->label(Yii::t('app', 'job_position'));
        echo $form->field($model, 'job[0][period_from]')->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
            'options' => ['class' => 'form-control']
        ])->label(Yii::t('app', 'job_period_from'));
        echo $form->field($model, 'job[0][period_to]')->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
            'options' => ['class' => 'form-control']
        ])->label(Yii::t('app', 'job_period_to'));
    }
    ?>

    <?= Html::a('Добавить еще одно место работы', null, [
        'class' => 'btn btn-success',
        'data' => [
            'toggle' => 'reroute',
            'action' => Url::toRoute(['profile/fieldget', 'field' => 'job[]', 'userId' => $model->id_user_profiles, 'count' => count($model->job)])
        ]
    ]) ?>


    <?= $form->field($model, 'interest')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'experience')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'character')->textarea(['rows' => 6]) ?>

    <?php
    echo $form->field($model, 'smoke')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(app\modules\language\models\Language\Message::find()->where(['id' => 56])->asArray()->all(), 'id_pr', 'message'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    <?php
    echo $form->field($model, 'alchocol')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(app\modules\language\models\Language\Message::find()->where(['id' => 57])->asArray()->all(), 'id_pr', 'message'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?php

    if(count($model->foreignlanguages) > 0 ) {
        foreach ($model->foreignlanguages as $key => $val) {


        echo $form->field($model, 'foreignlanguage['.$key.'][list_language]')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(app\modules\language\models\Language\Message::find()->where(['id' => 37])->asArray()->all(), 'id_pr', 'message'),
            'language' => 'ru',

            'options' => ['placeholder' => 'Select a state ...', 'value' => $val['list_language'],],
            'pluginOptions' => [
                'name'=> 'UserProfiles[foreignlanguage]['.$key.'][list_language]',
                'allowClear' => true
            ],
        ]);
            echo $form->field($model, 'foreignlanguage['.$key.'][list_language_level]')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(app\modules\language\models\Language\Message::find()->where(['id' => 38])->asArray()->all(), 'id_pr', 'message'),
                'language' => 'ru',

                'options' => ['placeholder' => 'Select a state ...', 'value' => $val['list_language_level'],],
                'pluginOptions' => [
                    'name'=> 'UserProfiles[foreignlanguage]['.$key.'][list_language_level]',
                    'allowClear' => true
                ],
            ])->label(Yii::t('app', 'foreignlanguage_level'));

            echo $form->field($model, 'foreignlanguage['.$key.'][description]')->textarea(['rows' => 6, 'value' => $val['description']])->label(Yii::t('app', 'foreignlanguage_description'));
        }

    }else{
        echo $form->field($model, 'foreignlanguage[0][list_language]')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(app\modules\language\models\Language\Message::find()->where(['id' => 37])->asArray()->all(), 'id_pr', 'message'),
            'language' => 'ru',

            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'name'=> 'UserProfiles[foreignlanguage][0][list_language]',
                'allowClear' => true
            ],
        ]);
        echo $form->field($model, 'foreignlanguage[0][list_language_level]')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(app\modules\language\models\Language\Message::find()->where(['id' => 38])->asArray()->all(), 'id_pr', 'message'),
            'language' => 'ru',

            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'name'=> 'UserProfiles[foreignlanguage][0][list_language_level]',
                'allowClear' => true
            ],
        ])->label(Yii::t('app', 'foreignlanguage_level'));
        echo $form->field($model, 'foreignlanguage[0][description]')->textarea(['rows' => 6])->label(Yii::t('app', 'foreignlanguage_description'));
    }
    ?>

    <?= Html::a('Добавить еще один язык', null, [
        'class' => 'btn btn-success',
        'data' => [
            'toggle' => 'reroute',
            'action' => Url::toRoute(['profile/fieldget', 'field'=>'foreignlanguage[]', 'userId' => $model->id_user_profiles, 'count' => count($model->foreignlanguages)])
        ]
    ]) ?>

    <?= $form->field($model, 'social_net')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'client_avatar')->fileInput(['id' => 'fileupload', 'data-url' => "/user/profile/upload?id=".$model->id_user_profiles]) ?>

    <?= $form->field($model, 'client_background')->fileInput() ?>

    <?php
    echo $form->field($model, 'religion')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(app\modules\language\models\Language\Message::find()->where(['id' => 36])->asArray()->all(), 'id_pr', 'message'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?php
    echo $form->field($model, 'children')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(app\modules\language\models\Language\Message::find()->where(['id' => 34])->asArray()->all(), 'id_pr', 'message'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    <?php
    echo $form->field($model, 'contries_id_contries')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(\app\modules\reference\models\Countries\Countries::find()->asArray()->all(), 'id_countries', 'name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    <?php
    echo $form->field($model, 'towns_id_towns')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(\app\modules\reference\models\Towns\Towns::find()->asArray()->all(), 'id_towns', 'name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
 
<div class="modal" id="my-modal_file" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content " >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2>Ресайз изображения</h2>
            </div>
            <div class="modal-body cropper-content">
                <img src="" id="Cropper" alt="">
            </div>
            <div class="modal-footer">
                <div   type="button" id="download_img" class="btn btn-success">Сохранить</div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

   <div class="modal" id="my-modal_success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content " >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2>Сообщение</h2>
            </div>
            <div class="modal-body cropper-content">
                <p>Изображение обновленно</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal" >Ок</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    function updateTime()
    {
        data = $('form[action="/user/profile/update?id=<?=$model->id_user_profiles?>"]').serializeObject();
        data._csrf = '<?=Yii::$app->request->getCsrfToken()?>'

        $.ajax({
            url: '/user/profile/updatetime?id=<?=$model->id_user_profiles?>',
            data: data,
            type: 'POST',

            success: function()
            {

            },
            errors: function()
            {

            },
        });


    }
function uploadImage(){
        $('#fileupload').fileupload({
            dataType: 'json',
            done: function (e, data) {
                $.each(data.result, function (index, file) {

                    if (file.error!=undefined){
                        $('#fileupload_error').html(file.error).show();
                        return 0;
                    }
                    else{
                        console.log(file.file)
                        //$('#imageCropper').html($("#template-popup-image-cropper").tmpl({text: 'Изображение', file: file.file}));
                        $('#imageCropper').fadeIn(500);


                        $('#Cropper').attr('src', file.file)
                        $('#Cropper').cropper({

                            aspectRatio: 9 / 9,
                            crop: function(e) {

                            },
                            built: function () {
                                $(this).cropper('reset');
                            }
                        });
                        $('.cropper-canvas img').attr('src', file.file)
                        $('.cropper-view-box img').attr('src', file.file)
                        var modalContainer = $('#my-modal_file');
                        modalContainer.modal('show');

                        var $image = $('#Cropper');
                        $('#download_img').click(function(){
                            var $image = $('#Cropper');
                            var data = $(this).data();
                            result = $image.cropper('getCroppedCanvas', data.option, data.secondOption)
                            url = result.toDataURL('image/jpeg')
                            

                            $('#Cropper').cropper('getCroppedCanvas').toBlob(function (blob) {
                                var formData = new FormData();
                                formData.append('image', blob);

                                $.ajax('/user/profile/uploadcropper?id=<?=$model->id_user_profiles?>', {
                                    method: "POST",
                                    data: formData,
                                    processData: false,
                                    contentType: false,
                                    success: function () {
                                        var modalContainer = $('#my-modal_file');
                                        modalContainer.modal('hide');     
                                        var modalContainer = $('#my-modal_success');
                                        modalContainer.modal('show');
                                        
                                    },
                                    error: function () {
                                        console.log('Upload error');
                                    }
                                });
                            });

                        })

                        $('#fileupload_error').hide();
                    }

                    uploadImage();
                });
            }
        });
    }

    window.onload = function(){
        $.fn.serializeObject = function()
        {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function() {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };
        uploadImage();
        setInterval( updateTime,  10000 );
        count = 0;
        $(document).on('click', '[data-toggle=reroute]', function(e) {
            e.preventDefault();

            $this = $(this);

            data = $this.data();
            if(data.count == 0 || !data.count)
                data.count = count+1;
            else
                data.count = data.count + count + 1;
            console.log(data.count)
            id = data.id = 'data-id'+Math.random()
            url = data.action;

            $.ajax({
                url: url,
                data: data,
                type: 'POST',

                success: function (data) {
                    $this.before(data)

                    $('select[data-id="'+id+'"').select2({});
                    $('.datepicker').datepicker({})
                }
            });
        });

    }

    
</script>
