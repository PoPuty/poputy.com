<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\UserProfiles\UserProfiles */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Profiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profiles-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_user_profiles], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_user_profiles], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_user_profiles',
            'name',
            'second_name',
            'surname',
            'sex',
            'zodiac',
            'marital_status',
            'education',
            'job',
            'interest',
            'experience',
            'character',
            'smoke',
            'alchocol',
            'foreign_language',
            'social_net',
            'photo',
            'background',
            'religion',
            'children',
            'users_id_users',
            'contries_id_contries',
            'towns_id_towns',
            'towns_contries_id_contries',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
