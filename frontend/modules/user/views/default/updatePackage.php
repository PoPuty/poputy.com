<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\modules\user\models\UserProfiles\UserProfiles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-profiles-form">

    <?php
    foreach($packages as $val)
    {
       echo DetailView::widget([
            'model' => $val,
            'attributes' => [
                'id_package',
                'name',
                'livetime',
                'price',

            ],
        ]);

    }
    ?>

</div>
