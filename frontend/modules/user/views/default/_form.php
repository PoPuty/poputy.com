<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\UserProfiles\UserProfiles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-profiles-form">

    <?php $form = ActiveForm::begin(['class' => 'update_user']); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'disabled' => 'true']) ?>
    <?= $form->field($model, 'code')->textInput(['maxlength' => true, 'value' => '']) ?>


    <div class="form-group">
        <?= Html::Button($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['type' => 'button', 'class' => $model->isNewRecord ? 'btn btn-success user_update' : 'btn btn-primary user_update']) ?>
        <?= Html::Button('Запросить код', ['type' => 'button', 'class' => $model->isNewRecord ? 'btn btn-success getCode' : 'btn btn-primary getCode']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    window.onload=function(){

        $('#user-code').keyup(function()
        {
            $.ajax({
                url: '/user/default/codevalidate?code='+$('#user-code').val(),
                type: 'post',
                data: {},
                success: function (response) {
                    response = jQuery.parseJSON(response)
                    if(response.status == 'true')
                    {
                        $('#user-email').removeAttr('disabled');

                    }else{

                    }
                }
            });
        })
        $('.user_update').click(function(){



        $.ajax({
            url: '/user/default/updateemail?code='+$('#user-code').val()+'&email='+$('#user-email').val(),
            type: 'post',
            data: {},
            success: function (response) {
                response = jQuery.parseJSON(response)
                if(response.status == 'true') {
                    alert('Данные успешно обновленны')
                    location.reload()
                }else{
                    alert('Код не найден')
                }
            }
        });
            return false;
        });

        $('.getCode').click(function(){


        // submit form
        $.ajax({
            url: '/user/default/getcode',
            type: 'post',
            data: {},
            success: function (response) {
                alert('Код отправлен на почту ')
            }
        });
            return false;
        });
    }
</script>