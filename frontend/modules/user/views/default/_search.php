<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\UserProfilesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-profiles-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_user_profiles') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'second_name') ?>

    <?= $form->field($model, 'surname') ?>

    <?= $form->field($model, 'sex') ?>

    <?php // echo $form->field($model, 'zodiac') ?>

    <?php // echo $form->field($model, 'marital_status') ?>

    <?php // echo $form->field($model, 'education') ?>

    <?php // echo $form->field($model, 'job') ?>

    <?php // echo $form->field($model, 'interest') ?>

    <?php // echo $form->field($model, 'experience') ?>

    <?php // echo $form->field($model, 'character') ?>

    <?php // echo $form->field($model, 'smoke') ?>

    <?php // echo $form->field($model, 'alchocol') ?>

    <?php // echo $form->field($model, 'foreign_language') ?>

    <?php // echo $form->field($model, 'social_net') ?>

    <?php // echo $form->field($model, 'photo') ?>

    <?php // echo $form->field($model, 'background') ?>

    <?php // echo $form->field($model, 'religion') ?>

    <?php // echo $form->field($model, 'children') ?>

    <?php // echo $form->field($model, 'users_id_users') ?>

    <?php // echo $form->field($model, 'contries_id_contries') ?>

    <?php // echo $form->field($model, 'towns_id_towns') ?>

    <?php // echo $form->field($model, 'towns_contries_id_contries') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
