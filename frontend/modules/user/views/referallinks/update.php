<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\ReferalLinks\ReferalLinks */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Referal Links',
]) . $model->id_referal_links;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Referal Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_referal_links, 'url' => ['view', 'id' => $model->id_referal_links]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="referal-links-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
