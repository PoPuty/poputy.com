<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\ReferalLinks\ReferalLinks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="referal-links-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'hash_link')->textInput(['maxlength' => true, 'disabled' => 'true']) ?>
    <?= $form->field($model, 'active')->hiddenInput(['maxlength' => true, 'value' => '1'])->label(false) ?>
    <?= $form->field($model, 'email')->hiddenInput(['maxlength' => true, 'value' => '1'])->label(false) ?>
    <?= $form->field($model, 'users_id_users')->hiddenInput(['maxlength' => true, 'value' => Yii::$app->user->id])->label(false) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
