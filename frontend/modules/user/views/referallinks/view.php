<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\ReferalLinks\ReferalLinks */

$this->title = $model->id_referal_links;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Referal Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="referal-links-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_referal_links], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_referal_links], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_referal_links',
            'active',
            'hash_link',
            'users_id_users',
        ],
    ]) ?>

</div>
