<?php

namespace app\modules\reference\models\Jobs;

use Yii;
use app\modules\user\models\UserProfiles\UserProfiles;
/**
 * This is the model class for table "jobs".
 *
 * @property integer $id
 * @property string $description
 * @property integer $user_profile_id
 *
 * @property UserProfiles $userProfile
 */
class Jobs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jobs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_profile_id'], 'integer'],
            [['description','period_from', 'period_to', 'name_campany', 'position'], 'string', 'max' => 255],
            [['user_profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserProfiles::className(), 'targetAttribute' => ['user_profile_id' => 'id_user_profiles']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'Description'),
            'user_profile_id' => Yii::t('app', 'User Profile ID'),
            'period_from' => Yii::t('app', 'period_from'),
            'period_to' => Yii::t('app', 'period_to'),
            'name_campany' => Yii::t('app', 'name_campany'),
            'position' => Yii::t('app', 'position'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfiles::className(), ['id_user_profiles' => 'user_profile_id']);
    }
}
