<?php

namespace app\modules\reference\models\PackageParcels;

use Yii;

/**
 * This is the model class for table "package_parcels".
 *
 * @property integer $id
 * @property string $name
 * @property string $weight
 * @property string $dimensions
 * @property integer $appl_id
 * @property integer $brittle
 * @property string $composition
 * @property integer $price
 * @property integer $currency
 */
class PackageParcels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package_parcels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['appl_id', 'brittle', 'price', 'currency'], 'integer'],
            [['composition'], 'string'],
            [['name', 'weight', 'dimensions'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'weight' => Yii::t('app', 'Weight'),
            'dimensions' => Yii::t('app', 'Dimensions'),
            'appl_id' => Yii::t('app', 'Appl ID'),
            'brittle' => Yii::t('app', 'Brittle'),
            'composition' => Yii::t('app', 'Composition'),
            'price' => Yii::t('app', 'Price'),
            'currency' => Yii::t('app', 'Currency'),
        ];
    }
}
