<?php

namespace app\modules\reference\models\Referallinks;

use Yii;

/**
 * This is the model class for table "referal_links".
 *
 * @property integer $id_referal_links
 * @property integer $active
 * @property string $hash_link
 * @property integer $users_id_users
 *
 * @property User $usersIdUsers
 */
class Referallinks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'referal_links';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active', 'users_id_users'], 'integer'],
            [['hash_link', 'email', 'link'], 'string', 'max' => 255],
            [['users_id_users'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['users_id_users' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_referal_links' => 'Id Referal Links',
            'active' => 'Active',
            'hash_link' => 'Hash Link',
            'users_id_users' => 'Users Id Users',
            'email' => 'email',
            'link' => 'link',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersIdUsers()
    {
        return $this->hasOne(User::className(), ['id' => 'users_id_users']);
    }
}
