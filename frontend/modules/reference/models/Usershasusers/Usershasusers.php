<?php

namespace app\modules\reference\models\Usershasusers;

use Yii;

/**
 * This is the model class for table "users_has_users".
 *
 * @property integer $users_id_users
 */
class Usershasusers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_has_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id_users'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'users_id_users' => 'Users Id Users',
        ];
    }
}
