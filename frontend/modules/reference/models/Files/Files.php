<?php

namespace app\modules\reference\models\Files;

use Yii;

/**
 * This is the model class for table "files".
 *
 * @property integer $id
 * @property string $name
 * @property string $path
 * @property integer $appl_id
 * @property string $exten
 * @property integer $author_id
 */
class Files extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'path', 'exten'], 'required'],
            [['appl_id', 'author_id'], 'integer'],
            [['name', 'path', 'exten'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'path' => Yii::t('app', 'Path'),
            'appl_id' => Yii::t('app', 'Appl ID'),
            'exten' => Yii::t('app', 'Exten'),
            'author_id' => Yii::t('app', 'Author ID'),
        ];
    }
}
