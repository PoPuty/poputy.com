<?php

namespace app\modules\reference\models\Foreignlanguage;

use Yii;
use app\modules\user\models\UserProfiles\UserProfiles;
/**
 * This is the model class for table "foreign_language".
 *
 * @property integer $id
 * @property string $description
 * @property integer $user_profile_id
 *
 * @property UserProfiles $userProfile
 */
class Foreignlanguage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foreign_language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_profile_id', 'lang_id', 'lang_level_id', 'list_language_level', 'list_language'], 'integer'],
            [['description'], 'string', 'max' => 255],
            [['user_profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserProfiles::className(), 'targetAttribute' => ['user_profile_id' => 'id_user_profiles']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'Description'),
            'user_profile_id' => Yii::t('app', 'User Profile ID'),
            'lang_id' => Yii::t('app', 'lang_id'),
            'lang_level_id' => Yii::t('app', 'lang_level_id'),
            'list_language_level' => Yii::t('app', 'list_language_level'),
            'list_language' => Yii::t('app', 'list_language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfiles::className(), ['id_user_profiles' => 'user_profile_id']);
    }
}
