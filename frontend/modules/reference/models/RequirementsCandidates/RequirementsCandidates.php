<?php

namespace app\modules\reference\models\RequirementsCandidates;

use Yii;

/**
 * This is the model class for table "requirements_candidates".
 *
 * @property integer $id
 * @property integer $id_list_person_characteristics
 * @property integer $id_sublist_person_characteristcs
 * @property integer $appl_id
 * @property integer $user_id
 */
class RequirementsCandidates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requirements_candidates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_list_person_characteristics', 'id_sublist_person_characteristcs', 'appl_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_list_person_characteristics' => Yii::t('app', 'Id List Person Characteristics'),
            'id_sublist_person_characteristcs' => Yii::t('app', 'Id Sublist Person Characteristcs'),
            'appl_id' => Yii::t('app', 'Appl ID'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }
}
