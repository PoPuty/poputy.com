<?php

namespace app\modules\reference\models\Towns;

use Yii;

/**
 * This is the model class for table "towns".
 *
 * @property integer $id_towns
 * @property string $name
 * @property integer $contries_id_contries
 */
class Towns extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'towns';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contries_id_contries'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_towns' => 'Id Towns',
            'name' => 'Name',
            'contries_id_contries' => 'Contries Id Contries',
        ];
    }
}
