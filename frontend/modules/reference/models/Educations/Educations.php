<?php

namespace app\modules\reference\models\Educations;

use Yii;
use app\modules\user\models\UserProfiles\UserProfiles;
/**
 * This is the model class for table "educations".
 *
 * @property integer $id
 * @property string $description
 * @property integer $user_profile_id
 *
 * @property UserProfiles $userProfile
 */
class Educations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'educations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_profile_id'], 'integer'],
            [['description', 'period_from', 'period_to', 'name', 'view'], 'string', 'max' => 255],
            [['user_profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserProfiles::className(), 'targetAttribute' => ['user_profile_id' => 'id_user_profiles']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'Description'),
            'user_profile_id' => Yii::t('app', 'User Profile ID'),
            'period_from' => Yii::t('app', 'period_from'),
            'period_to' => Yii::t('app', 'period_to'),
            'name' => Yii::t('app', 'education_name'),
            'view' => Yii::t('app', 'education_view'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfiles::className(), ['id_user_profiles' => 'user_profile_id']);
    }
}
