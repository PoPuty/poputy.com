<?php

namespace app\modules\reference\models\Countries;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property integer $id_countries
 * @property string $name
 * @property integer $id_countries_pr
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_countries_pr'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_countries' => 'Id Countries',
            'name' => 'Name',
            'id_countries_pr' => 'Id Countries Pr',
        ];
    }
}
