<?php
namespace frontend\models;

use yii\base\Model;
use app\modules\user\models\UserProfiles\UserProfiles;
use common\models\User;
use yii\helpers\Url;
/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_repeat;
    public $confirm;
    public $hash_link;
    public $invite;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            [['username'], 'string', 'min' => 2, 'max' => 255],
            [['invite'], 'integer'],

            ['confirm', 'compare', 'compareValue'=>1, 'message'=>'Вы должны согласится с правилами'],
            ['email', 'trim'],
            ['email', 'required'],
            ['hash_link', 'safe'],
            ['invite', 'safe'],

            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'required'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],

        ];
    }
    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'password_repeat' => 'Повторите Пароль',
            'confirm' => 'Согласие с правилами',
        ];
    }
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {

        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->email;
        $user->email = $this->email;
        $user->status = User::STATUS_DELETED;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        //
        $to = $user->email;
        $subject = "Welcome To GhazaliTajuddin.com!";
        $message = "Thank you for joining!, we have sent you a separate email that contains your activation link";
        $from = "FROM: mr.ghazali@gmail.com";



        //echo $to.$subject.$message.$from;

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: Mr. Ghazali < mr.ghazali@gmail.com>' . "\r\n";

        $subject2 = "Your Activation Link";

        $message2 = "<html><body>Please click this below to activate your membership<br />".
            Url::toRoute('/site/activate?email='.$user->email.'&auth_key='.$user->auth_key, array('email' => $user->email, 'auth_key' => $user->auth_key)).

            "

                                                                   Thanks you.
                                                                   ". sha1(mt_rand(10000, 99999).time().$user->email) ."
                                                                   </body></html>";
        if(isset($this->invite) && isset($this->hash_link) && $this->invite != '')
        {
            $user->invite = $this->invite;
            $user->status = User::STATUS_ACTIVE;
            unset($this->hash_link);
            unset($this->invite);
        }else{
            mail($to, $subject2, $message2, $headers);
        }




        //
        $res = $user->save();



        $userP = new UserProfiles();
        $userP->users_id_users = $user->id;
        $userP->created_at = time();
        $userP->updated_at = time();
        $userP->save();

        return $res ? $user : null;
    }
}
