<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\User;
use app\modules\articles\models\ArticleCategories\ArticleCategories;
use app\modules\articles\models\ArticleItems\ArticleItems;
use app\modules\articles\models\Tags\Tags;

use yii\bootstrap\ButtonDropdown;
AppAsset::register($this);

$tags = Tags::find()->limit(10)->orderBy(['id_tag'=>SORT_DESC])->all();
$cat = ArticleCategories::find()->all();
$art = ArticleItems::find()->limit(7)->orderBy(['created'=>SORT_DESC])->all();
$slider = ArticleItems::find()->limit(10)->where(['slider' => 1])->orderBy(['created'=>SORT_DESC])->all();

$art_Training = ArticleItems::find()->where(['catid' => 2])->limit(2)->orderBy(['created'=>SORT_DESC])->all();
$art_Health = ArticleItems::find()->where(['catid' => 4])->limit(3)->orderBy(['created'=>SORT_DESC])->all();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" type="text/css" media="(max-width:768px)" href="/css/responsive-0.css">
    <link rel="stylesheet" type="text/css" media="(min-width:769px) and (max-width:992px)" href="/css/responsive-768.css">
    <link rel="stylesheet" type="text/css" media="(min-width:993px) and (max-width:1200px)" href="/css/responsive-992.css">
    <link rel="stylesheet" type="text/css" media="(min-width:1201px)" href="/css/responsive-1200.css">
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:300,300italic,400,400italic,700,700italic' rel='stylesheet' type='text/css'>

</head>
<body>
<?php $this->beginBody() ?>
<?php
$user = User::findOne(['id'=> Yii::$app->user->id]);


use  yii\web\Session;

$session = new Session;
$session->open();
$lang = $session->get('languages');

?>
 <div id="wrapper" class="wide">      

 <header id="header" role="banner">                
            <!-- Header meta -->
            <div class="header_meta">
                <div class="container">
                 
                    <!-- Top menu -->
                    <nav class="top_navigation" role="navigation">
                        <span class="top_navigation_toggle"><i class="fa fa-reorder"></i></span>
                        <ul class="menu">
                            <?php
                                foreach ($cat as $key => $value) {
                                    switch($lang)
                                    {
                                        case 'en': $value->name = $value->name_en;  break;
                                        case 'ru': $value->name = $value->name_ru;   break;
                                        case 'fi': $value->name = $value->name_fi;   break;

                                    }
                                   ?>
                                    <li><a href="/<?=$value->id?>/<?=$value->alias?>/"><?=$value->name;?></a></li>
                                   <?php
                                }
                            ?>

                            
                                
                            
                            <li class="search_icon_form"><a href="#"><i class="fa fa-search"></i></a>
                                <div class="sub-search">
                                    <form method="get" action="/search">
                                        <input type="search" name="search" placeholder="Search...">
                                        <input type="submit" value="Search">
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </nav><!-- End Top menu -->
                </div>
            </div><!-- End Header meta -->
        <div id="header_main" class="sticky header_main">
                <div class="container">
                    <!-- Logo -->
                    <div class="site_brand">
                        <h1 id="site_title"><a href="/"><img src="/svg/trenniajakiri_logo.svg"></a></h1>
                        <h2 id="site_description"><?=Yii::t('frontend', 'Digi Magazine');?></h2>
                        <!--
                        <a href="#">
                            <img src="images/TrendyBlog.png" alt="Logo">
                        </a>
                        -->
                    </div><!-- End Logo -->
                    <!-- Site navigation -->
                    <nav class="site_navigation" role="navigation">
                        <span class="site_navigation_toggle"><i class="fa fa-reorder"></i></span>
                        <ul class="menu">
                            
                            
                           

                            <?php
                            if (Yii::$app->user->isGuest) { ?>
                                <li><a href="/site/languages?l=fi">Fin</a></li>
								<li><a href="/site/languages?l=ee">Est</a></li>
                                <li><a href="/site/languages?l=ru">Rus</a></li>
                                <li><a href="/site/languages?l=en">Eng</a></li>
                                <li><a href="/site/login">Login</a></li>
                                <li><a href="/site/signup">Signup</a></li>
                            <?php
                            }else{?>
                                <li><a href="/site/languages?l=fi">Fin</a></li>
								<li><a href="/site/languages?l=ee">Est</a></li>
                                <li><a href="/site/languages?l=ru">Rus</a></li>
                                <li><a href="/site/languages?l=en">Eng</a></li>
                                 <li>
                                    <?php

                                        echo Html::beginForm(['/site/logout'], 'post')
                                        . Html::submitButton(
                                            'Logout (' . Yii::$app->user->identity->username  . ')',
                                            ['class' => 'btn btn-link logout']
                                        )
                                        . Html::endForm();
                                    ?>
                                        <?php if(isset(Yii::$app->user->identity)): ?>
                                        <ul class="sub-menu">
                                            <li><a href="<?='/user/default/update?id='.Yii::$app->user->identity->id?>"><?=Yii::t('frontend', 'change_data');?></a></li>
                                            <li><a href="/user/default/updatepackage"><?=Yii::t('frontend', 'Subscribing');?></a></li>

                                        </ul>
                                    </li>
                                    <?php endif; } ?>    
                            
                        </ul>
                    </nav><!-- End Site navigation -->
                </div>
            </div><!-- End Header main -->

   </header>
        <?php if($_SERVER['REQUEST_URI'] == '/'): ?>
<section>
    <?= Alert::widget() ?>
            <div class="container">
                <div class="row">
                    <!-- Main content -->
                    <div class="col col_9_of_12">
                        <!-- Content slider -->
                        <div class="content_slider">
                            <ul>
                                <!-- Item -->
                                <?php 
                                    foreach ($slider as $key => $value) {

                                        switch($lang)
                                        {
                                            case 'en': $value->title = $value->title_en; $value->category->name = $value->category->name_en; break;
                                            case 'ru': $value->title = $value->title_ru; $value->category->name = $value->category->name_ru;  break;
                                            case 'fi': $value->title = $value->title_fi; $value->category->name = $value->category->name_fi;  break;

                                        }
                                       ?>

                                       <li>
                                            <a href="<?=$value->url;?>"><img src="<?=$value->thumbpath;?>" alt="Slider"></a>
                                            <div class="slider_caption">
                                                <div class="thumb_meta">
                                                    <span class="category" jQuery><a href="<?=$value->caturl?>"><?=$value->category->name;?></a></span>
                                                  
                                                </div>
                                                <div class="thumb_link">
                                                    <h3><a href="<?=$value->url;?>"><?=$value->title?></a></h3>
                                                </div>
                                            </div>
                                        </li>
                                       <?php
                                    }
                                ?>
                                <!-- End Item -->
                                <!-- Item -->
                                
                            </ul>
                        </div><!-- End Content slider -->
                     
                        <div class="row">
                            <div class="col col_12_of_12">
                                <div class="multipack clearfix">
                                    <!-- Layout post 1 -->
                                    <div class="layout_post_1">
                                        <div class="item_thumb">
                                            <div class="thumb_icon">
                                                <?php
                                                switch($lang)
                                                {
                                                    case 'en': $art[0]->title = $art[0]->title_en; $art[0]->category->name = $art[0]->category->name_en; $art[0]->introtext = $art[0]->introtext_en; break;
                                                    case 'ru': $art[0]->title = $art[0]->title_ru; $art[0]->category->name = $art[0]->category->name_ru; $art[0]->introtext = $art[0]->introtext_ru;  break;
                                                    case 'fi': $art[0]->title = $art[0]->title_fi; $art[0]->category->name = $art[0]->category->name_fi; $art[0]->introtext = $art[0]->introtext_fi;  break;

                                                }
                                                ?>
                                                <a href="<?=$art[1]->url;?>" jQuery><i class="fa fa-copy"></i></a>
                                            </div>
                                            <div class="thumb_hover">
                                                <a href="<?=$art[1]->url;?>"><img src="<?=$art[0]->thumbpath;?>" alt="Post"></a>
                                            </div>
                                            <div class="thumb_meta">
                                                <span class="category" jQuery><a href="<?=$value->caturl?>"><?=$art[0]->category->name;?></a></span>
                                                
                                            </div>
                                        </div>
                                        <div class="item_content">
                                            <h4><a href="<?=$art[0]->url;?>"><?=$art[0]->title;?></a></h4>
                                            <p><?=$art[0]->introtext;?> [...]</p>
                                        </div>
                                    </div><!-- End Layout post 1 -->
                                    <!-- Post lists -->
                                    <div class="list_posts">
                                    <?php 
                                    foreach ($art as $key => $value) {
                                        if($key <= 1) continue;
                                        switch($lang)
                                        {
                                            case 'en': $value->title = $value->title_en;  break;
                                            case 'ru': $value->title = $value->title_ru;  break;
                                            case 'fi': $value->title = $value->title_fi;  break;

                                        }
                                       ?>
                                        <!-- Post -->
                                        <div class="post clearfix">
                                            <div class="item_thumb">
                                                <div class="thumb_icon">
                                                    <a href="<?=$value->url;?>" jQuery><i class="fa fa-copy"></i></a>
                                                </div>
                                                <div class="thumb_hover">
                                                    <a href="<?=$value->url;?>"><img src="<?=$value->thumbpath;?>" alt="Post"></a>
                                                </div>
                                            </div>
                                            <div class="item_content">
                                                
                                                <h4><a href="<?=$value->url;?>"><?=$value->title;?></a></h4>
                                                <div class="item_meta clearfix">
                                                    <span class="meta_date"><?=$value->dateformat;?></span>
                                                   
                                                </div>
                                            </div>
                                        </div><!-- End Post -->

                                        <?php };?>
                                        <!-- Post -->
                                        
                                    </div><!-- End Post lists -->
                                </div>
                            </div>
                        </div>

                        <!-- Panel title -->
                        <div class="panel_title">
                            <div>
                                <h4><a href="/2/training"><?=Yii::t('frontend', 'Training');?></a></h4>
                            </div>
                        </div><!-- End Panel title -->
                        <!-- Layout post 2 -->
                        <div class="row">
                            <?php foreach ($art_Training as $key => $value) {
                                switch($lang)
                                {
                                    case 'en': $value->title = $value->title_en; $value->category->name = $value->category->name_en; $value->introtext = $value->introtext_en;  break;
                                    case 'ru': $value->title = $value->title_ru; $value->category->name = $value->category->name_ru; $value->introtext = $value->introtext_ru;  break;
                                    case 'fi': $value->title = $value->title_fi; $value->category->name = $value->category->name_fi; $value->introtext = $value->introtext_fi;  break;

                                }
                               ?>


                            <div class="col col_12_of_12">
                                <!-- Layout post 1 -->
                                <div class="layout_post_2 clearfix">
                                    <div class="item_thumb">
                                        <div class="thumb_icon">
                                            <a href="<?=$value->url;?>" jQuery><i class="fa fa-copy"></i></a>
                                        </div>
                                        <div class="thumb_hover">
                                            <a href="<?=$value->url;?>"><img src="<?=$value->thumbpath;?>" alt="Post"></a>
                                        </div>
                                        <div class="thumb_meta">
                                            <span class="category" jQuery><a href="<?=$value->caturl;?>"><?=$value->category->name;?></a></span>
                                            
                                        </div>
                                    </div>
                                    <div class="item_content">
                                        <h4><a href="post_single.html"><?=$value->title;?></a></h4>
                                        <p><?=$value->introtext;?> [...]</p>
                                        <div class="item_meta clearfix">
                                            <span class="meta_date"><?=$value->dateformat;?></span>
                                           
                                        </div>
                                    </div>
                                </div><!-- End Layout post 1 -->
                            </div>
                            <?php  } ?>
                            
                        </div><!-- End Layout post 2 -->
                        <!-- Panel title -->



                        </div>
                        <!-- Sidebar -->
                    <div class="col col_3_of_12">
                        <!-- Widget Search widget -->
                        <div class="widget">
                            <div class="widget_title"><h3><?=Yii::t('frontend', 'Search widget');?></h3></div>
                            <div class="tb_widget_search">
                                <form method="get" action="/search">
                                    <input type="text" name ="search">
                                    <input type="submit" value="Search">
                                </form>
                            </div>
                        </div><!-- End Widget Search widget -->
                        <!-- Widget Social widget -->
                        <div class="widget">
                            <div class="widget_title"><h3><?=Yii::t('frontend', 'Socialize');?></h3></div>
                            <div class="tb_widget_socialize clearfix">
                                <a href="http://www.facebook.com/" target="_blank" class="icon facebook">
                                    <div class="symbol"><i class="fa fa-facebook"></i></div>
                                    <div class="text"><p>46,841</p><p>Facebook</p></div>
                                </a>
                                <a href="https://plus.google.com" target="_blank"  class="icon google">
                                    <div class="symbol"><i class="fa fa-google-plus"></i></div>
                                    <div class="text"><p>17,045</p><p>Google+</p></div>
                                </a>
                                <a href="http://www.twitter.com/" target="_blank"  class="icon twitter">
                                    <div class="symbol"><i class="fa fa-twitter"></i></div>
                                    <div class="text"><p>3,075</p><p>Twitter</p></div>
                                </a>
                                <a href="http://www.linkedin.com/" target="_blank"  class="icon linkedin">
                                    <div class="symbol"><i class="fa fa-linkedin"></i></div>
                                    <div class="text"><p>15,441</p><p>LinkedIn</p></div>
                                </a>
                            </div>
                        </div><!-- End Widget Social widget -->
                       
                        
                        <!-- Widget Banners 125 -->
                        <div class="widget" style="display: none">
                            <div class="widget_title"><h3><?=Yii::t('frontend', 'Banner');?></h3></div>
                            <div class="tb_widget_banner_125 clearfix">
                                <a href="http://themeforest.net/user/different-themes/portfolio?ref=CodeoStudio" target="_blank">
                                    <img src="demo/125x125.png" alt="Banner">
                                </a>
                                <a href="http://themeforest.net/user/different-themes/portfolio?ref=CodeoStudio" target="_blank">
                                    <img src="demo/125x125.png" alt="Banner">
                                </a>
                                <a href="http://themeforest.net/user/different-themes/portfolio?ref=CodeoStudio" target="_blank">
                                    <img src="demo/125x125.png" alt="Banner">
                                </a>
                                <a href="http://themeforest.net/user/different-themes/portfolio?ref=CodeoStudio" target="_blank">
                                    <img src="demo/125x125.png" alt="Banner">
                                </a>
                            </div>
                        </div><!-- End Widget Banners 125 -->
                       
                        <!-- Widget posts -->
                        <div class="widget">
                            <div class="widget_title"><h3><?=Yii::t('frontend', 'Latest posts');?></h3></div>
                            <div class="tb_widget_posts_big clearfix">
                                <?php
                                foreach ($art as $key => $value) {
                                    if($key >= 3 ) break;
                                    switch($lang)
                                    {
                                        case 'en': $value->title = $value->title_en; $value->category->name = $value->category->name_en; $value->introtext = $value->introtext_en;  break;
                                        case 'ru': $value->title = $value->title_ru; $value->category->name = $value->category->name_ru; $value->introtext = $value->introtext_ru;  break;
                                        case 'fi': $value->title = $value->title_fi; $value->category->name = $value->category->name_fi; $value->introtext = $value->introtext_fi;  break;

                                    }
                                    ?>
                                <!-- Post item -->
                                <div class="item clearfix">
                                    <div class="item_content">
                                        <h4><a href="<?=$value->url;?>"><span class="format" jQuery><?=$value->category->name;?></span><?=$value->title;?></a></h4>
                                        <p><?=$value->introtextlitle;?> [...]</p>
                                        <div class="item_meta clearfix">
                                            <span class="meta_date"><?=$value->dateformat;?></span>

                                        </div>
                                    </div>
                                </div><!-- End Post item -->
                                <?php }?>
                            </div>
                        </div><!-- End Widget posts -->
                        <!-- Widget Tags -->
                        <div class="widget">
                            <div class="widget_title"><h3><?=Yii::t('frontend', 'Tags');?></h3></div>
                            <div class="tb_widget_tagcloud clearfix">
                                <?php foreach ($tags as $key => $value) {
                                    switch($lang)
                                    {
                                        case 'en': $value->name = $value->name_en;  break;
                                        case 'ru': $value->name = $value->name_ru;   break;
                                        case 'fi': $value->name = $value->name_fi;   break;

                                    }
                                    ?>
                                    <a href="/tags?tag=<?=$value->id_tag;?>"><?=$value->name;?></a>
                                <?php }?>
                                
                            </div>
                        </div><!-- End Widget Tags -->
                    </div><!-- End Sidebar -->
                        </div>

                        <div class="panel_title">
                    <div>
                        <h4><a href="blog.html"><?=Yii::t('frontend', 'Movies');?></a></h4>
                    </div>
                </div><!-- End Panel title -->
                <div class="row">

                <?php foreach ($art_Health as $key => $value) {
                    switch($lang)
                    {
                        case 'en': $value->introtext = $value->introtext_en; $value->title = $value->title_en; $value->category->name = $value->category->name_en;  break;
                        case 'ru': $value->introtext = $value->introtext_ru; $value->title = $value->title_ru; $value->category->name = $value->category->name_ru;   break;
                        case 'fi': $value->introtext = $value->introtext_fi; $value->title = $value->title_fi; $value->category->name = $value->category->name_fi;   break;

                    }
                    ?>
                    <div class="col col_4_of_12">
                        <!-- Layout post 1 -->
                        <div class="layout_post_1">
                            <div class="item_thumb">
                                <div class="thumb_icon">
                                    <a href="<?=$value->url;?>" jQuery><i class="fa fa-copy"></i></a>
                                </div>
                                <div class="thumb_hover">
                                    <a href="<?=$value->url;?>"><img src="<?=$value->thumbpath;?>" alt="Post"></a>
                                </div>
                                <div class="thumb_meta">
                                    <span class="category" jQuery><a href="<?=$value->caturl;?>"><?=$value->category->name;?></a></span>
                                    
                                </div>
                            </div>
                            <div class="item_content">
                                <h4><a href="post_single.html"><?=$value->title;?></a></h4>
                                <div class="item_meta clearfix">
                                    <span class="meta_date"><?=$value->dateformat;?></span>
                                   
                                </div>
                                <p><?=$value->introtext;?> [...]</p>
                            </div>
                        </div><!-- End Layout post 1 -->
                    </div>
                <?php };?>
                    
                </div>
                        </div>
                        </section>
        <?php else:?>
            <section>
            <div class="container">
                <div class="row">
				
                    <?= Breadcrumbs::widget([
							'homeLink' => [ 
							  'label' => \Yii::t('frontend', 'Main page'),
							  'url' => Yii::$app->homeUrl,
						 ],
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <?= Alert::widget() ?>
                    <?= $content ?>
                </div>
            </div>
            </section>
        <?php endif;?>

    
</div>

<footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col col_3_of_12">
                        <!-- Widget Text widget -->
                        <div class="widget contacts">
                            <div class="widget_title"><h3><?=Yii::t('frontend', 'Trenniajakiri.ee');?></h3></div>
                            <span><?=Yii::t('frontend', 'Ruby Fox OÜ');?></span>
                            <span><?=Yii::t('frontend', 'Registrikood: 14217759');?></span>
                            <span><a href="tel:+37258667771"><?=Yii::t('frontend', 'telefon');?></a></span>
                        </div><!-- End Widget text widget -->
                        <!-- Widget Social widget -->
                        <div class="widget">
                            <div class="widget_title"><h3>Socialize</h3></div>
                            <div class="tb_widget_socialize clearfix">
                                <a href="http://www.facebook.com/" target="_blank" class="icon facebook">
                                    <div class="symbol"><i class="fa fa-facebook"></i></div>
                                    <div class="text"><p>46,841</p><p>Facebook</p></div>
                                </a>
                                <a href="https://plus.google.com" target="_blank"  class="icon google">
                                    <div class="symbol"><i class="fa fa-google-plus"></i></div>
                                    <div class="text"><p>17,045</p><p>Google+</p></div>
                                </a>
                            </div>
                        </div><!-- End Widget Social widget -->
                    </div>
                    <div class="col col_3_of_12">
                        
                    </div>
                    <div class="col col_3_of_12">
                        <!-- Widget recent posts -->
                        <div class="widget">
                            <div class="widget_title"><h3><?=Yii::t('frontend', 'Recent posts');?></h3></div>
                            <div class="tb_widget_recent_list clearfix">
                                <?php
                                foreach ($art as $key => $value) {
                                    if($key >= 3 ) break;
                                    switch($lang)
                                    {
                                        case 'en': $value->title = $value->title_en;  break;
                                        case 'ru': $value->title = $value->title_ru;  break;
                                        case 'fi': $value->title = $value->title_fi;  break;

                                    }
                                    ?>
                                <!-- Post item -->
                                <div class="item clearfix">
                                    <div class="item_thumb">
                                        <div class="thumb_icon">
                                            <a href="<?=$value->url;?>"><i class="fa fa-copy"></i></a>
                                        </div>
                                        <div class="thumb_hover">
                                            <a href="<?=$value->url;?>"><img src="<?=$value->thumbpath;?>" alt="Post"></a>
                                        </div>
                                    </div>
                                    <div class="item_content">
                                        <h4><a href="<?=$value->url;?>"><?=$value->title;?></a></h4>
                                        <div class="item_meta clearfix">
                                            <span class="meta_date"><?=$value->dateformat;?></span>
                                            
                                        </div>
                                    </div>
                                </div><!-- End Post item -->
                                <?php }?>
                                </div><!-- End Post item -->
                            </div>
                        </div><!-- End Widget recent posts -->
                    </div>
                    <div class="col col_3_of_12">
                       
                    </div>
                </div>
            </div>
        </footer><!-- End Footer -->
        <!-- Copyright and rules -->
        <div id="copyright" role="contentinfo">
            <div class="container">
                <p>&copy; 2017 <a href="#">Trenniajakiri.ee.</a><?=Yii::t('frontend', 'All rights reserved.');?> </p><span><a href="/site/uldtingimused"><?=Yii::t('frontend', 'Uldtingimused');?></a><a href="/site/kasutus"><?=Yii::t('frontend', 'Kasutus- ja uldtingimused');?></a></span> 
            </div>
        </div><!-- End Copyright -->
    </div><!-- End Wrapper -->

<?php $this->endBody() ?>

    <script type="text/javascript" src="/js/jqueryscript.min.js"></script>
    <script type="text/javascript" src="/js/jqueryuiscript.min.js"></script>
    <script type="text/javascript" src="/js/easing.min.js"></script>        
    <script type="text/javascript" src="/js/smoothscroll.min.js"></script>        
    <script type="text/javascript" src="/js/magnific.min.js"></script>        
    <script type="text/javascript" src="/js/bxslider.min.js"></script>        
    <script type="text/javascript" src="/js/fitvids.min.js"></script>
    <script type="text/javascript" src="/js/viewportchecker.min.js"></script>        
    <script type="text/javascript" src="/js/init.js"></script>        
</body>
</html>
<?php $this->endPage() ?>
