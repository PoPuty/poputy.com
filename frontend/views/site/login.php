<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
use nodge\eauth\EAuth;
use yii\helpers\Html;
use yii\bootstrap\Alert;
use yii\bootstrap\ActiveForm;
$this->title = Yii::t('frontend', 'login');
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
	<?php if(isset($_GET['activate']) && $_GET['activate'] == 'true') : ?>
	
		<p> <?=Yii::t('frontend', 'confirm_active');?></p>
	<?php endif;?>
    

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
				  <?= Yii::$app->session->getFlash('Activate'); ?>
                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div style="color:#999;margin:1em 0">
                    <?= Yii::t('frontend', 'If you forgot your password you can'); ?> <?= Html::a( Yii::t('frontend', 'reset it'), ['site/request-password-reset']) ?>.
                </div>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('frontend', 'login'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
<?php
    if (Yii::$app->getSession()->hasFlash('error')) {
        echo '<div class="alert alert-danger">'.Yii::$app->getSession()->getFlash('error').'</div>';
    }
?>
<p class="lead">Do you already have an account on one of these sites? Click the logo to log in with it here:</p>
<?php echo \nodge\eauth\Widget::widget(array('action' => 'site/login')); ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
