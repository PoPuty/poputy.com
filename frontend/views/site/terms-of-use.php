<?php
$this->title = 'Kasutus- ja uldtingimused';
$this->params['breadcrumbs'][] = $this->title;
?>
<h3>Kasutus- ja uldtingimused</h3>

<p class="rules-paragraph">
1. Uldsatted
</p><p class="rules-paragraph">
1.1. Kliendikonto, ehk “MinuKonto” (MK), kasutamisel noustute jargima Ruby Fox OÜ kehtestatud kasutustingimusi.
</p><p class="rules-paragraph">
1.2. MK-d haldab Ruby Fox OÜ (edaspidi RF), registrikood 14217759, juriidiline asukoht ja postiaadress ide tn 12-15, Rakvere linn, Lääne-Viru maakond, 44316.
</p><p class="rules-paragraph">
2. Moisted
</p><p class="rules-paragraph">
2.1. MK – teenus, mis holbustab veebikeskkondade ja -teenuste kasutamist, sidudes nende kasutajaskonna uheks tervikuks.
</p><p class="rules-paragraph">
2.2. Kasutaja – MK registreerunud fuusiline isik.
</p><p class="rules-paragraph">
2.3. Isikuandmed – MK kasutamisel tekkivad andmed kasutaja kohta.
</p><p class="rules-paragraph">
2.4. MK – konto, mille abil on kasutajal voimalik kasutada teenusepakkuja veebikeskkondi ja -teenuseid.
</p><p class="rules-paragraph">
2.5 Teenusepakkuja – juriidiline isik, kes pakub konto kaudu ligipaasu oma veebikeskkondadele ja -teenustele. Teenusepakkuja on Ruby Fox OÜ, registrikood 14217759.
</p><p class="rules-paragraph">
2.6. Veebikeskkond – teenusepakkuja hallatav internetikeskkond ning mille voimaluste kasutamiseks on tarvis olla registreerinud kasutaja.
</p><p class="rules-paragraph">
2.7. Veebiteenused – teenusepakkujate hallatavad teenused, mis voimaldavad uksnes MK registreerinud kasutajatel tasu eest voi tasuta lugeda ajalehti ja ajakirju eri seadmete abil (muu hulgas internetis, iPadi versioonis, Androidi versioonis jne) ning tarbida erinevaid teenuseid
</p><p class="rules-paragraph">
2.8. Sisu – kasutaja poolt profiili ja/voi veebikeskkonda lisatud ja/voi ules laaditud kommentaar/kirje, artikkel, foto voi video jne.
</p><p class="rules-paragraph">
3. MinuKonto sisu
</p><p class="rules-paragraph">
3.1. MK voimaldab vastavalt iga veebikeskkonna spetsiifikale laadida kasutajal veebikeskkonda ules pilte ja videoid ning kasutada teenusepakkujate foorumeid, ajakirjade digitaalseid valjaandeid ning muid vastavas keskkonnas pakutavaid voimalusi.
</p><p class="rules-paragraph">
3.2. MK voimaldab kasutajal lisada ja/voi ules laadida sisu eri veebikeskkondades, jargides peale kasutustingimuste ka vastava veebikeskkonna kasutustingimusi ning vottes arvesse, et kasutaja avaldatud ja/voi ules laaditud sisu eest kannab isiklikku varalist vastutust kasutaja. Iga teenusepakkuja voib kasutustingimustele lisaks kehtestada oma veebikeskkonna ja -teenuste kasutustingimused, mis reguleerivad konkreetse veebikeskkonna ja -teenuste kasutamist ning millega noustumine on vastava veebikeskkonna ja -teenuste kasutamise eeltingimus.
</p><p class="rules-paragraph">
3.3. MK voimaldab kasutajal hakata teenusepakkuja kaasautoriks, edastades talle avaldamiseks kommentaari/kirje, artikli, foto voi video vm. Kasutaja edastatud ja/voi ules laaditud kommentaari/kirje, artikli, foto voi video avaldamise eest kannab isiklikku varalist vastutust kasutaja. RF, ega teenusepakkuja, ei vastuta kasutaja poolt teenusepakkujale avaldamiseks edastatud materjali kasutamise ja/voi mittekasutamise eest ja/voi sellest tulenevate voimalike kulude ja kahjude eest.
</p><p class="rules-paragraph">
4. Kasutajakonto loomine ja sulgemine, teiste kontode kasutamine ja sidumine
</p><p class="rules-paragraph">
4.1. MK teenuse kasutamiseks peab isik omale registreerima MinuKonto.
</p><p class="rules-paragraph">
4.2. MK teenuse kasutamiseks peab kasutaja arvutis olema lubatud kasutada kupsiseid (ingl cookie), mis voimaldavad aktiivset sessiooni meeles pidada ja aitavad veebilehte navigeerida.
</p><p class="rules-paragraph">
4.3. MK loomisega kinnitab kasutaja, et on vahemalt 18aastane ja konto kasutamiseks taielikult teo- ja otsustusvoimeline fuusiline isik.
</p><p class="rules-paragraph">
4.4. MK loomiseks peab isik valima endale kasutajanime, ekraaninime ja salasona (parooli). Soovi korral saab kasutaja siduda MK oma Google'i, Facebooki, Yahoo voi Twitteri, Odnoklassniki, VK kontoga. Selliselt on tal voimalik edaspidi teenusepakkuja veebikeskkondade ja -teenuste kasutamiseks sisse logida vastavate kasutajatunnustega.
</p><p class="rules-paragraph">
4.5. Kasutaja on teadlik, et kasutajanime ja salasona (parooli) sattumisel kolmandate isikute valdusesse vastutab ta nende isikute poolt kasutajale voetud kohustuste taitmise voi kahju huvitamise kohustuse eest.
</p><p class="rules-paragraph">
4.6. Juhul kui kasutaja soovib MK sulgeda, tuleb tal saata vastav sooviavaldus e-posti aadressile klienditugi@trenniajakiri.ee 
</p><p class="rules-paragraph">
4.7. Kasutustingimuste rikkumisel on RF-l oigus kasutaja konto tahtajatult sulgeda ilma kasutajat sellest ette teavitamata. RF ei ole kohustatud kasutajale konto sulgemist pohjendama.
</p><p class="rules-paragraph">
5. Kampaania tingimused
</p><p class="rules-paragraph">
5.1. Tutvumis- ja kampaaniahinnad kehtivad paketi valiku juures toodud perioodile ja ainult vastava teenuspaketi vormistamisel. Pusitellimuste puhul, parast vastava perioodi loppu, rakendub uuele tellimusele automaatselt tavahind, kui kasutaja pole enne tellimust lopetanud. Pusitellimust on voimalik igal hetkel lopetada MK profiili vaates tellimuste halduse jaotuses voi vottes uhendust klienditeenindusega telefonil +37258667771 voi e-posti aadressil klienditugi@trenniajakiri.ee .
</p><p class="rules-paragraph">
5.2. Pusitellimus on automaatselt pikenev nii maksekaardi, kui mobiiliga tehtud makse. Pusitellimust saab iga hetk katkestada. Tasutud perioodi eest tagasiarveldust ei tehta.
</p><p class="rules-paragraph">
5.3. Tagasiarveldusi summadele alla 8 euro ei tehta.
</p><p class="rules-paragraph">
6. Kupsise salvestamine
</p><p class="rules-paragraph">
6.1. Kupsis- tekstifail, mis saadetakse ja salvestatakse Kasutaja arvutisse veebilehtede poolt, mida Kasutaja kulastab. Kupsis salvestatakse Kasutaja veebilehitseja failikataloogis. Juhul, kui Kasutaja on kulastanud veebilehte varem, loeb veebilehitseja kupsist ning edastab vastava teabe veebilehele voi elemendile, mis kupsise algselt salvestas. Lisateave kupsiste kohta on kattesaadav veebilehel www.aboutcookies.org.
</p><p class="rules-paragraph">
6.2. Kupsised voimaldavad jalgida veebilehe kasutusstatistikat, rubriikide populaarsusest ning muudest veebilehel tehtavatest toimingutest. Kupsisest saadavat teavet kasutatakse veebilehe kasutusmugavuse ja sisu parandamise eesmargil.
</p><p class="rules-paragraph">
6.3. RF kuuluvad domeenid voivad muuhulgas sisaldada elemente, mis salvestavad kupsiseid kolmanda poole nimel.
</p><p class="rules-paragraph">
7. Veebilehel kasutatavate kupsiste tuubid
</p><p class="rules-paragraph">
7.1. Pusikupsised on hadavajalikud veebilehel ringi liikumiseks ja selle funktsioonide kasutamiseks. Pusikupsiste puudumisel ei saaks Kasutaja kasutada koiki veebilehe funktsioone.
</p><p class="rules-paragraph">
7.2. Sessioonikupsised voimaldavad veebilehel meelde jatta Kasutaja poolt tehtud varasemaid valikud (naiteks kasutajanimi, keelevalik) ning seelabi pakkuda tohusamaid ja isikuparasemaid funktsioone.
</p><p class="rules-paragraph">
7.3. Jalgimiskupsised koguvad andmeid Kasutaja kaitumisest veebilehel. Jalgimiskupsisest saadav informatsioon voimaldab tosta veebilehe kasutusmugavust.
</p><p class="rules-paragraph">
7.4. Reklaamkupsised koguvad andmeid Kasutaja sirvimisharjumuste kohta, mis omakorda voimaldab veebilehel esitada Kasutaja eelistustega kooskolas olevat reklaamisisu. Lisaks voimaldab vastav kupsiseliik moota reklaamikampaania tohusust.
</p><p class="rules-paragraph">
8.Kupsisest keeldumine
</p><p class="rules-paragraph">
8.1. Kasutajal on oigus keelduda kupsiste salvestamisest arvutisse. Vastava soovi korral peab Kasutaja muutma oma veebilehitseja seadeid. Parema ulevaate saamiseks on soovituslik uurida Kasutaja veebilehitseja kupsiste haldamise suvandeid.
</p><p class="rules-paragraph">
8.2. Erituupi veebilehitsejad kasutavad erinevaid meetodeid kupsiste keelamiseks. Tapsem informatsioon on kuvatud veebilehel www.aboutcookies.org.
</p><p class="rules-paragraph">
8.3. Kupsiste blokeerimisel peab Kasutaja arvestama, et koik veebilehe funktsioonid ei pruugi enam Kasutajale olla kattesaadavad.
</p><p class="rules-paragraph">
9. Kasutaja profiil
</p><p class="rules-paragraph">
9.1. Domeeniaadressil trenniajakiri.ee sailitatakse koigi kasutajate profiilid ja tegevused ning need on avalikkusele nahtavad seni, kuni kasutaja ei ole oma seadeid muutnud.
</p><p class="rules-paragraph">
9.1.1. Profiilis kuvatakse vaid kasutaja poolt avalikuks muudetud andmed, sh vaikimisi on alati nahtav ekraaninimi ja avatar (foto). Kasutajal on voimalik andmeid alati muuta ja lisada.
</p><p class="rules-paragraph">
9.2. RF ja teenusepakkuja ei vastuta kasutaja profiilis avalikustatud andmetega seotud asjaolude eest, samuti sellest tulenevate voimalike kulude ja kahjude eest.
</p><p class="rules-paragraph">
9.3. Isikuandmete tootlemisel kohaldatakse seadusandlusega satestatud isikuandmete tootlemise pohimotteid.
</p><p class="rules-paragraph">
10. Nouded sisule
</p><p class="rules-paragraph">
10.1. MK vahendusel ei ole lubatud avaldada ega esitada teenusepakkujale avaldamiseks sisu, mis on:
</p><p class="rules-paragraph">
10.1.1. kuritahtlik, eksitav, ahvardav, vagivalda voi vaenu ohutav ning rassilist, usulist, sotsiaalset, rahvuslikku ja soolist diskrimineerimist propageeriv;
</p><p class="rules-paragraph">
10.1.2. eetikanormide ning hea tavaga vastuolus, sh solvav, au ja vaarikust alandav;
</p><p class="rules-paragraph">
10.1.3. autori-, omandi-, andmekaitse- voi muid isikuoigusi rikkuv;
</p><p class="rules-paragraph">
10.1.4. reklaamisisuga;
</p><p class="rules-paragraph">
10.1.5. pornograafilise voi muu ebasundsa sisuga;
</p><p class="rules-paragraph">
10.1.6. muul viisil Eesti Vabariigi oigusaktidega vastuolus.
</p><p class="rules-paragraph">
10.2. RF jatab endale oiguse eemaldada MK vahendusel kasutaja poolt avaldatud sisu, mis on vastuolus kasutustingimusega. VS ei ole kohustatud kasutajale sisu eemaldamist pohjendama.
</p><p class="rules-paragraph">
10.3. Juhul kui kasutaja avaldatud sisu voi tema poolt teenusepakkujale edastatud sisu avaldamiseks puudus kasutajal oigus voi selle avaldamisel rikuti kolmandate isikute oigusi, voib RF edastada kasutaja isikuandmed isikule, kelle oigusi avaldamine rikkus.
</p><p class="rules-paragraph">
10.4. Kasutaja kannab RF-le voi teenusepakkujale isikliku varaga kogu kahju (sh oigusabikulud), mis tekib kolmandate isikute poolt nouete esitamisega RF voi teenusepakkuja vastu, tingituna kasutaja poolt MK vahendusel avaldatud, ules laaditud ja/voi teenusepakkujale edastatud sisust.
</p><p class="rules-paragraph">
11. Tagastamisoigus
</p><p class="rules-paragraph">
13.1. Tarbijal on oigus pohjust avaldamata taganeda Lepingust 7 paeva jooksul.
</p><p class="rules-paragraph">
13.2. Taganemistahtaeg lopeb 7 paeva moodumisel alates:
</p><p class="rules-paragraph">
13.2.1. Lepingu solmimise paevast Lepingu puhul, mille esemeks on Teenuse osutamine voi muu kestva soorituse tegemine, voi digitaalne sisu, mida ei edastata fuusilisel andmekandjal.
</p><p class="rules-paragraph">
13.2.2. Paevast, mil Klient voi Kliendi nimetatud kolmas isik, kes ei ole kauba vedaja, sai asja fuusiliselt enda valdusesse Lepingu puhul, mille esemeks on Kauba uleandmine;
</p><p class="rules-paragraph">
13.2.3. Paevast, mil Klient voi Kliendi nimetatud kolmas isik, kes ei ole Kauba vedaja, on saanud viimase asja fuusiliselt enda valdusesse Lepingu puhul, mille kohaselt tuleb Kliendile ule anda mitu asja, mis toimetatakse kohale eraldi;
</p><p class="rules-paragraph">
13.2.4. Paevast, mil Klient voi Kliendi nimetatud kolmas isik, kes ei ole Kauba vedaja, on saanud viimase asja fuusiliselt enda valdusesse Lepingu puhul, mille kohaselt tuleb Kliendile asi ule anda mitmes osas;
</p><p class="rules-paragraph">
13.2.5. Paevast, mil Klient voi Kliendi nimetatud kolmas isik, kes ei ole kauba vedaja, on saanud esimese asja fuusiliselt enda valdusesse kestvuslepingu puhul, mille esemeks on asjade korduv uleandmine kindlaksmaaratud aja jooksul.
</p><p class="rules-paragraph">
13.3. Taganemisoiguse kasutamiseks peab Tarbija uhemottelise avaldusega (nt posti, faksi voi e-postiga saadetud kiri) teavitama RF-i oma otsusest taganeda Lepingust. Tarbija voib selleks kasutada Uldtingimustele lisatud taganemisavalduse tuupvormi, kuid see ei ole kohustuslik. Taganemisavalduse tuupvorm on kattesaadav siit.
</p><p class="rules-paragraph">
13.4. Taganemisoiguse kasutamise tahtajast kinnipidamiseks piisab, kui Tarbija saadab teate taganemisoiguse kasutamise kohta enne taganemistahtaja loppu.
</p><p class="rules-paragraph">
13.5. Kui Tarbija taganeb Lepingust, tagastab RF Tarbijale koik Tarbijalt saadud maksed, sealhulgas kattetoimetamiskulud (valja arvatud taiendavad kulud, mis tulenevad Tarbija valitud kattetoimetamise viisist, mis erineb RF pakutud koige odavamast tavaparasest kattetoimetamise viisist) viivitamata, kuid hiljemalt 14 paeva moodumisel alates paevast, mil RF saab teada Tarbija otsusest Lepingust taganeda. RF teeb tagasimaksed, kasutades sama makseviisi, mida kasutas Tarbija makse tegemiseks, valja arvatud juhul, kui Tarbija on sonaselgelt andnud nousoleku teistsuguse makseviisi kasutamiseks. Tarbijale ei kaasne sellise maksete tagastamisega teenustasu ega muud kulu.
</p><p class="rules-paragraph">
13.6. RF on oigus keelduda tagasimaksete tegemisest seni, kuni RF on Lepingu esemeks oleva asja tagasi saanud voi kuni Tarbija on esitanud toendid, et asi on tagasi saadetud, soltuvalt sellest, kumb toimub varem.
</p><p class="rules-paragraph">
13.7. Tarbija kohustub taganemisavalduse esitamisel saatma asja tagasi voi andma selle RF ule viivitamata, kuid hiljemalt 14 paeva moodumisel paevast, mil Tarbija teatas RF oma Lepingust taganemisest.
</p><p class="rules-paragraph">
13.8. Tarbija vastutab uksnes asja vaartuse vahenemise eest, mis on tingitud asja kasutamisest muul viisil, kui on vaja asja olemuses, omadustes ja toimimises veendumiseks.
</p><p class="rules-paragraph">
13.9. Asja tagastamise otsesed kulud katab Tarbija.
</p><p class="rules-paragraph">
13.10. Tarbija taganemisoigust ei kohaldata:

</p><p class="rules-paragraph">

13.10.1. Lepingule, mille esemeks on ajalehtede, ajakirjade ja muude perioodiliselt ilmuvate valjaannete uleandmine, valja arvatud selliste valjaannete tellimiseks solmitud kestvuslepingule.
</p><p class="rules-paragraph">
13.10.2. Lepingule, mille esemeks on suletud umbrises audio- voi videosalvestise voi arvutitarkvara uleandmine, kui Tarbija on umbrise avanud.
</p><p class="rules-paragraph">
13.10.3. Lepingule, mille esemeks on digitaalse sisu edastamine, mida ei toimetata katte fuusilisel andmekandjal.
</p><p class="rules-paragraph">
13.11. Kui Kauba tarnimine (s.h. osatarned), Teenuse osutamine voi muu kestva soorituse tegemine (nt perioodilise trukise uleandmine) algas taganemistahtaja jooksul, tuleb Tarbijal RF-ile tasuda lepingu taitmisena uleantu vaartus proportsionaalselt uleantuga ajani, mil Tarbija teatas oma taganemisest Lepingust, vottes arvesse Lepingu kogumahtu.
</p><p class="rules-paragraph">
14. Loppsatted
</p><p class="rules-paragraph">
14.1. Kasutaja kohustub hoiduma mistahes viisil ja vahenditega MK toimimisse ja/voi tehnilistesse lahendustesse sekkumisest. Keelatud on igasugune tegevus, millega voib kaasneda MK susteemi ulekoormamine voi toimimise hairimine.
</p><p class="rules-paragraph">
14.2. RF-l on oigus MK kasutustingimusi uhepoolselt muuta, tagades selle vastavuse Eesti Vabariigi oigusaktidega. Kasutustingimuste uhepoolse muutmise pohjuseks voib olla vajadus korvaldada vastuolu seadusega, viia kooskolla veebikeskkondade voi -teenuste vajaduste ja voimalustega vmt.
</p><p class="rules-paragraph">
14.3. MK kasutamisest tekkinud vaidlused lahendatakse Viru Maakohtus, lahtudes kaesolevatest tingimustest ja Eesti Vabariigi oigusaktidest.
</p>
<style>
	p {
		margin: 0 0 5px 0;
	}
</style>
