<?php
$this->title = 'ULDTINGIMUSED';
$this->params['breadcrumbs'][] = $this->title;
?>
<h3>ULDTINGIMUSED</h3>


<p>
1. ULDSATTED
</p><p>
1.1. Kaesolevad Ruby Fox OÜ (Ruby Fox) Uldtingimused on Ruby Fox OÜ ja Kliendi vahel solmitud Lepingu lahutamatu osa.
</p><p>
1.2. Lisaks Uldtingimustele reguleerivad Lepinguga tekkivaid oigussuhteid Eesti Vabariigis kehtivad volaoigusseadus (edaspidi VOS), tarbijakaitseseadus (edaspidi TKS) ja teised kauba muuki, teenuse osutamist voi pooltevahelist oigussuhet muul viisil reguleerivad oigusaktid.
</p><p>
1.3. Lepingus, Uldtingimustes ja pooltevahelises suhtluses kasutatakse moisteid alljargnevas tahenduses:
</p><p>
1.3.1 E-Pood on veebiaadressil trenniajakiri.ee asuv Muugikanal.
</p><p>
1.3.2. Isikuandmed on fuusilise isiku nimi ja tema identifitseerimist voimaldavad andmed (nimi, isikukood, sunniaeg), isikut toendava dokumendi andmed, kontaktandmed (aadress, e-post, telefoni number) ning muud isikuandmed, mis on Ruby Fox teatavaks saanud seoses Lepingu solmimise ja taitmisega.
</p><p>
1.3.3. Isikuandmetetootlemine on igasugune Kliendi Isikuandmetega tehtav toiming, sealhulgas Kliendi Isikuandmete kogumine, salvestamine, korrastamine, sailitamine, muutmine ja avalikustamine, juurdepaasu voimaldamine, paringute teostamine ja valjavotete tegemine, kasutamine, edastamine, ristkasutamine, uhendamine, sulgemine, kustutamine voi havitamine, voi mitu eelnimetatud toimingut, soltumata toimingute teostamise viisist ja kasutatavatest vahenditest.
</p><p>
1.3.4. Kasutajakonto on Ruby Fox tasuliste digitaalsete teenuste kasutamiseks loodud personaalne konto, mille kaudu Klient ennast identifitseerib.
</p><p>
1.3.5. Kaup on Ruby Fox poolt Muugikanali kaudu muudav ese voi teenus, sh intellektuaase omandi kaitse all olevad elektroonsed valjaanded.
</p><p>
1.3.6. Kinnitus on Kliendile vahetult parast Lepingu solmimist edastatav teade, mis sisaldab viidet Uldtingimustele ning Lepingu ja Kauba voi Teenuse uksikasjadele.
</p><p>
1.3.7. Klient on iga fuusiline voi juriidiline isik, kes kasutab voi on avaldanud soovi kasutada Ruby Fox teenuseid.
</p><p>
1.3.8. Leping on Ruby Fox ja Kliendi vahel kauba omandamiseks voi teenuse osutamiseks Ruby Fox'i Muugikanali vahendusel solmitud leping.
</p><p>
1.3.9. Muugikanal on Ruby Fox poolt Kliendiga suhtlemiseks, kaupade muugiks ning teenuste osutamiseks loodud vahend. Ruby Fox'i Muugikanaliteks on eelkoige E-Pood ja otsepostitus.
</p><p>
1.3.10. Otsepostitus on igasugune Kliendile saadetud teave, mis on kavandatud otseselt voi kaudselt edendama Ruby Fox kaupade voi teenuste pakkumist voi tostma Ruby Fox mainet. Info, mis on seotud toote voi teenuse osutamise, haldamise voi lepingu taitmisega ei ole Otsepostitus.
</p><p>
1.3.11. Salasona on Kliendi poolt valitud tahe- ja/voi numbrikombinatsioon, mis identifitseerib klienti.
</p><p>
1.3.12. Tarbija on fuusilisest isikust Klient.
</p><p>
1.3.13. Teenus on Ruby Fox poolt Muugikanali kaudu muudav teenus.
</p><p>
1.3.14. Telefonimuuk on Ruby Fox Muugikanal, mille eesmargiks on lepingu solmimine telefonitsi.
</p><p>
1.3.15. Ruby Fox on juriidiline isik Ruby Fox OÜ (registrikood 14217759, juriidiline asukoht ja postiaadress SSide tn 12-15, Rakvere linn, Lääne-Viru maakond, 44316, e-posti aadress info@victorystyle.ee).
</p><p>
1.3.16. Uldtingimused on kaesolevad tingimused, mis on Ruby Fox ja Kliendi vahel Muugikanali kaudu solmitud Lepingu lahutamatu osa.
</p><p>


2. HIND
</p><p>
2.1. Muugikanalites naidatakse hindu nii kaibemaksuga kui ka ilma kaibemaksuta.
</p><p>
2.2. Hinnad kehtivad Lepingu solmimisest kuni kokku lepitud maksetahtaja aegumiseni.
</p><p>
2.3. Ruby Fox jatab endale oiguse teha hindades muudatusi. Muudatusi kajastatakse veebilehel trenniajakiri.ee
</p><p>
2.4. Ruby Fox teatab Kliendile Muugikanalis tellimisprotsessi alguses vastuvoetavad maksevahendid.
</p><p>


3. RUBY FOX OIGUSED JA KOHUSTUSED
</p><p>
3.1. Ruby Fox on oigus Lepingust sanktsioonideta taganeda ning jatta tellitud kaup ule andmata voi teenus osutamata kui:
</p><p>
3.1.1. Kaup on laost otsa saanud;
</p><p>
3.1.2. Ruby Fox voi kaasatud kolmandad isikud ei tarni enam tellitud Kaupa voi ei osuta tellitud Teenust;
</p><p>
3.1.3. Kauba hinda voi omadusi on E-Poes kuvatud valesti susteemivea tottu;
</p><p>
3.1.4. Klient rikub Uldtingimusi.
</p><p>
3.2. Kui Ruby Fox ei ole voimalik Lepingut taita, voetakse Kliendiga uhendust ning tagastatakse tasutud summa nii ruttu kui voimalik, kuid mitte hiljem kui 30 paeva jooksul. Ruby Fox voib tellitu asemel ule anda sama kvaliteedi ja hinnaga asja voi osutada sama kvaliteedi ja hinnaga teenuse.
</p><p>


4. LEPINGU SOLMIMISE KORD
</p><p>
4.1. Lepingu solmimise kord
</p><p>
4.1.1. Klient valib soovitavad toote(d) ja/voi teenuse(d) ning lisab need ostukorvi.
</p><p>
4.1.2. Lepingu solmimiseks vajutab Klient ostukorvis lingile Kinnita, taidab noutud andmevaljad ning tasub ekraanile kuvatava arve koheselt pangaulekande voi krediitkaardiga, solmib e-arve pusimakse lepingu voi tellib arve.
</p><p>
4.1.3. Arve tasumisel, e-arve pusimakse lepingu solmimisel voi juriidilisele isikule arve valjastamisel loetakse Leping solmituks.
</p><p>
4.1.4. Parast Lepingu solmimist edastab Ruby Fox Kliendile Kinnituse lepingu solmimise kohta. Kinnitus edastatakse Kliendi poolt Lepingu solmimisel voi Kasutajakonto loomisel Ruby Fox-ile antud kontaktandmetele (e-mail, telefon, vmt).
</p><p>
4.1.5. Klient kohustub Lepingu solmimisel edastama Ruby Fox-ile ainult oiged andmed.
</p><p>
4.1.6. Lepingu solmimise kohta Kliendile saadetav kinnitus sisaldab viidet Uldtingimustele ning Lepingu ja Kauba voi Teenuse uksikasjadele.
</p><p>
4.1.7. Kinnituses sisalduva viite kaudu sailib Kliendil juurdepaas Lepingu uksikasjadele kahe aasta jooksul Lepingu solmimisest ja/voi kuni Kasutajakonto on aktiivne.
</p><p>
4.1.8. Lepingu solmimine toimub eesti keeles.
</p><p>
4.2. Lepingu solmimine erijuhtudel:
- Digikaubad. Kliendile saadetakse kinnitus lepingu solmimise kohta. Kinnitus sisaldab muuhulgas teavet digitaalses vormis koostatud ja edastatud Kauba voi Teenuse kasutusviisi kohta, rakendatavatest tehnilistest kaitsemeetmete ning riist- ja tarkvaraga uhilduvuse kohta, mida Ruby Fox teab voi peab teadma; samuti sellest, et Lepingu solmimisega annab Klient nousoleku digitaalse sisu edastamiseks ning kinnitab, et ta kaotab sellega taganemisoiguse.
</p><p>
5. TELLIMUSE TAITMINE


</p><p>
5.1. Kliendil on oigus valida Ruby Fox pakutavate Kauba kohaletoimetamise viiside vahel.
</p><p>
5.2. Ruby Fox pakutavateks Tellimuse taitmise viisideks on olenevalt Kauba omadustest Kauba kohaletoimetamine, Teenuse osutamine voi muu soorituse tegemine (eelkoige digitaalse sisu edastamine voi kattesaadavaks tegemine).
</p><p>
5.3. Kauba kohaletoimetamise korral on Ruby Fox kohustused taidetud, kui Ruby Fox voi tema ulesandel tegutsev kolmas isik on Kliendi tellimusele vastaval tahtpaeval voi tahtaja jooksul (nende puudumisel tahtaja jooksul, mida voib asjaolusid arvestades moistlikult oodata):
</p><p>


5.3.1. Toimetanud Kauba Kliendi poolt nimetatud aadressile, postkasti (s.h. Klient peab tagama, et postkast on turvaline, valjaande mootmetele vastav ja Kliendi postkastina identifitseeritav) voi pakiautomaati.
</p><p>
5.3.2. andnud Kliendile uleantava Kauba ule Kliendile voi Kliendi seaduslikule esindajale isikut toendava dokumendi esitamisel. Kolmandatele isikutele kauba uleandmine toimub kokkuleppel Kliendiga.
</p><p>
5.3.3. Ruby Fox teatab Tarbijale Muugikanalis hiljemalt tellimisprotsessi alguses kattetoimetamise piirangud (naiteks piirkonnad, kuhu kauba kattetoimetamine ei ole voimalik voi on voimalik taiendava tasu eest).
</p><p>
5.3.4. Digitaalne sisu loetakse edastatuks voi kattesaadavaks tehtuks kui Ruby Fox on sellise digitaalse sisu, mida ei toimetata katte fuusilisel andmekandjal, edastamist alustanud.
</p><p>
6.1. Teenus loetakse osutatuks, kui see on Kliendile tarnitud/osutatud Kliendi tellimusele vastaval tahtpaeval voi tahtaja jooksul ning nende puudumisel tahtaja jooksul, mida voib asjaolusid arvestades moistlikuks pidada.


</p><p>
6. TAGANEMISOIGUS
</p><p>
6.1. Tarbijal on oigus pohjust avaldamata taganeda Lepingust 14 paeva jooksul.
</p><p>
6.2. Taganemistahtaeg lopeb 14 paeva moodumisel alates:
</p><p>
6.2.1. Lepingu solmimise paevast Lepingu puhul, mille esemeks on Teenuse osutamine voi muu kestva soorituse tegemine, voi digitaalne sisu, mida ei edastata fuusilisel andmekandjal.
</p><p>
6.2.2. paevast, mil Klient voi Kliendi nimetatud kolmas isik, kes ei ole kauba vedaja, sai asja fuusiliselt enda valdusesse Lepingu puhul, mille esemeks on Kauba uleandmine;
</p><p>
6.2.3. paevast, mil Klient voi Kliendi nimetatud kolmas isik, kes ei ole Kauba vedaja, on saanud viimase asja fuusiliselt enda valdusesse Lepingu puhul, mille kohaselt tuleb Kliendile ule anda mitu asja, mis toimetatakse kohale eraldi;
</p><p>
6.2.4. paevast, mil Klient voi Kliendi nimetatud kolmas isik, kes ei ole Kauba vedaja, on saanud viimase asja fuusiliselt enda valdusesse Lepingu puhul, mille kohaselt tuleb Kliendile asi ule anda mitmes osas;
</p><p>
6.2.5. paevast, mil Klient voi Kliendi nimetatud kolmas isik, kes ei ole kauba vedaja, on saanud esimese asja fuusiliselt enda valdusesse kestvuslepingu puhul, mille esemeks on asjade korduv uleandmine kindlaksmaaratud aja jooksul.
</p><p>
6.2.6. Taganemisoiguse kasutamiseks peab Tarbija uhemottelise avaldusega (nt posti, faksi voi e-postiga saadetud kiri) teavitama Ruby Fox'i punktis 10.2 toodud kontaktandmetel oma otsusest taganeda Lepingust. Tarbija voib selleks kasutada Uldtingimustele lisatud taganemisavalduse tuupvormi, kuid see ei ole kohustuslik. Taganemisavalduse tuupvorm on kattesaadav siit.
</p><p>
6.2.7.	Taganemisoiguse kasutamise tahtajast kinnipidamiseks piisab, kui Tarbija saadab teate taganemisoiguse kasutamise kohta enne taganemistahtaja loppu.
</p><p>
6.2.8.	Kui Tarbija taganeb Lepingust, tagastab Ruby Fox Tarbijale koik Tarbijalt saadud maksed, sealhulgas kattetoimetamiskulud (valja arvatud taiendavad kulud, mis tulenevad Tarbija valitud kattetoimetamise viisist, mis erineb Ruby Fox pakutud koige odavamast tavaparasest kattetoimetamise viisist) viivitamata, kuid hiljemalt 14 paeva moodumisel alates paevast, mil Ruby Fox saab teada Tarbija otsusest Lepingust taganeda. Ruby Fox teeb tagasimaksed, kasutades sama makseviisi, mida kasutas Tarbija makse tegemiseks, valja arvatud juhul, kui Tarbija on sonaselgelt andnud nousoleku teistsuguse makseviisi kasutamiseks. Tarbijale ei kaasne sellise maksete tagastamisega teenustasu ega muud kulu.
</p><p>
6.3. Ruby Fox on oigus keelduda tagasimaksete tegemisest seni, kuni Ruby Fox on Lepingu esemeks oleva asja tagasi saanud voi kuni Tarbija on esitanud toendid, et asi on tagasi saadetud, soltuvalt sellest, kumb toimub varem.
</p><p>
6.4. Tarbija kohustub taganemisavalduse esitamisel saatma asja tagasi voi andma selle Ruby Fox'ile ule viivitamata, kuid hiljemalt 14 paeva moodumisel paevast, mil Tarbija teatas Ruby Fox'ile oma Lepingust taganemisest.
</p><p>
6.5. Tarbija vastutab uksnes asja vaartuse vahenemise eest, mis on tingitud asja kasutamisest muul viisil, kui on vaja asja olemuses, omadustes ja toimimises veendumiseks.
</p><p>
6.6 Asja tagastamise otsesed kulud katab Tarbija.
</p><p>
6.7. Tarbija taganemisoigust ei kohaldata:
</p><p>
6.7.1. Lepingule, mille esemeks on ajalehtede, ajakirjade ja muude perioodiliselt ilmuvate valjaannete uleandmine, valja arvatud selliste valjaannete tellimiseks solmitud kestvuslepingule;
</p><p>
6.7.2. Lepingule, mille esemeks on suletud umbrises audio- voi videosalvestise voi arvutitarkvara uleandmine, kui Tarbija on umbrise avanud;
</p><p>
6.7.3. Lepingule, mille esemeks on digitaalse sisu edastamine, mida ei toimetata katte fuusilisel andmekandjal.
</p><p>
6.8.4. Kui Kauba tarnimine (s.h. osatarned), Teenuse osutamine voi muu kestva soorituse tegemine (nt ajalehe uleandmine, koolitusmaterjalide edastamine enne konverentsi) algas taganemistahtaja jooksul, tuleb Tarbijal Ruby Fox'ile tasuda lepingu taitmisena uleantu vaartus proportsionaalselt uleantuga ajani, mil Tarbija teatas oma taganemisest Lepingust, vottes arvesse Lepingu kogumahtu.
</p><p>


7. ISIKUANDMETE TOOTLEMINE
</p><p>


7.1. Lepingu solmimisega annab Klient Ruby Fox'ile nousoleku tema Isikuandmete tootlemiseks vastavalt Uldtingimuste punktis 1.3 c) satestatule.
</p><p>
7.2. Kliendi Isikuandmete tootlemise pohieesmargid on:
</p><p>
7.2.1. Kliendi identifitseerimine.
</p><p>
7.2.2. Kliendi ees voetud kohustuste taitmine oma toodete edastamise ja teenuste osutamise osas.
</p><p>
7.2.3. Kliendiga suhtlemine ja talle Ruby Fox toodete ja teenuste pakkumiste edastamine.
</p><p>
7.2.4. Kliendile hoolikalt valitud koostoopartnerite kaupade ja teenuste pakkumine.
</p><p>
7.2.5. Kliendibaasi haldamine ja analuusimine, et parandada teenuste ja toodete kattesaadavust, valikut, kvaliteeti jmt ning teha Kliendile parimaid pakkumisi.
</p><p>
7.2.6. Kampaaniate korraldamine ja kliendirahulolu uuringute teostamine.
</p><p>
7.2.7. Kliendi maksekohustuse taitmise tagamine.
</p><p>
7.2.8. Kliendi kaitumise analuusimine erinevates Muugikanalites.
</p><p>
7.2.9. Seaduses satestatud kohustuste taitmine voi seaduses lubatud kasutusviiside rakendamine.
</p><p>
7.3. Lisaks Kliendi poolt avaldatud Isikuandmetele on Ruby Fox'il oigus kontrollida ja taiendada Kliendi isikuandmeid avalikest allikatest ja muudest oigusparaselt ligipaasetavatest allikatest saadavate andmetega.
</p><p>
7.4. Ruby Fox teeb koostood isikutega, kellele Ruby Fox voib edastada vastava koostoo raames ja eesmargil Kliendiga seotud andmeid, sh isikuandmeid. Sellisteks isikuteks voivad olla Ruby Fox'i toogruppi kuuluvad isikud, kliendirahulolu uuringut korraldavad ettevotted, volgade sissenoudmisteenuse osutajad, maksehaireregistrid, IT partnerid, postiteenust vahendavad voi osutavad isikud jms asutused ja organisatsioonid. Ruby Fox ei vooranda, rendi ega anna muul viisil kolmandatele isikutele ule voi kolmandate isikute kasutusse Kliendi andmeid.
</p><p>
7.5. Isikuandmete tootlemisel on Kliendil koik isikuandmete kaitse seadusest tulenevad oigused, millest olulisemad on:
</p><p>
7.5.1. Oigus saada Ruby Fox'lt teavet tema kohta kogutud isikuandmete kohta 7 toopaeva jooksul arvates Ruby Fox'ile vastava kirjaliku avalduse esitamisest;
</p><p>
7.5.2. Oigus nouda ebaoigete isikuandmete parandamist;
</p><p>
7.5.3 Oigus keelata tema kontaktandmete kasutamist reklaami ja pakkumiste saatmiseks;
</p><p>
7.5.4. Oigus nouda isikuandmete tootlemise lopetamist, kui isikuandmete tootlemine ei ole seaduse alusel lubatud;
</p><p>
7.5.5. Oigus poorduda Ruby Fox'i, isikuandmete volitatud tootleja, Andmekaitse Inspektsiooni voi kohtu poole kui Klient leiab, et isikuandmete tootlemisel on rikutud tema oigusi.
</p><p>
7.6. Reklaamiks ja pakkumisteks ei loeta teateid, mis on seotud Lepingu taitmisega (nt meeldetuletus Lepingust tuleneva kohustuse taitmiseks).
</p><p>
7.7. Ruby Fox'il on oigus salvestada koiki sidevahendite (e-posti, telefoni vm) teel antud Kliendi teateid ja korraldusi, samuti teavet ja muid toimingud, mis Klient on teinud ning vajadusel kasutada neid salvestisi korralduste voi muude toimingute toendamiseks.

</p><p>

8. OTSEPOSTITUSED
</p><p>
8.1. Lepingu solmimisega voi Kasutajakonto loomisega annab Klient Ruby Fox'ile nousoleku endale Otsepostitusega kommertsteadaannete saatmiseks.
</p><p>
8.2. Kui Klient ei soovi Otsepostitusi saada, saab ta vastava soovi edastada Ruby Fox'ile Uldtingimuste punktis 10.2 toodud kontaktandmetel.
</p><p>
8.3. Eelmises punktis nimetatud nousolek holmab ka nousolekut Ruby Fox koostoopartnerist kolmanda isiku info (sh reklaami) saamiseks. Ruby Fox'il on oigus Kliendile edastada vaid hoolikalt valja valitud koostoopartnerist kolmanda isiku pakkumisi, kui need on seotud voi osa Ruby Fox pakkumisest (naiteks reklaami banner uudiskirja osana). Koostoopartner ei saa oma kasutusse Kliendi andmeid, kui Klient ei ole andnud oma andmete edastamiseks otsest ja selgesonalist nousolekut.
</p><p>
8.4. Kliendil on igal ajal oigus teatada Ruby Fox'ile oma soovist enam mitte saada Ruby Fox'i pakkumisi.
</p><p>

9. PRETENSIOONIDE ESITAMISE KORD
</p><p>
9.1. Kliendil on oigus Kauba voi Teenuse lepingutingimustele mittevastavuse korral tugineda seaduses satestatud oiguskaitsevahenditele.
</p><p>
9.2. Ruby Fox'i vastutus Lepingu alusel on piiratud konkreetse (osa)lepingu ja/voi tellimuse hinnaga.
</p><p>
9.3. Kliendil on oigus Kauba voi Teenuse puuduse ilmnemisel poorduda Ruby Fox'i poole punktis 10.2 toodud kontaktandmetel.
</p><p>
9.4. Klient annab Ruby Fox'ile viimase soovi korral ja tingimusel, et see on moistlikult voimalik ega oma negatiivset moju Ruby Fox'i majandustegevusele voi oiguslikule seisundile, voimaluse moistliku aja valtel omal kulul heastada voi vahendada mistahes Lepingu rikkumisega kaasneda voivat kahju. Puuduste parandamise voi asendamise lepivad Ruby Fox ja Klient omavahel kokku.
</p><p>
9.5. Ruby Fox ei vastuta Kliendi suul Kaubale tekkinud puuduste eest, mis on tekkinud Kauba ebaoige hoiustamise voi sihtotstarbevastase kasutamise tulemusena.
</p><p>
9.6. Teade Kauba puuduste avastamise kohta peab olema esitatud kahe kuu jooksul alates nouetele mittevastavuse voi puuduse avastamisest. Koik teated Kauba puuduste kohta vaadatakse labi ja Kliendiga voetakse uhendust esimesel voimalusel, kuid mitte hiljem kui 14 paeva jooksul alates teate kattesaamisest.
</p><p>
9.7. Kui Ruby Fox ei ole Tarbija kaebust lahendanud voi Tarbija ei noustu Ruby Fox'i lahendusega, voib Tarbija poorduda Tarbijavaidluste komisjoni poole. Menetlustingimustega saab tutvuda ning avaldust esitada siin. Tarbija voib poorduda ka Euroopa Liidu tarbijavaidlusi lahendava platvormi poole.
</p><p>
9.8. Kliendil on oigus igal ajal esitada pretensioone oma Isikuandmete tootlemise osas, sealhulgas nouda enda kohta kaivate Isikuandmete tootlemise lopetamist, avalikustamise voi Isikuandmetele juurdepaasu voimaldamise lopetamist ja/voi kogutud andmete kustutamist voi havitamist, kui selline oigus tuleneb isikuandmete kaitse seadusest voi muust oigusaktist. Kui Klient leiab, et Ruby Fox on Isikuandmete tootlemisel rikkunud tema oigusi, on tal oigus poorduda rikkumise lopetamise noudega Ruby Fox'i poole. Kliendil on igal ajal oigus poorduda oma Isikuandmetest tulenevate oiguste kaitseks Andmekaitse Inspektsiooni voi kohtu poole.

</p><p>

10. MUUD TINGIMUSED
</p><p>
10.1. Ruby Fox'il on oigus uhepoolselt muuta kaesolevaid Uldtingimusi ning tegelikku rakendamist. Muutmisest teavitab Ruby Fox Klienti Ruby Fox'i kodulehekuljel, e-posti teel voi muul viisil vahemalt uks (1) kuu enne vastavate muudatuste joustumist.
</p><p>
10.2. Ruby Fox kontaktandmed: e-posti aadress klienditugi@trenniajakiri.ee ja tel. nr: +37258667771.
</p>
<style>
	p {
		margin: 0 0 5px 0;
	}
</style>
