-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: articles
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article_attachments`
--

DROP TABLE IF EXISTS `article_attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemid` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `titleAttribute` text,
  `hits` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_attachments`
--

LOCK TABLES `article_attachments` WRITE;
/*!40000 ALTER TABLE `article_attachments` DISABLE KEYS */;
/*!40000 ALTER TABLE `article_attachments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article_categories`
--

DROP TABLE IF EXISTS `article_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `description` text,
  `parentid` int(11) NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `access` varchar(64) NOT NULL,
  `language` char(7) NOT NULL,
  `theme` varchar(12) NOT NULL DEFAULT 'blog',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `image` text,
  `image_caption` varchar(255) DEFAULT NULL,
  `image_credits` varchar(255) DEFAULT NULL,
  `params` text,
  `metadesc` text,
  `metakey` text,
  `robots` varchar(20) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `copyright` varchar(50) DEFAULT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `metadesc_en` text NOT NULL,
  `metadesc_ru` text NOT NULL,
  `metakey_ru` text NOT NULL,
  `metakey_en` text NOT NULL,
  `description_ru` text NOT NULL,
  `description_en` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_categories`
--

LOCK TABLES `article_categories` WRITE;
/*!40000 ALTER TABLE `article_categories` DISABLE KEYS */;
INSERT INTO `article_categories` VALUES (2,'Training','training','',0,1,'public','it-IT','blog',0,'','','','{\"categoriesImageWidth\":\"small\",\"categoriesIntroText\":\"No\",\"categoriesFullText\":\"No\",\"categoriesCreatedData\":\"No\",\"categoriesModifiedData\":\"No\",\"categoriesUser\":\"No\",\"categoriesHits\":\"No\",\"categoriesDebug\":\"No\",\"categoryImageWidth\":\"small\",\"categoryIntroText\":\"No\",\"categoryFullText\":\"No\",\"categoryCreatedData\":\"No\",\"categoryModifiedData\":\"No\",\"categoryUser\":\"No\",\"categoryHits\":\"No\",\"categoryDebug\":\"No\",\"itemImageWidth\":\"small\",\"itemIntroText\":\"No\",\"itemFullText\":\"No\",\"itemCreatedData\":\"No\",\"itemModifiedData\":\"No\",\"itemUser\":\"No\",\"itemHits\":\"No\",\"itemDebug\":\"No\"}','','','index, follow','','','','','','','','','',''),(3,'Nutrition','nutrition','',0,1,'public','it-IT','blog',0,'','','','{\"categoriesImageWidth\":\"small\",\"categoriesIntroText\":\"No\",\"categoriesFullText\":\"No\",\"categoriesCreatedData\":\"No\",\"categoriesModifiedData\":\"No\",\"categoriesUser\":\"No\",\"categoriesHits\":\"No\",\"categoriesDebug\":\"No\",\"categoryImageWidth\":\"small\",\"categoryIntroText\":\"No\",\"categoryFullText\":\"No\",\"categoryCreatedData\":\"No\",\"categoryModifiedData\":\"No\",\"categoryUser\":\"No\",\"categoryHits\":\"No\",\"categoryDebug\":\"No\",\"itemImageWidth\":\"small\",\"itemIntroText\":\"No\",\"itemFullText\":\"No\",\"itemCreatedData\":\"No\",\"itemModifiedData\":\"No\",\"itemUser\":\"No\",\"itemHits\":\"No\",\"itemDebug\":\"No\"}','','','index, follow','','','','','','','','','',''),(4,'Health','health','',0,1,'public','it-IT','blog',0,'','','','{\"categoriesImageWidth\":\"small\",\"categoriesIntroText\":\"No\",\"categoriesFullText\":\"No\",\"categoriesCreatedData\":\"No\",\"categoriesModifiedData\":\"No\",\"categoriesUser\":\"No\",\"categoriesHits\":\"No\",\"categoriesDebug\":\"No\",\"categoryImageWidth\":\"small\",\"categoryIntroText\":\"No\",\"categoryFullText\":\"No\",\"categoryCreatedData\":\"No\",\"categoryModifiedData\":\"No\",\"categoryUser\":\"No\",\"categoryHits\":\"No\",\"categoryDebug\":\"No\",\"itemImageWidth\":\"small\",\"itemIntroText\":\"No\",\"itemFullText\":\"No\",\"itemCreatedData\":\"No\",\"itemModifiedData\":\"No\",\"itemUser\":\"No\",\"itemHits\":\"No\",\"itemDebug\":\"No\"}','','','index, follow','','','Health en','Health ru','','','','','',''),(5,'Beauty','beauty','',0,1,'public','it-IT','blog',0,'','','','{\"categoriesImageWidth\":\"small\",\"categoriesIntroText\":\"No\",\"categoriesFullText\":\"No\",\"categoriesCreatedData\":\"No\",\"categoriesModifiedData\":\"No\",\"categoriesUser\":\"No\",\"categoriesHits\":\"No\",\"categoriesDebug\":\"No\",\"categoryImageWidth\":\"small\",\"categoryIntroText\":\"No\",\"categoryFullText\":\"No\",\"categoryCreatedData\":\"No\",\"categoryModifiedData\":\"No\",\"categoryUser\":\"No\",\"categoryHits\":\"No\",\"categoryDebug\":\"No\",\"itemImageWidth\":\"small\",\"itemIntroText\":\"No\",\"itemFullText\":\"No\",\"itemCreatedData\":\"No\",\"itemModifiedData\":\"No\",\"itemUser\":\"No\",\"itemHits\":\"No\",\"itemDebug\":\"No\"}','','','index, follow','','','','','','','','','',''),(6,'Motivation','motivation','',0,1,'public','it-IT','blog',0,'','','','{\"categoriesImageWidth\":\"small\",\"categoriesIntroText\":\"No\",\"categoriesFullText\":\"No\",\"categoriesCreatedData\":\"No\",\"categoriesModifiedData\":\"No\",\"categoriesUser\":\"No\",\"categoriesHits\":\"No\",\"categoriesDebug\":\"No\",\"categoryImageWidth\":\"small\",\"categoryIntroText\":\"No\",\"categoryFullText\":\"No\",\"categoryCreatedData\":\"No\",\"categoryModifiedData\":\"No\",\"categoryUser\":\"No\",\"categoryHits\":\"No\",\"categoryDebug\":\"No\",\"itemImageWidth\":\"small\",\"itemIntroText\":\"No\",\"itemFullText\":\"No\",\"itemCreatedData\":\"No\",\"itemModifiedData\":\"No\",\"itemUser\":\"No\",\"itemHits\":\"No\",\"itemDebug\":\"No\"}','','','index, follow','','','','','','','','','',''),(7,'Blogs','blogs','',0,1,'public','it-IT','blog',0,'','','','{\"categoriesImageWidth\":\"small\",\"categoriesIntroText\":\"No\",\"categoriesFullText\":\"No\",\"categoriesCreatedData\":\"No\",\"categoriesModifiedData\":\"No\",\"categoriesUser\":\"No\",\"categoriesHits\":\"No\",\"categoriesDebug\":\"No\",\"categoryImageWidth\":\"small\",\"categoryIntroText\":\"No\",\"categoryFullText\":\"No\",\"categoryCreatedData\":\"No\",\"categoryModifiedData\":\"No\",\"categoryUser\":\"No\",\"categoryHits\":\"No\",\"categoryDebug\":\"No\",\"itemImageWidth\":\"small\",\"itemIntroText\":\"No\",\"itemFullText\":\"No\",\"itemCreatedData\":\"No\",\"itemModifiedData\":\"No\",\"itemUser\":\"No\",\"itemHits\":\"No\",\"itemDebug\":\"No\"}','','','index, follow','','','','','','','','','',''),(8,'Video','video','',0,1,'public','it-IT','blog',0,'','','','{\"categoriesImageWidth\":\"small\",\"categoriesIntroText\":\"No\",\"categoriesFullText\":\"No\",\"categoriesCreatedData\":\"No\",\"categoriesModifiedData\":\"No\",\"categoriesUser\":\"No\",\"categoriesHits\":\"No\",\"categoriesDebug\":\"No\",\"categoryImageWidth\":\"small\",\"categoryIntroText\":\"No\",\"categoryFullText\":\"No\",\"categoryCreatedData\":\"No\",\"categoryModifiedData\":\"No\",\"categoryUser\":\"No\",\"categoryHits\":\"No\",\"categoryDebug\":\"No\",\"itemImageWidth\":\"small\",\"itemIntroText\":\"No\",\"itemFullText\":\"No\",\"itemCreatedData\":\"No\",\"itemModifiedData\":\"No\",\"itemUser\":\"No\",\"itemHits\":\"No\",\"itemDebug\":\"No\"}','','','index, follow','','','','','','','','','',''),(9,'BeFit','befit','',0,1,'public','it-IT','blog',0,'','','','{\"categoriesImageWidth\":\"small\",\"categoriesIntroText\":\"No\",\"categoriesFullText\":\"No\",\"categoriesCreatedData\":\"No\",\"categoriesModifiedData\":\"No\",\"categoriesUser\":\"No\",\"categoriesHits\":\"No\",\"categoriesDebug\":\"No\",\"categoryImageWidth\":\"small\",\"categoryIntroText\":\"No\",\"categoryFullText\":\"No\",\"categoryCreatedData\":\"No\",\"categoryModifiedData\":\"No\",\"categoryUser\":\"No\",\"categoryHits\":\"No\",\"categoryDebug\":\"No\",\"itemImageWidth\":\"small\",\"itemIntroText\":\"No\",\"itemFullText\":\"No\",\"itemCreatedData\":\"No\",\"itemModifiedData\":\"No\",\"itemUser\":\"No\",\"itemHits\":\"No\",\"itemDebug\":\"No\"}','','','index, follow','','','','','','','','','',''),(10,'Events','events','',0,1,'public','it-IT','blog',0,'','','','{\"categoriesImageWidth\":\"small\",\"categoriesIntroText\":\"No\",\"categoriesFullText\":\"No\",\"categoriesCreatedData\":\"No\",\"categoriesModifiedData\":\"No\",\"categoriesUser\":\"No\",\"categoriesHits\":\"No\",\"categoriesDebug\":\"No\",\"categoryImageWidth\":\"small\",\"categoryIntroText\":\"No\",\"categoryFullText\":\"No\",\"categoryCreatedData\":\"No\",\"categoryModifiedData\":\"No\",\"categoryUser\":\"No\",\"categoryHits\":\"No\",\"categoryDebug\":\"No\",\"itemImageWidth\":\"small\",\"itemIntroText\":\"No\",\"itemFullText\":\"No\",\"itemCreatedData\":\"No\",\"itemModifiedData\":\"No\",\"itemUser\":\"No\",\"itemHits\":\"No\",\"itemDebug\":\"No\"}','','','index, follow','','','','','','','','','',''),(11,'MyFitness','myfitness','',0,1,'public','it-IT','blog',0,'','','','{\"categoriesImageWidth\":\"small\",\"categoriesIntroText\":\"No\",\"categoriesFullText\":\"No\",\"categoriesCreatedData\":\"No\",\"categoriesModifiedData\":\"No\",\"categoriesUser\":\"No\",\"categoriesHits\":\"No\",\"categoriesDebug\":\"No\",\"categoryImageWidth\":\"small\",\"categoryIntroText\":\"No\",\"categoryFullText\":\"No\",\"categoryCreatedData\":\"No\",\"categoryModifiedData\":\"No\",\"categoryUser\":\"No\",\"categoryHits\":\"No\",\"categoryDebug\":\"No\",\"itemImageWidth\":\"small\",\"itemIntroText\":\"No\",\"itemFullText\":\"No\",\"itemCreatedData\":\"No\",\"itemModifiedData\":\"No\",\"itemUser\":\"No\",\"itemHits\":\"No\",\"itemDebug\":\"No\"}','','','index, follow','','','','','','','','','',''),(12,'FitnessPro','fitnesspro','',0,1,'public','it-IT','blog',0,'','','','{\"categoriesImageWidth\":\"small\",\"categoriesIntroText\":\"No\",\"categoriesFullText\":\"No\",\"categoriesCreatedData\":\"No\",\"categoriesModifiedData\":\"No\",\"categoriesUser\":\"No\",\"categoriesHits\":\"No\",\"categoriesDebug\":\"No\",\"categoryImageWidth\":\"small\",\"categoryIntroText\":\"No\",\"categoryFullText\":\"No\",\"categoryCreatedData\":\"No\",\"categoryModifiedData\":\"No\",\"categoryUser\":\"No\",\"categoryHits\":\"No\",\"categoryDebug\":\"No\",\"itemImageWidth\":\"small\",\"itemIntroText\":\"No\",\"itemFullText\":\"No\",\"itemCreatedData\":\"No\",\"itemModifiedData\":\"No\",\"itemUser\":\"No\",\"itemHits\":\"No\",\"itemDebug\":\"No\"}','','','index, follow','','','','','','','','','',''),(13,'Shop','shop','',0,1,'public','it-IT','blog',0,'','','','{\"categoriesImageWidth\":\"small\",\"categoriesIntroText\":\"No\",\"categoriesFullText\":\"No\",\"categoriesCreatedData\":\"No\",\"categoriesModifiedData\":\"No\",\"categoriesUser\":\"No\",\"categoriesHits\":\"No\",\"categoriesDebug\":\"No\",\"categoryImageWidth\":\"small\",\"categoryIntroText\":\"No\",\"categoryFullText\":\"No\",\"categoryCreatedData\":\"No\",\"categoryModifiedData\":\"No\",\"categoryUser\":\"No\",\"categoryHits\":\"No\",\"categoryDebug\":\"No\",\"itemImageWidth\":\"small\",\"itemIntroText\":\"No\",\"itemFullText\":\"No\",\"itemCreatedData\":\"No\",\"itemModifiedData\":\"No\",\"itemUser\":\"No\",\"itemHits\":\"No\",\"itemDebug\":\"No\"}','','','index, follow','','','','','','','','','','');
/*!40000 ALTER TABLE `article_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article_items`
--

DROP TABLE IF EXISTS `article_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `catid` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `introtext` text,
  `fulltext` text,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `access` varchar(64) NOT NULL,
  `language` char(7) NOT NULL,
  `theme` varchar(12) NOT NULL DEFAULT 'blog',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `hits` int(11) NOT NULL DEFAULT '0',
  `image` text,
  `image_caption` varchar(255) DEFAULT NULL,
  `image_credits` varchar(255) DEFAULT NULL,
  `video` text,
  `video_type` varchar(20) DEFAULT NULL,
  `video_caption` varchar(255) DEFAULT NULL,
  `video_credits` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `params` text,
  `metadesc` text,
  `metakey` text,
  `robots` varchar(20) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `copyright` varchar(50) DEFAULT NULL,
  `tags_id_tag` int(11) DEFAULT NULL,
  `tags` varchar(1000) DEFAULT NULL,
  `relations` varchar(255) NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `introtext_en` longtext NOT NULL,
  `fulltext_en` longtext NOT NULL,
  `title_ru` varchar(255) NOT NULL,
  `introtext_ru` longtext NOT NULL,
  `fulltext_ru` longtext NOT NULL,
  `slider` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx-article_items-tags_id_tag` (`tags_id_tag`),
  CONSTRAINT `fk-article_items-tags_id_tag` FOREIGN KEY (`tags_id_tag`) REFERENCES `tags` (`id_tag`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_items`
--

LOCK TABLES `article_items` WRITE;
/*!40000 ALTER TABLE `article_items` DISABLE KEYS */;
INSERT INTO `article_items` VALUES (1,'Etiam convallis feugiat risus, vitae molestie tellus lacinia non in consectetur magna','news',2,5,'<p>Etiam convallis feugiat risus, vitae molestie tellus lacinia non. Sed et condimentum urna, in consectetur magna. Curabitur porta lobortis nulla. Aliquam volutpat luctus pharetra. Nam suscipit venenatis lectus, sit amet feugiat enim iaculis ac. Curabitur et egestas ante. Pellentesque hendrerit imperdiet faucibus</p>\r\n','<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis. Phasellus feugiat risus dui. Nullam faucibus nulla at hendrerit condimentum. Pellentesque feugiat nulla quam, a euismod nisi auctor quis. In ut odio pellentesque, feugiat enim id, mattis leo. Nunc ut libero eget magna maximus fermentum non quis enim. Sed in enim id ipsum eleifend viverra. Nulla ultrices luctus urna eget dictum. Integer ipsum nisl, condimentum eget porta vitae, sodales vel sem. Etiam hendrerit odio nulla, vel accumsan felis interdum id. Morbi magna nibh, suscipit id semper nec, pretium tincidunt nibh. Suspendisse facilisis felis sit amet orci dignissim sollicitudin. Quisque fringilla odio et metus ultricies pretium. Quisque eu posuere ante, sed vestibulum nisi. Vivamus ipsum felis, interdum id tincidunt tincidunt, auctor ac lectus. Sed id est id diam fringilla faucibus nec maximus nibh. Nunc molestie eleifend gravida. Ut at laoreet augue.</p>\r\n',1,'Client','All','blog',0,0,'-.jpg','','','','youtube','','','2017-03-24 20:24:37',5,'2017-03-29 11:36:35',5,'','','','index, follow','','',NULL,'','','','','','','','',0),(2,'Etiam convallis feugiat risus, vitae molestie tellus lacinia non in consectetur magna','etiam-convallis-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna',3,5,'<p>Etiam convallis feugiat risus, vitae molestie tellus lacinia non. Sed et condimentum urna, in consectetur magna. Curabitur porta lobortis nulla. Aliquam volutpat luctus pharetra. Nam suscipit venenatis lectus, sit amet feugiat enim iaculis ac. Curabitur et egestas ante. Pellentesque hendrerit imperdiet faucibus</p>\r\n','<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis. Phasellus feugiat risus dui. Nullam faucibus nulla at hendrerit condimentum. Pellentesque feugiat nulla quam, a euismod nisi auctor quis. In ut odio pellentesque, feugiat enim id, mattis leo. Nunc ut libero eget magna maximus fermentum non quis enim. Sed in enim id ipsum eleifend viverra. Nulla ultrices luctus urna eget dictum. Integer ipsum nisl, condimentum eget porta vitae, sodales vel sem. Etiam hendrerit odio nulla, vel accumsan felis interdum id. Morbi magna nibh, suscipit id semper nec, pretium tincidunt nibh. Suspendisse facilisis felis sit amet orci dignissim sollicitudin. Quisque fringilla odio et metus ultricies pretium. Quisque eu posuere ante, sed vestibulum nisi. Vivamus ipsum felis, interdum id tincidunt tincidunt, auctor ac lectus. Sed id est id diam fringilla faucibus nec maximus nibh. Nunc molestie eleifend gravida. Ut at laoreet augue.</p>\r\n',1,'public','All','blog',0,0,'etiam-convallis-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna.jpg','','','','youtube','','','2017-03-28 21:37:10',5,'0000-00-00 00:00:00',0,'',NULL,NULL,'index, follow','','',NULL,'','','','','','','','',0),(3,'Etiam convallis feugiat risus, vitae molestie tellus lacinia non in consectetur magna','etiam-convallis-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna',4,5,'<p>Etiam convallis feugiat risus, vitae molestie tellus lacinia non. Sed et condimentum urna, in consectetur magna. Curabitur porta lobortis nulla. Aliquam volutpat luctus pharetra. Nam suscipit venenatis lectus, sit amet feugiat enim iaculis ac. Curabitur et egestas ante. Pellentesque hendrerit imperdiet faucibus</p>\r\n','<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis. Phasellus feugiat risus dui. Nullam faucibus nulla at hendrerit condimentum. Pellentesque feugiat nulla quam, a euismod nisi auctor quis. In ut odio pellentesque, feugiat enim id, mattis leo. Nunc ut libero eget magna maximus fermentum non quis enim. Sed in enim id ipsum eleifend viverra. Nulla ultrices luctus urna eget dictum. Integer ipsum nisl, condimentum eget porta vitae, sodales vel sem. Etiam hendrerit odio nulla, vel accumsan felis interdum id. Morbi magna nibh, suscipit id semper nec, pretium tincidunt nibh. Suspendisse facilisis felis sit amet orci dignissim sollicitudin. Quisque fringilla odio et metus ultricies pretium. Quisque eu posuere ante, sed vestibulum nisi. Vivamus ipsum felis, interdum id tincidunt tincidunt, auctor ac lectus. Sed id est id diam fringilla faucibus nec maximus nibh. Nunc molestie eleifend gravida. Ut at laoreet augue.</p>\r\n',1,'public','All','blog',0,0,'etiam-convallis-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna.jpg','','','','youtube','','','2017-03-28 21:37:58',5,'0000-00-00 00:00:00',0,'',NULL,NULL,'index, follow','','',NULL,'','','','','','','','',0),(4,'Etiam convallis feugiat risus, vitae molestie tellus lacinia non in consectetur magna','etiam-convallis-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna',5,5,'<p>Etiam convallis feugiat risus, vitae molestie tellus lacinia non. Sed et condimentum urna, in consectetur magna. Curabitur porta lobortis nulla. Aliquam volutpat luctus pharetra. Nam suscipit venenatis lectus, sit amet feugiat enim iaculis ac. Curabitur et egestas ante. Pellentesque hendrerit imperdiet faucibus</p>\r\n','<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis. Phasellus feugiat risus dui. Nullam faucibus nulla at hendrerit condimentum. Pellentesque feugiat nulla quam, a euismod nisi auctor quis. In ut odio pellentesque, feugiat enim id, mattis leo. Nunc ut libero eget magna maximus fermentum non quis enim. Sed in enim id ipsum eleifend viverra. Nulla ultrices luctus urna eget dictum. Integer ipsum nisl, condimentum eget porta vitae, sodales vel sem. Etiam hendrerit odio nulla, vel accumsan felis interdum id. Morbi magna nibh, suscipit id semper nec, pretium tincidunt nibh. Suspendisse facilisis felis sit amet orci dignissim sollicitudin. Quisque fringilla odio et metus ultricies pretium. Quisque eu posuere ante, sed vestibulum nisi. Vivamus ipsum felis, interdum id tincidunt tincidunt, auctor ac lectus. Sed id est id diam fringilla faucibus nec maximus nibh. Nunc molestie eleifend gravida. Ut at laoreet augue.</p>\r\n',1,'public','All','blog',0,0,'etiam-convallis-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna.jpg','','','','youtube','','','2017-03-28 21:38:39',5,'2017-03-28 23:27:07',5,'',NULL,NULL,'index, follow','','',NULL,'','','','','','','','',0),(5,'Etiam convallis feugiat risus, vitae molestie tellus lacinia non in consectetur magna','etiam-convallis-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna',6,5,'<p>Etiam convallis feugiat risus, vitae molestie tellus lacinia non. Sed et condimentum urna, in consectetur magna. Curabitur porta lobortis nulla. Aliquam volutpat luctus pharetra. Nam suscipit venenatis lectus, sit amet feugiat enim iaculis ac. Curabitur et egestas ante. Pellentesque hendrerit imperdiet faucibus</p>\r\n','<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis. Phasellus feugiat risus dui. Nullam faucibus nulla at hendrerit condimentum. Pellentesque feugiat nulla quam, a euismod nisi auctor quis. In ut odio pellentesque, feugiat enim id, mattis leo. Nunc ut libero eget magna maximus fermentum non quis enim. Sed in enim id ipsum eleifend viverra. Nulla ultrices luctus urna eget dictum. Integer ipsum nisl, condimentum eget porta vitae, sodales vel sem. Etiam hendrerit odio nulla, vel accumsan felis interdum id. Morbi magna nibh, suscipit id semper nec, pretium tincidunt nibh. Suspendisse facilisis felis sit amet orci dignissim sollicitudin. Quisque fringilla odio et metus ultricies pretium. Quisque eu posuere ante, sed vestibulum nisi. Vivamus ipsum felis, interdum id tincidunt tincidunt, auctor ac lectus. Sed id est id diam fringilla faucibus nec maximus nibh. Nunc molestie eleifend gravida. Ut at laoreet augue.</p>\r\n',1,'public','All','blog',0,0,'etiam-convallis-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna.jpg','','','','youtube','','','2017-03-28 21:39:12',5,'0000-00-00 00:00:00',0,'',NULL,NULL,'index, follow','','',NULL,'','','','','','','','',0),(6,'Etiam convallis w feugiat risus, vitae molestie tellus lacinia non in consectetur magna','etiam-convallis-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna',7,5,'<p>Etiam convallis feugiat risus, vitae molestie tellus lacinia non. Sed et condimentum urna, in consectetur magna. Curabitur porta lobortis nulla. Aliquam volutpat luctus pharetra. Nam suscipit venenatis lectus, sit amet feugiat enim iaculis ac. Curabitur et egestas ante. Pellentesque hendrerit imperdiet faucibus</p>\r\n','<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis. Phasellus feugiat risus dui. Nullam faucibus nulla at hendrerit condimentum. Pellentesque feugiat nulla quam, a euismod nisi auctor quis. In ut odio pellentesque, feugiat enim id, mattis leo. Nunc ut libero eget magna maximus fermentum non quis enim. Sed in enim id ipsum eleifend viverra. Nulla ultrices luctus urna eget dictum. Integer ipsum nisl, condimentum eget porta vitae, sodales vel sem. Etiam hendrerit odio nulla, vel accumsan felis interdum id. Morbi magna nibh, suscipit id semper nec, pretium tincidunt nibh. Suspendisse facilisis felis sit amet orci dignissim sollicitudin. Quisque fringilla odio et metus ultricies pretium. Quisque eu posuere ante, sed vestibulum nisi. Vivamus ipsum felis, interdum id tincidunt tincidunt, auctor ac lectus. Sed id est id diam fringilla faucibus nec maximus nibh. Nunc molestie eleifend gravida. Ut at laoreet augue.</p>\r\n',1,'public','All','blog',0,0,'etiam-convallis-w-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna.jpg','','','','youtube','','','2017-03-28 21:39:57',5,'2017-03-28 21:49:03',5,'',NULL,NULL,'index, follow','','',NULL,'','','','','','','','',0),(7,'Etiam convallis feug3iat risus, vitae molestie tellus lacinia non in consectetur magna','etiam-convallis-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna',8,5,'<p>Etiam convallis feugiat risus, vitae molestie tellus lacinia non. Sed et condimentum urna, in consectetur magna. Curabitur porta lobortis nulla. Aliquam volutpat luctus pharetra. Nam suscipit venenatis lectus, sit amet feugiat enim iaculis ac. Curabitur et egestas ante. Pellentesque hendrerit imperdiet faucibus</p>\r\n','<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis. Phasellus feugiat risus dui. Nullam faucibus nulla at hendrerit condimentum. Pellentesque feugiat nulla quam, a euismod nisi auctor quis. In ut odio pellentesque, feugiat enim id, mattis leo. Nunc ut libero eget magna maximus fermentum non quis enim. Sed in enim id ipsum eleifend viverra. Nulla ultrices luctus urna eget dictum. Integer ipsum nisl, condimentum eget porta vitae, sodales vel sem. Etiam hendrerit odio nulla, vel accumsan felis interdum id. Morbi magna nibh, suscipit id semper nec, pretium tincidunt nibh. Suspendisse facilisis felis sit amet orci dignissim sollicitudin. Quisque fringilla odio et metus ultricies pretium. Quisque eu posuere ante, sed vestibulum nisi. Vivamus ipsum felis, interdum id tincidunt tincidunt, auctor ac lectus. Sed id est id diam fringilla faucibus nec maximus nibh. Nunc molestie eleifend gravida. Ut at laoreet augue.</p>\r\n',1,'public','All','blog',0,0,'etiam-convallis-feug3iat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna.jpg','','','','youtube','','','2017-03-28 21:40:31',5,'2017-03-28 21:47:38',5,'',NULL,NULL,'index, follow','','',NULL,'','','','','','','','',0),(8,'Etiam convallis 2 feugiat risus, vitae molestie tellus lacinia non in consectetur magna','etiam-convallis-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna',9,5,'<p>Etiam convallis feugiat risus, vitae molestie tellus lacinia non. Sed et condimentum urna, in consectetur magna. Curabitur porta lobortis nulla. Aliquam volutpat luctus pharetra. Nam suscipit venenatis lectus, sit amet feugiat enim iaculis ac. Curabitur et egestas ante. Pellentesque hendrerit imperdiet faucibus</p>\r\n','<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis. Phasellus feugiat risus dui. Nullam faucibus nulla at hendrerit condimentum. Pellentesque feugiat nulla quam, a euismod nisi auctor quis. In ut odio pellentesque, feugiat enim id, mattis leo. Nunc ut libero eget magna maximus fermentum non quis enim. Sed in enim id ipsum eleifend viverra. Nulla ultrices luctus urna eget dictum. Integer ipsum nisl, condimentum eget porta vitae, sodales vel sem. Etiam hendrerit odio nulla, vel accumsan felis interdum id. Morbi magna nibh, suscipit id semper nec, pretium tincidunt nibh. Suspendisse facilisis felis sit amet orci dignissim sollicitudin. Quisque fringilla odio et metus ultricies pretium. Quisque eu posuere ante, sed vestibulum nisi. Vivamus ipsum felis, interdum id tincidunt tincidunt, auctor ac lectus. Sed id est id diam fringilla faucibus nec maximus nibh. Nunc molestie eleifend gravida. Ut at laoreet augue.</p>\r\n',1,'public','All','blog',0,0,'etiam-convallis-2-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna.jpg','','','','youtube','','','2017-03-28 21:41:13',5,'2017-03-28 21:46:31',5,'',NULL,NULL,'index, follow','','',NULL,'','','','','','','','',0),(9,'Etiam convallis 1 feugiat risus, vitae molestie tellus lacinia non in consectetur magna','etiam-convallis-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna',10,5,'<p>Etiam convallis feugiat risus, vitae molestie tellus lacinia non. Sed et condimentum urna, in consectetur magna. Curabitur porta lobortis nulla. Aliquam volutpat luctus pharetra. Nam suscipit venenatis lectus, sit amet feugiat enim iaculis ac. Curabitur et egestas ante. Pellentesque hendrerit imperdiet faucibus</p>\r\n','<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis. Phasellus feugiat risus dui. Nullam faucibus nulla at hendrerit condimentum. Pellentesque feugiat nulla quam, a euismod nisi auctor quis. In ut odio pellentesque, feugiat enim id, mattis leo. Nunc ut libero eget magna maximus fermentum non quis enim. Sed in enim id ipsum eleifend viverra. Nulla ultrices luctus urna eget dictum. Integer ipsum nisl, condimentum eget porta vitae, sodales vel sem. Etiam hendrerit odio nulla, vel accumsan felis interdum id. Morbi magna nibh, suscipit id semper nec, pretium tincidunt nibh. Suspendisse facilisis felis sit amet orci dignissim sollicitudin. Quisque fringilla odio et metus ultricies pretium. Quisque eu posuere ante, sed vestibulum nisi. Vivamus ipsum felis, interdum id tincidunt tincidunt, auctor ac lectus. Sed id est id diam fringilla faucibus nec maximus nibh. Nunc molestie eleifend gravida. Ut at laoreet augue.</p>\r\n',1,'public','All','blog',0,0,'etiam-convallis-1-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna.jpg','','','','youtube','','','2017-03-28 21:41:46',5,'2017-03-29 16:06:17',5,'',NULL,NULL,'index, follow','','',NULL,'2,3,','3,5,6,','','','','','','',0),(10,'Etiam convallis feugiat risus, vitae molestie tellus lacinia non in consectetur magna','etiam-convallis-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna',11,5,'<p>Etiam convallis feugiat risus, vitae molestie tellus lacinia non. Sed et condimentum urna, in consectetur magna. Curabitur porta lobortis nulla. Aliquam volutpat luctus pharetra. Nam suscipit venenatis lectus, sit amet feugiat enim iaculis ac. Curabitur et egestas ante. Pellentesque hendrerit imperdiet faucibus</p>\r\n','<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis. Phasellus feugiat risus dui. Nullam faucibus nulla at hendrerit condimentum. Pellentesque feugiat nulla quam, a euismod nisi auctor quis. In ut odio pellentesque, feugiat enim id, mattis leo. Nunc ut libero eget magna maximus fermentum non quis enim. Sed in enim id ipsum eleifend viverra. Nulla ultrices luctus urna eget dictum. Integer ipsum nisl, condimentum eget porta vitae, sodales vel sem. Etiam hendrerit odio nulla, vel accumsan felis interdum id. Morbi magna nibh, suscipit id semper nec, pretium tincidunt nibh. Suspendisse facilisis felis sit amet orci dignissim sollicitudin. Quisque fringilla odio et metus ultricies pretium. Quisque eu posuere ante, sed vestibulum nisi. Vivamus ipsum felis, interdum id tincidunt tincidunt, auctor ac lectus. Sed id est id diam fringilla faucibus nec maximus nibh. Nunc molestie eleifend gravida. Ut at laoreet augue.</p>\r\n',1,'public','All','blog',0,0,'etiam-convallis-feugiat-risus-vitae-molestie-tellus-lacinia-non-in-consectetur-magna.png','','','','youtube','','','2017-03-28 21:42:23',5,'2017-04-07 17:52:41',5,'',NULL,NULL,'index, follow','','',NULL,'','','Etiam convallis feugiat risus, vitae molestie tellus lacinia non in consectetur magna','<p>Etiam convallis feugiat risus, vitae molestie tellus lacinia non. Sed et condimentum urna, in consectetur magna. Curabitur porta lobortis nulla. Aliquam volutpat luctus pharetra. Nam suscipit venenatis lectus, sit amet feugiat enim iaculis ac. Curabitur et egestas ante. Pellentesque hendrerit imperdiet faucibus</p>\r\n','<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis. Phasellus feugiat risus dui. Nullam faucibus nulla at hendrerit condimentum. Pellentesque feugiat nulla quam, a euismod nisi auctor quis. In ut odio pellentesque, feugiat enim id, mattis leo. Nunc ut libero eget magna maximus fermentum non quis enim. Sed in enim id ipsum eleifend viverra. Nulla ultrices luctus urna eget dictum. Integer ipsum nisl, condimentum eget porta vitae, sodales vel sem. Etiam hendrerit odio nulla, vel accumsan felis interdum id. Morbi magna nibh, suscipit id semper nec, pretium tincidunt nibh. Suspendisse facilisis felis sit amet orci dignissim sollicitudin. Quisque fringilla odio et metus ultricies pretium. Quisque eu posuere ante, sed vestibulum nisi. Vivamus ipsum felis, interdum id tincidunt tincidunt, auctor ac lectus. Sed id est id diam fringilla faucibus nec maximus nibh. Nunc molestie eleifend gravida. Ut at laoreet augue.</p>\r\n','Etiam convallis feugiat risus, vitae molestie tellus lacinia non in consectetur magna','<p>Etiam convallis feugiat risus, vitae molestie tellus lacinia non. Sed et condimentum urna, in consectetur magna. Curabitur porta lobortis nulla. Aliquam volutpat luctus pharetra. Nam suscipit venenatis lectus, sit amet feugiat enim iaculis ac. Curabitur et egestas ante. Pellentesque hendrerit imperdiet faucibus</p>\r\n','<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nam in pharetra nulla. Cras aliquet feugiat sapien a dictum. Sed ullamcorper, erat eu cursus sollicitudin, lorem orci condimentum ante, non tincidunt velit dolor eget lacus. Ut dolor ex, gravida in posuere non, vulputate ut est. Maecenas euismod arcu turpis, pulvinar dapibus quam hendrerit in. In hac habitasse platea dictumst. Maecenas nisi elit, aliquet vel laoreet sed, euismod eu ipsum. In varius facilisis ultrices. Integer ac faucibus justo, vitae gravida dolor. Phasellus semper sem mauris, sit amet luctus justo tincidunt sit amet. Sed fermentum, metus vel mollis iaculis, lorem neque consectetur sem, quis sodales lorem augue id nulla. Praesent sodales arcu ut mi pharetra sodales. Etiam tincidunt eget neque vitae dapibus. In pellentesque tellus quis arcu hendrerit, at placerat mauris fermentum. Vestibulum placerat justo facilisis turpis imperdiet sagittis. Phasellus feugiat risus dui. Nullam faucibus nulla at hendrerit condimentum. Pellentesque feugiat nulla quam, a euismod nisi auctor quis. In ut odio pellentesque, feugiat enim id, mattis leo. Nunc ut libero eget magna maximus fermentum non quis enim. Sed in enim id ipsum eleifend viverra. Nulla ultrices luctus urna eget dictum. Integer ipsum nisl, condimentum eget porta vitae, sodales vel sem. Etiam hendrerit odio nulla, vel accumsan felis interdum id. Morbi magna nibh, suscipit id semper nec, pretium tincidunt nibh. Suspendisse facilisis felis sit amet orci dignissim sollicitudin. Quisque fringilla odio et metus ultricies pretium. Quisque eu posuere ante, sed vestibulum nisi. Vivamus ipsum felis, interdum id tincidunt tincidunt, auctor ac lectus. Sed id est id diam fringilla faucibus nec maximus nibh. Nunc molestie eleifend gravida. Ut at laoreet augue.</p>\r\n',1);
/*!40000 ALTER TABLE `article_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles_relations`
--

DROP TABLE IF EXISTS `articles_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_article` int(11) DEFAULT NULL,
  `id_article_relations` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx-articles_relations-id_article` (`id_article`),
  KEY `idx-articles_relations-id_article_relations` (`id_article_relations`),
  CONSTRAINT `fk-articles_relations-id_article` FOREIGN KEY (`id_article`) REFERENCES `article_items` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-articles_relations-id_article_relations` FOREIGN KEY (`id_article_relations`) REFERENCES `article_items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles_relations`
--

LOCK TABLES `articles_relations` WRITE;
/*!40000 ALTER TABLE `articles_relations` DISABLE KEYS */;
INSERT INTO `articles_relations` VALUES (1,9,2),(2,9,3);
/*!40000 ALTER TABLE `articles_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles_tags`
--

DROP TABLE IF EXISTS `articles_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tag` int(11) DEFAULT NULL,
  `id_article` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles_tags`
--

LOCK TABLES `articles_tags` WRITE;
/*!40000 ALTER TABLE `articles_tags` DISABLE KEYS */;
INSERT INTO `articles_tags` VALUES (8,2,9),(9,3,9);
/*!40000 ALTER TABLE `articles_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` VALUES ('admin','5',1490351582);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('admin',1,'Админ',NULL,NULL,1490351521,1490351521),('articles-create-categories',2,'articles-create-categories',NULL,NULL,1490384147,1490384147),('articles-create-items',2,'articles-create-items',NULL,NULL,1490373838,1490373838),('articles-delete-all-items',2,'articles-delete-all-items',NULL,NULL,1490373838,1490373838),('articles-delete-categories',2,'articles-delete-categories',NULL,NULL,1490384147,1490384147),('articles-delete-his-items',2,'articles-delete-his-items',NULL,NULL,1490373838,1490373838),('articles-index-all-items',2,'articles-index-all-items',NULL,NULL,1490354625,1490354625),('articles-index-categories',2,'articles-index-categories',NULL,NULL,1490384147,1490384147),('articles-index-his-items',2,'articles-index-his-items',NULL,NULL,1490373838,1490373838),('articles-publish-all-items',2,'articles-publish-all-items',NULL,NULL,1490373838,1490373838),('articles-publish-categories',2,'articles-publish-categories',NULL,NULL,1490384147,1490384147),('articles-publish-his-items',2,'articles-publish-his-items',NULL,NULL,1490373838,1490373838),('articles-update-all-items',2,'articles-update-all-items',NULL,NULL,1490373838,1490373838),('articles-update-categories',2,'articles-update-categories',NULL,NULL,1490384147,1490384147),('articles-update-his-items',2,'articles-update-his-items',NULL,NULL,1490373838,1490373838),('articles-view-categories',2,'articles-view-categories',NULL,NULL,1490384147,1490384147),('articles-view-items',2,'articles-view-items',NULL,NULL,1490373838,1490373838),('client',1,'Клиент',NULL,NULL,1490351554,1490351554);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('admin','articles-create-categories'),('admin','articles-create-items'),('admin','articles-delete-all-items'),('admin','articles-delete-categories'),('admin','articles-delete-his-items'),('admin','articles-index-all-items'),('admin','articles-index-categories'),('admin','articles-index-his-items'),('admin','articles-publish-all-items'),('admin','articles-publish-categories'),('admin','articles-publish-his-items'),('admin','articles-update-all-items'),('admin','articles-update-categories'),('admin','articles-update-his-items'),('admin','articles-view-categories'),('admin','articles-view-items');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id_banner` int(11) NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `articles_id_article` int(11) NOT NULL,
  `categories_id_category` int(11) NOT NULL,
  PRIMARY KEY (`id_banner`),
  KEY `idx-banners-articles_id_article` (`articles_id_article`),
  KEY `idx-banners-categories_id_category` (`categories_id_category`),
  CONSTRAINT `fk-banners-articles_id_article` FOREIGN KEY (`articles_id_article`) REFERENCES `article_items` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-banners-categories_id_category` FOREIGN KEY (`categories_id_category`) REFERENCES `article_categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,'/uploads/banners/1490800655.jpg','/uploads/banners/1490800655.jpg',1,2),(2,'/uploads/banners/1490800676.jpg','/uploads/banners/1490800676.jpg',1,2),(3,'/uploads/banners/1490800692.jpg','/uploads/banners/1490800692.jpg',1,2);
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bay_articles`
--

DROP TABLE IF EXISTS `bay_articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bay_articles` (
  `id_bay_articles` int(11) NOT NULL AUTO_INCREMENT,
  `articles_id_article` int(11) NOT NULL,
  `packages_id_package` int(11) NOT NULL,
  `users_id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_bay_articles`),
  KEY `idx-bay_articles-articles_id_article` (`articles_id_article`),
  KEY `idx-bay_articles-packages_id_package` (`packages_id_package`),
  KEY `idx-bay_articles-users_id_user` (`users_id_user`),
  CONSTRAINT `fk-bay_articles-articles_id_article` FOREIGN KEY (`articles_id_article`) REFERENCES `article_items` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-bay_articles-packages_id_package` FOREIGN KEY (`packages_id_package`) REFERENCES `packages` (`id_package`) ON DELETE CASCADE,
  CONSTRAINT `fk-bay_articles-users_id_user` FOREIGN KEY (`users_id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bay_articles`
--

LOCK TABLES `bay_articles` WRITE;
/*!40000 ALTER TABLE `bay_articles` DISABLE KEYS */;
/*!40000 ALTER TABLE `bay_articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupons` (
  `id_coupon` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` int(11) NOT NULL,
  `livetime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_coupon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupons`
--

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `appl_id` int(11) DEFAULT NULL,
  `exten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (7,'39f5433d4eddcf59cd3dd826c4c7d0bd','/uploads/fotos/1490137357.jpg',NULL,'jpg',5),(8,'230e0d32d1de9e0d0e3e8bc6aa31821c_fitted_630x420','/uploads/fotos/1490137357.jpg',NULL,'jpg',5),(9,'39f5433d4eddcf59cd3dd826c4c7d0bd','uploads/brif/1490137357.jpg',23,'jpg',5),(10,'230e0d32d1de9e0d0e3e8bc6aa31821c_fitted_630x420','/uploads/fotos/1490137357.jpg',23,'jpg',5);
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `translation` text COLLATE utf8_unicode_ci,
  `message` text COLLATE utf8_unicode_ci,
  `id_pr` int(11) NOT NULL AUTO_INCREMENT,
  `related` int(11) DEFAULT '0',
  PRIMARY KEY (`id_pr`),
  UNIQUE KEY `unique_id_pr` (`id_pr`),
  KEY `fk_message_source_message` (`id`),
  CONSTRAINT `fk_message_source_message` FOREIGN KEY (`id`) REFERENCES `source_message` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1487172619),('m130524_201442_init',1487172624),('m140506_102106_rbac_init',1490351496),('m151021_200401_create_article_categories_table',1490354809),('m151021_200427_create_article_items_table',1490354809),('m151021_200518_create_article_attachments_table',1490354809),('m170222_050019_add_profile_users',1487838718),('m170223_131426_add_related_profile',1487856091),('m170224_113010_update_user',1487950190),('m170224_151415_applications',1487950474),('m170224_160412_applications_update',1488106791),('m170226_105709_update_user',1488106791),('m170304_113744_language',1488627940),('m170304_122013_language_inset',1488656591),('m170304_193747_update_user',1488656591),('m170319_154312_update_lang_table',1489938250),('m170319_163634_update_edu_table',1489941560),('m170319_164403_update_jobs_table',1489941873),('m170319_191823_update_apl',1489955461),('m170326_185354_insert_table',1490554887),('m170326_190149_insert_table_packege',1490555427),('m170326_191037_insert_index_packege',1490555836),('m170326_210116_insert_banners',1490562435),('m170326_210452_insert_banners',1490562435),('m170328_123923_tags',1490704986),('m170328_125849_tags_articles',1490705984),('m170328_130130_tags_articles',1490706171),('m170329_133229_articles_relations',1490794577);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages` (
  `id_package` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `livetime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_package`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages`
--

LOCK TABLES `packages` WRITE;
/*!40000 ALTER TABLE `packages` DISABLE KEYS */;
INSERT INTO `packages` VALUES (1,'test1 ','2017-03-10','2123');
/*!40000 ALTER TABLE `packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages_has_users`
--

DROP TABLE IF EXISTS `packages_has_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages_has_users` (
  `packages_id_package` int(11) NOT NULL,
  `users_id_user` int(11) NOT NULL,
  `bay_date` int(11) DEFAULT NULL,
  `finish_date` int(11) DEFAULT NULL,
  KEY `idx-packages_has_users-users_id_user` (`users_id_user`),
  KEY `idx-packages_has_users-packages_id_package` (`packages_id_package`),
  CONSTRAINT `fk-packages_has_users-packages_id_package` FOREIGN KEY (`packages_id_package`) REFERENCES `packages` (`id_package`) ON DELETE CASCADE,
  CONSTRAINT `fk-packages_has_users-users_id_user` FOREIGN KEY (`users_id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages_has_users`
--

LOCK TABLES `packages_has_users` WRITE;
/*!40000 ALTER TABLE `packages_has_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `packages_has_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `source_message`
--

DROP TABLE IF EXISTS `source_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `source_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `source_message`
--

LOCK TABLES `source_message` WRITE;
/*!40000 ALTER TABLE `source_message` DISABLE KEYS */;
INSERT INTO `source_message` VALUES (1,'articles','ru-Ru'),(2,'articles','it-IT');
/*!40000 ALTER TABLE `source_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id_tag` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_en` longtext COLLATE utf8_unicode_ci NOT NULL,
  `keywords_en` text COLLATE utf8_unicode_ci NOT NULL,
  `name_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_ru` longtext COLLATE utf8_unicode_ci NOT NULL,
  `keywords_ru` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_tag`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'test','test','test','test','test_en','test_en','test_en','test_en','test_ru','test_ru','test_ru','test_ru'),(2,'test2','test2','test2','test2','','','','','','','',''),(3,'7','7','7','7','','','','','','','','');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `invite` int(11) DEFAULT NULL,
  `blocked_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `coupons_id_coupon` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`),
  KEY `idx-user-coupons_id_coupon` (`coupons_id_coupon`),
  CONSTRAINT `fk-user-coupons_id_coupon` FOREIGN KEY (`coupons_id_coupon`) REFERENCES `coupons` (`id_coupon`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (5,'igor456727@yandex.ru','Sbcjckn3NtXp7sXpECtIjrNyF48tF6QS','$2y$13$9Y0.dRe5Ku9Qonq4s0Q5kOThek3dhZjgYLuXMS/F1xuXpxlfQ59Wa',NULL,'igor456727@yandex.ru',10,1487838773,1487838773,NULL,'0','igor456727@yandex.ru','58d6e4e20d52e9.22600880',NULL),(8,'igor.morgachev.b@gmail.com','y3LdsU-2IPBOKAg8QatNv92S4fMdWVVA','$2y$13$p3zMr/lGDaQWuhoiln5UoejZwyKnWw9If/0BknNaNBuI.AC0IZ5.i',NULL,'igor.morgachev.b@gmail.com',0,1487847241,1487847241,NULL,'','','',NULL),(10,'igor4567237@yandex.ru','xjndeIENcnzd3bZdz9KdS9Yr4FW7odvY','$2y$13$cq3eGQAdWCnqXDzvwvfIreGMe9CU/gSrfitWYnj/9xMpspU93kTNm',NULL,'igor4567237@yandex.ru',10,1488109609,1488109609,5,'','','',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_has_users`
--

DROP TABLE IF EXISTS `users_has_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_has_users` (
  `users_id_users` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_has_users`
--

LOCK TABLES `users_has_users` WRITE;
/*!40000 ALTER TABLE `users_has_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_has_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-07 16:13:44
